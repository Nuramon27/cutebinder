#![deny(broken_intra_doc_links)]
#![allow(unused_imports)]

//! The cutebinder-helpre library contains functions and structs which
//! are used by the code generated by cutebinder.
//!
//! These are empty "ffi-structs" like `QString` and enums or bitfields
//! like [`QItemDataRole`].

use std::convert::TryInto;
use std::mem::size_of;
use std::mem::{self, ManuallyDrop};
use std::ptr;
use std::slice;

use bitflags::bitflags;
use chrono::{Datelike, NaiveDate, NaiveDateTime, NaiveTime, Timelike};
use libc::{c_char, c_int};

#[allow(non_camel_case_types)]
#[repr(C)]
pub struct void {
    #[allow(unused)]
    phantom: [u8; 0],
}

#[repr(C)]
pub union OptionChar<T> {
    some: ManuallyDrop<T>,
    none: c_char,
}

impl<T> OptionChar<T> {
    pub fn new(value: T) -> Self {
        OptionChar {
            some: ManuallyDrop::new(value),
        }
    }
    pub fn uninit() -> Self {
        OptionChar { none: 0 }
    }
    pub fn as_ptr(&self) -> *const T {
        self as *const _ as *const T
    }
    pub fn as_mut_ptr(&mut self) -> *mut T {
        self as *mut _ as *mut T
    }
}

impl<T> std::fmt::Debug for OptionChar<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "OptionChar<{}>", std::any::type_name::<T>())
    }
}

#[derive(Debug)]
#[repr(C)]
pub struct OptionFFI<T> {
    pub is_some: c_char,
    pub value: OptionChar<T>,
}

impl<T> Drop for OptionFFI<T> {
    fn drop(&mut self) {
        if self.is_some == true as c_char {
            unsafe {
                ptr::drop_in_place(self.value.as_mut_ptr());
            }
        }
    }
}

impl<T: From<R>, R> From<Option<R>> for OptionFFI<T> {
    fn from(other: Option<R>) -> Self {
        //unsafe {println!("sizes rust: {} {} {} {}", size_of::<OptionFFI<T>>(), size_of_val(&other.is_some), size_of_val(&other.value.none), size_of_val(&other.value.some));}
        //unsafe {println!("pointers rust: {:?} - {:?} / {:?}", &other.is_some as *const _, &other.value.none as *const _, &other.value.some as *const _);}
        //unsafe { println!("some: {:?}", other.value.some);}
        if let Some(value) = other {
            OptionFFI {
                is_some: true as c_char,
                value: OptionChar::new(T::from(value)),
            }
        } else {
            OptionFFI {
                is_some: false as c_char,
                value: OptionChar::uninit(),
            }
        }
    }
}

use std::mem::size_of_val;
impl<T: std::fmt::Debug> OptionFFI<T> {
    pub fn inner_into<R: From<T>>(self) -> Option<R> {
        unsafe {
            println!(
                "sizes rust: {} {} {} {}",
                size_of::<OptionFFI<T>>(),
                size_of_val(&self.is_some),
                size_of_val(&self.value.none),
                size_of_val(&self.value.some)
            );
        }
        unsafe {
            println!(
                "pointers rust: {:?} - {:?} / {:?}",
                &self.is_some as *const _,
                &self.value.none as *const _,
                &self.value.some as *const _
            );
        }
        unsafe {
            println!("some: {:?}", self.value.some);
        }
        println!("size rust: {}", std::mem::size_of::<OptionFFI<T>>());
        if dbg!(self.is_some) == true as c_char {
            // Safe because `other` has been checked for `is_some`.
            unsafe {
                let res = ptr::read(self.value.as_ptr());
                mem::forget(self);
                Some(R::from(res))
            }
        } else {
            None
        }
    }
}

pub struct OptionDummy {
    #[allow(unused)]
    phantom: [u8; 0],
}

pub struct QString {
    #[allow(unused)]
    phantom: [u8; 0],
}

#[derive(Debug, Clone)]
#[repr(C)]
pub struct QStringParts {
    data: *const u16,
    len: usize,
}

impl From<QStringParts> for String {
    fn from(other: QStringParts) -> String {
        unsafe { String::from_utf16(slice::from_raw_parts(other.data, other.len)).unwrap() }
    }
}

#[derive(Debug, Clone)]
#[repr(C)]
pub struct RustStringParts {
    #[allow(unused)]
    data: *const c_char,
    #[allow(unused)]
    len: usize,
}

impl From<&str> for RustStringParts {
    fn from(other: &str) -> Self {
        RustStringParts {
            len: other.len(),
            data: other.as_ptr() as *const c_char,
        }
    }
}

#[derive(Debug, Clone)]
#[repr(C)]
pub struct QByteArray {
    #[allow(unused)]
    phantom: [u8; 0],
}

#[derive(Debug, Clone)]
#[repr(C)]
pub struct QByteArrayParts {
    data: *const c_char,
    len: usize,
}

impl From<QByteArrayParts> for Vec<i8> {
    fn from(other: QByteArrayParts) -> Vec<i8> {
        unsafe { Vec::from(slice::from_raw_parts(other.data, other.len)) }
    }
}

#[derive(Debug, Clone)]
#[repr(C)]
pub struct RustByteArrayParts {
    #[allow(unused)]
    data: *const u8,
    #[allow(unused)]
    len: usize,
}

impl From<&[u8]> for RustByteArrayParts {
    fn from(other: &[u8]) -> Self {
        RustByteArrayParts {
            len: other.len(),
            data: other.as_ptr(),
        }
    }
}

#[derive(Debug, Clone)]
#[repr(C)]
pub struct FFIDate {
    year: i32,
    month: u32,
    day: u32,
}

impl From<NaiveDate> for FFIDate {
    fn from(other: NaiveDate) -> Self {
        FFIDate {
            year: other.year(),
            month: other.month(),
            day: other.day(),
        }
    }
}

impl From<FFIDate> for NaiveDate {
    fn from(other: FFIDate) -> Self {
        NaiveDate::from_ymd(other.year, other.month, other.day)
    }
}

#[derive(Debug, Clone)]
#[repr(C)]
pub struct FFITime {
    hour: u32,
    minute: u32,
    second: u32,
    millisecond: u32,
}

impl From<NaiveTime> for FFITime {
    fn from(other: NaiveTime) -> Self {
        FFITime {
            hour: other.hour(),
            minute: other.minute(),
            second: other.second(),
            millisecond: other.nanosecond() / 1000000,
        }
    }
}

impl From<FFITime> for NaiveTime {
    fn from(other: FFITime) -> Self {
        NaiveTime::from_hms_nano(other.hour, other.minute, other.second, other.millisecond)
    }
}

#[derive(Debug, Clone)]
#[repr(C)]
pub struct FFIDateTime {
    date: FFIDate,
    time: FFITime,
}

impl From<NaiveDateTime> for FFIDateTime {
    fn from(other: NaiveDateTime) -> Self {
        FFIDateTime {
            date: other.date().into(),
            time: other.time().into(),
        }
    }
}

impl From<FFIDateTime> for NaiveDateTime {
    fn from(other: FFIDateTime) -> Self {
        NaiveDateTime::new(other.date.into(), other.time.into())
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[repr(C)]
pub struct Point {
    pub x: i32,
    pub y: i32,
}
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[repr(C)]
pub struct Size {
    pub width: i32,
    pub height: i32,
}

#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct PointF {
    pub x: f64,
    pub y: f64,
}
#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct SizeF {
    pub width: f64,
    pub height: f64,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[repr(C)]
pub struct Rect {
    pub x: i32,
    pub y: i32,
    pub width: i32,
    pub height: i32,
}

#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct RectF {
    pub x: f64,
    pub y: f64,
    pub width: f64,
    pub height: f64,
}

/// An `enum` that can hold all valid values of a `QItemDataRole`.
#[repr(u16)]
pub enum QItemDataRole {
    DisplayRole = 0x0,
    DecorationRole = 0x1,
    EditRole = 0x2,
    ToolTipRole = 0x3,
    StatusTipRole = 0x4,
    WhatsThisRole = 0x5,
    SizeHintRole = 0xD,
    FontRole = 0x6,
    TextAlignmentRole = 0x7,
    BackgroundRole = 0x8,
    ForegroundRole = 0x9,
    CheckStateRole = 0xA,
    InitialSortOrderRole = 0xE,
    AccessibleTextRole = 0xB,
    AccessibleDescriptionRole = 0xC,
    UserRole = 0x100,
}

bitflags! {
    /// A bitflag corresponding to Qts `ItemFlags`.
    pub struct ItemFlags: u16 {
        const NO_ITEM_FLAGS           = 0x000;
        const ITEM_IS_SELECTABLE      = 0x001;
        const ITEM_IS_EDITABLE        = 0x002;
        const ITEM_IS_DRAG_ENABLED    = 0x004;
        const ITEM_IS_DROP_ENABLED    = 0x008;
        const ITEM_IS_USER_CHECKABLE  = 0x010;
        const ITEM_IS_ENABLED         = 0x020;
        const ITEM_IS_AUTO_TRISTATE   = 0x040;
        const ITEM_NEVER_HAS_CHILDREN = 0x080;
        const ITEM_IS_USER_TRISTATE   = 0x100;
    }
}

pub mod prelude {
    pub use super::{
        void, FFIDate, FFIDateTime, FFITime, ItemFlags, OptionFFI, Point, PointF, QByteArray,
        QByteArrayParts, QItemDataRole, QString, QStringParts, Rect, RectF, RustByteArrayParts,
        RustStringParts, Size, SizeF,
    };
    pub use chrono::{NaiveDate, NaiveDateTime, NaiveTime};
    pub use libc::{c_char, c_int};
}
