#![allow(clippy::if_same_then_else)]

use std::env;
use std::path::Path;
use std::process::Command;

use walkdir::WalkDir;

fn main() {
    let out_dir = env::var("OUT_DIR").unwrap().trim().to_string();
    let manifest_dir = env::var("CARGO_MANIFEST_DIR").unwrap();

    let root_dir = Path::new(&manifest_dir).parent().unwrap();
    let pro_file = root_dir.join("cutebinder_test.pro");
    // If QMAKE_PATH is set, the location of qmake is taken from there.
    // Otherwise, qmake is invoked from path.
    let qmake_cmd = match env::var("QMAKE_PATH") {
        Ok(path) => Path::new(&path).to_str().unwrap().to_string(),
        Err(_) => "qmake".to_string(),
    };
    let qmake_output = Command::new(&qmake_cmd)
        .args(&[pro_file.clone()])
        .args(&["CONFIG+=release"])
        .arg(&format!("VERSION={}", env::var("CARGO_PKG_VERSION").unwrap()))
        .current_dir(&out_dir)
        .output()
        .expect(
            "qmake could not be invoked. Ensure that qmake is in your PATH \
             or set the QMAKE_PATH variable to a directory containing a qmake executable.",
        );
    match (qmake_output.status.code(), qmake_output.stderr) {
        (Some(0), err) => eprintln!("{}", String::from_utf8(err).unwrap()),
        (_, err) => panic!(
            "qmake failed with error:\n{}",
            String::from_utf8(err).unwrap()
        ),
    };

    // On Windows, invoke jom or nmake, on Linux, invoke make in order
    // to build the cpp-code.
    let host = env::var("HOST").unwrap();
    let make_output = if host.contains("windows") {
        Command::new("jom")
            .current_dir(&out_dir)
            .output()
            .or_else(|_| Command::new("nmake").current_dir(&out_dir).output())
            .expect(
                "Neither jom nor nmake could be called. Ensure that one \
                 of them is in your PATH.",
            )
    } else {
        if let Ok(num_jobs) = env::var("NUM_JOBS") {
            Command::new("make")
                .args(&["-j", &num_jobs])
                .current_dir(&out_dir)
                .output()
                .expect("make could not be called. Ensure that it is in your PATH.")
        } else {
            Command::new("make")
                .current_dir(&out_dir)
                .output()
                .expect("make could not be called. Ensure that it is in your PATH.")
        }
    };
    match (make_output.status.code(), make_output.stderr) {
        (Some(0), err) => eprintln!("{}", String::from_utf8(err).unwrap()),
        (_, err) => panic!(
            "Make failed with error:\n{}",
            String::from_utf8(err).unwrap()
        ),
    };

    // Set weddedederr_gui_qml as a dependency.
    println!("cargo:rustc-link-search=all={}", out_dir);
    println!(
        "cargo:rustc-link-search=all={}",
        format!("{}/{}", out_dir, "release")
    );
    println!("cargo:rustc-link-lib=static=cutebinder_test");

    // Set the qt libraries as dependencies, obtaining their location from qmake.
    let qt_library_path = String::from_utf8(
        Command::new(&qmake_cmd)
            .args(&["-query", "QT_INSTALL_LIBS"])
            .output()
            .unwrap()
            .stdout,
    )
    .unwrap();
    println!("cargo:rustc-link-search=all={}", qt_library_path);
    println!("cargo:rustc-link-lib=Qt5Core");
    //println!("cargo:rustc-link-lib=Qt5Qml");
    //println!("cargo:rustc-link-lib=Qt5QuickControls2");
    println!("cargo:rustc-link-lib=Qt5Gui");
    println!("cargo:rustc-link-lib=Qt5Widgets");

    // Link against the c++ standard library.
    // The logic for determining its name is copied from the cc crate.
    let stdcpp_name = if host.contains("msvc") {
        None
    } else if host.contains("apple") {
        Some("c++".to_string())
    } else if host.contains("freebsd") {
        Some("c++".to_string())
    } else if host.contains("openbsd") {
        Some("c++".to_string())
    } else {
        Some("stdc++".to_string())
    };
    if let Some(stdcpp_name) = stdcpp_name {
        println!("cargo:rustc-link-lib=dylib={}", stdcpp_name);
    }
    // A try to enable static linking on windows:
    // let plugin_dir = String::from_utf8(Command::new("qmake")
    //     .args(&["-query", "QT_INSTALL_PLUGINS"])
    //     .output()
    //     .unwrap()
    //     .stdout).unwrap();
    // println!("cargo:rustc-link-search=all={}", format!("{}/{}", plugin_dir.trim(), "platforms"));
    // println!("cargo:rustc-link-lib=qdirect2d");
    // println!("cargo:rustc-link-lib=qoffscreen");
    // println!("cargo:rustc-link-lib=qwebgl");
    // println!("cargo:rustc-link-lib=qwindows");
    // println!("cargo:rustc-link-lib=qminimal");

    // println!("cargo:rustc-link-search=all={}", format!("{}/{}", plugin_dir.trim(), "styles"));
    // println!("cargo:rustc-link-lib=qwindowsvistastyle");

    // println!("cargo:rustc-link-search=all={}", format!("{}/{}", plugin_dir.trim(), "platformthemes"));
    // println!("cargo:rustc-link-lib=qxdgdesktopportal");

    // println!("cargo:rustc-link-lib=user32");
    // println!("cargo:rustc-link-lib=ole32");
    // println!("cargo:rustc-link-lib=shell32");
    // println!("cargo:rustc-link-lib=ws2_32");
    // println!("cargo:rustc-link-lib=oleaut32");
    // println!("cargo:rustc-link-lib=comdlg32");
    // println!("cargo:rustc-link-lib=imm32");
    // println!("cargo:rustc-link-lib=winmm");
    // println!("cargo:rustc-link-lib=glu32");
    // println!("cargo:rustc-link-lib=opengl32");
    // println!("cargo:rustc-link-lib=gdi32");
    // println!("cargo:rustc-link-lib=mpr");
    // println!("cargo:rustc-link-lib=netapi32");
    // println!("cargo:rustc-link-lib=version");
    // println!("cargo:rustc-link-lib=uuid");


    for entry in WalkDir::new(root_dir.join("gui_qml")) {
        println!(
            "cargo:rerun-if-changed={}",
            entry.expect("Error listing files.").path().display()
        );
    }
    //println!("rerun-if-changed={}", "/home/Manuel/Code/black_walker/gui_qt/lib.cpp");
    println!("cargo:rerun-if-env-changed=QMAKE_PATH");
    println!("cargo:rerun-if-env-changed=PATH");
}
