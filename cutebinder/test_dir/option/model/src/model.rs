use crate::interface::{self, MasterTrait, MasterEmitter, MasterCollection};

use chrono::NaiveTime;

pub struct Master {
    value: Option<i32>,
    time: Option<NaiveTime>,
    s: Option<String>,
    emitter: MasterEmitter,
}

impl MasterTrait for Master {
    fn new_default(signal_struct: MasterCollection) -> Master {
        Master {
            value: None,
            time: None,
            s: None,
            emitter: signal_struct.emitter
        }
    }

    fn get_value(&self) -> Option<i32> {
        self.value
    }

    fn set_value(&mut self, value: Option<i32>) {
        self.value = value;
    }

    fn get_time(&mut self) -> Option<NaiveTime> {
        self.time
    }

    fn set_time(&mut self, time: Option<NaiveTime>) {
        self.time = time;
    }

    fn get_string(&self) -> Option<String> {
        self.s.clone()
    }

    fn set_string(&mut self, value: Option<String>) {
        self.s = value;
    }

    fn emit_test_signals(&mut self) {
        self.emitter.send_value_some(Some(12));
        self.emitter.send_string_some(Some("Yay".to_string()));
        self.emitter.send_string_none(None);
    }

    fn object_qt(&mut self) -> *mut cutebinder_helper::void {
        self.emitter.ptr
    }
}