#include <bdg/Bindings.hpp>
#include <bdg/helper.hpp>

#include <QApplication>
#include <QObject>
#include <iostream>

extern "C" int lib(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Master m;
    assert((!m.getValue().is_some));
    assert((!m.getTime().is_some));
    assert((!m.getString().is_some));

    m.setValue(Option<qint32>(19));
    auto x = m.getValue();
    assert((x.is_some));
    assert((x.some == 19));

    m.setTime(Option<QTime>(QTime(12, 1, 30)));
    auto y = m.getTime();
    assert((y.is_some));
    assert((y.some == QTime(12, 1, 30)));

    m.setString(Option<QString>("Hello World"));
    auto s = m.getString();
    assert((s.is_some));
    assert((s.some == QString("Hello World")));

    m.setValue(Option<qint32>());
    m.setTime(Option<QTime>());
    m.setString(Option<QString>());
    assert((!m.getValue().is_some));
    assert((!m.getTime().is_some));
    assert((!m.getString().is_some));

    QObject::connect(&m, &Master::sendValueSome, [](Option<qint32> v) {
        assert((v.is_some));
        assert((v.some == 12));
    });
    QObject::connect(&m, &Master::sendStringNone, [](Option<QString> s) {
        assert((!s.is_some));
    });
    QObject::connect(&m, &Master::sendStringSome, [](Option<QString> s) {
        assert((s.is_some));
        assert((s.some == QString("Yay")));
    });

    emit m.sendValueSome(Option<qint32>(12));
    emit m.sendStringNone(Option<QString>());
    emit m.sendStringSome(Option<QString>(QString("Yay")));

    m.emitTestSignals();

    return 0;
}
