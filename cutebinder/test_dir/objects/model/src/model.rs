use std::pin::Pin;

use chrono::NaiveDate;
use cutebinder_helper::void;

use crate::interface::{self, MasterTrait, MasterEmitter, MasterCollection, ChildTrait, ChildEmitter, ChildCollection};

pub struct Master {
    s: String,
    child: Pin<Box<Child>>,
    emitter: MasterEmitter,
}

impl MasterTrait for Master {
    fn new_default(signal_struct: MasterCollection) -> Master {
        let mut res = Master {
            s: String::new(),
            child: Box::pin(Child::new_default(signal_struct.child.clone())),
            emitter: signal_struct.emitter,
        };
        res.child.emitter = signal_struct.child.produce_emitter(&mut res.child);
        res
    }

    fn from_string(s: String, signal_struct: MasterCollection) -> Master {
        Master {
            s,
            child: Box::pin(Child::new_default(signal_struct.child)),
            emitter: signal_struct.emitter
        }
    }

    fn object_qt(&mut self) -> *mut void {
        self.emitter.ptr
    }

    fn get_string(&self) -> String {
        self.s.clone()
    }

    fn set_string(&mut self, value: String) {
        self.s = value;
    }

    fn get_child(&mut self) -> *mut void {
        self.child.object_qt()
    }

    fn set_value_child(&mut self, value: i32) {
        self.child.v = value;
    }
}

pub struct Child {
    v: i32,
    emitter: ChildEmitter,
}

impl ChildTrait for Child {
    fn new_default(signal_struct: ChildCollection) -> Child {
        Child {
            v: 0,
            emitter: signal_struct.emitter.clone()
        }
    }

    fn object_qt(&mut self) -> *mut void {
        self.emitter.ptr
    }

    fn get_value(&self) -> i32 {
        self.v
    }
    fn set_value(&mut self, value: i32) {
        self.v = value;
    }
}