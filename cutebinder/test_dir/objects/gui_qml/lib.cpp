#include <bdg/Bindings.hpp>
#include <bdg/helper.hpp>

#include <QApplication>
#include <iostream>
#include <cassert>

extern "C" int lib(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Master m;

    std::cout << "0" << std::endl;
    m.setString(QString("Hello world"));
    assert((m.getString() == QString("Hello world")));

    Child *c = m.getChild();
    c->setValue(30);
    assert((c->getValue() == 30));

    m.setValueChild(40);
    assert((c->getValue() == 40));

    return 0;
}
