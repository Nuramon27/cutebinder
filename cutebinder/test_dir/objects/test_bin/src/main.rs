use std::env;
use std::ffi;

use libc::{c_char, c_int};

extern crate model;

extern "C" {
    /// The main function of the Qt-side, provided by
    /// a C- or C++-library.
    fn lib(argc: c_int, argv: *mut *mut c_char) -> c_int;
}

fn main() {
    let argv: Vec<ffi::CString> = env::args_os()
        .into_iter()
        .map(|arg| ffi::CString::new(Vec::<u8>::from(arg.into_string().unwrap())).unwrap())
        .collect();
    let args: Vec<*const c_char> = argv.into_iter().map(|arg| arg.as_ptr()).collect();

    unsafe {
        lib(args.len() as c_int, args.as_ptr() as *mut *mut c_char);
    }
}
