use crate::interface::{self, MasterTrait, MasterEmitter, MasterCollection};

use chrono::NaiveDate;
use cutebinder_helper::void;

pub struct Master {
    value: i32,
    date: NaiveDate,
    s: String,
    emitter: MasterEmitter,
}

impl MasterTrait for Master {
    fn new_default(signal_struct: MasterCollection) -> Master {
        Master {
            value: 0,
            date: Default::default(),
            s: String::new(),
            emitter: signal_struct.emitter,
        }
    }

    fn from_string(s: String, signal_struct: MasterCollection) -> Master {
        Master {
            value: s.len() as i32,
            date: NaiveDate::from_ymd(2000, 10, 10),
            s,
            emitter: signal_struct.emitter
        }
    }

    fn get_value(&self) -> i32 {
        self.value
    }

    fn set_value(&mut self, value: i32) {
        self.value = value;
    }

    fn get_value_by_ref(&self) -> &i32 {
        &self.value
    }

    fn get_string(&self) -> String {
        self.s.clone()
    }

    fn set_string(&mut self, value: String) {
        self.s = value;
    }

    fn get_date(&self) -> NaiveDate {
        self.date
    }

    fn set_date(&mut self, date: NaiveDate) {
        self.date = date
    }

    fn send_all_signals(&mut self) {
        self.emitter.wake_up();
        self.emitter.send_value(30);
    }
    fn object_qt(&mut self) -> *mut void {
        self.emitter.ptr
    }
}