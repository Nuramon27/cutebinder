#include <bdg/Bindings.hpp>
#include <bdg/helper.hpp>

#include <QApplication>
#include <iostream>
#include <cassert>

extern "C" int lib(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QObject *q = nullptr;
    Master m;

    m.setValue(20);
    assert((m.getValue() == 20));
    m.setDate(QDate(2012, 12, 20));
    assert((m.getDate() == QDate(2012, 12, 20)));
    m.setString(QString("Hello world"));
    assert((m.getString() == QString("Hello world")));

    bool woken_up = false;

    QObject::connect(&m, &Master::wakeUp, [&]() {
        woken_up = true;
    });

    qint32 value_ext = 100;
    QObject::connect(&m, &Master::sendValue, [&](qint32 value) {
        value_ext = value;
    });

    m.sendAllSignals();

    assert(woken_up);
    assert((value_ext == 30));

    return 0;
}
