use std::iter;

use std::fmt;
use std::io::{self, Write};

use bitflags::bitflags;

use crate::{brace, indented, paren, statement_cpp, statement_rust, token_cpp, token_rust};

use super::expressions::{FunctionCall, RValue};
use super::references::{CallMode, RefMode};
use super::statements::{Initialization, Ret};
use super::types::{PrimitiveType::Void, Type, Type::Primitive};
use super::{Context, Repr};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum Initializer {
    Nullptr,
    Creation { name: String, value: FunctionCall },
}
