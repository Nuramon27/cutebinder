//! The reference module contains structs that describe how
//! variables are referenced, in C++ and Rust expressed
//! by combinations of the characters '*' an '&'.
//!
//! When talking about referencing, we need to differ between
//! three different things:
//!
//! 1. The reference and pointer operators as type constructors,
//!     i.e. what you use in the declaration of a type.
//!     These are represented by [`CallMode`].
//!
//! 2. The reference mode when a value is taken,
//!     i.e. a '&' in front of an expression.
//!     These are represented by [`RefMode`].
//!
//! 3. The reference mode when assigning to a value,
//!     i.e. a '*' in front of an lvalue.
//!     These are represented by [`AssignMode`].
//!
//! _Remark: This is a heavy simplification of the full referencing system
//! of both languages, especially since it does not support nested
//! references, but it should suffice for our purposes._

use std::fmt;
use std::io::{self, Write};

use super::{Context, Repr};

/// How a variable is passed to a function.
///
/// This is used in function declarations (parameters and return value)
/// and in variable declarations.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum CallMode {
    /// The variable is passed by value (i.e. by move for rust values)
    Value,
    /// An immutable reference of the variable is passed
    Reference,
    /// A mutable reference of the variable is passed
    ReferenceMut,
    /// A const pointer to the variable is passed
    Pointer,
    /// A mutable pointer to the variable is passed
    PointerMut,
}

impl From<AssignMode> for CallMode {
    fn from(other: AssignMode) -> Self {
        match other {
            AssignMode::Value => CallMode::Value,
            AssignMode::ReferenceMut => CallMode::ReferenceMut,
            AssignMode::PointerMut => CallMode::PointerMut,
        }
    }
}

impl<W: Write> Repr<W> for CallMode {
    fn cpp(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        match self {
            CallMode::Value => Ok(()),
            CallMode::Reference => write!(w, "const &"),
            CallMode::ReferenceMut => write!(w, "&"),
            CallMode::Pointer => write!(w, "const *"),
            CallMode::PointerMut => write!(w, "*"),
        }
    }
    fn rust(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        match self {
            CallMode::Value => Ok(()),
            CallMode::Reference => write!(w, "&"),
            CallMode::ReferenceMut => write!(w, "&mut "),
            CallMode::Pointer => write!(w, "*const "),
            CallMode::PointerMut => write!(w, "*mut "),
        }
    }
}

impl CallMode {
    /// Turn this mode into a `CallMode` approrpriate for the ffi-boundary,
    /// i.e. turn references into the corresponding pointers.
    pub fn into_ffi_mode(self) -> Self {
        match self {
            CallMode::Value => CallMode::Value,
            CallMode::Reference | CallMode::Pointer => CallMode::Pointer,
            CallMode::ReferenceMut | CallMode::PointerMut => CallMode::PointerMut,
        }
    }
    /// Turns a `Value` into a mutable reference.
    pub fn by_reference(self) -> Self {
        match self {
            CallMode::Value => CallMode::ReferenceMut,
            x => x,
        }
    }
    /// Turns const references and pointers into mutable references and pointers.
    pub fn as_mutable(self) -> Self {
        match self {
            CallMode::Value => CallMode::Value,
            CallMode::Reference | CallMode::ReferenceMut => CallMode::ReferenceMut,
            CallMode::Pointer | CallMode::PointerMut => CallMode::PointerMut,
        }
    }
}

/// How the value of a variable is taken.
///
/// This is used when taking the value of a variable
/// (e.g. on the right hand side of an assignment)
/// and when passing a variable to a function.
///
/// Recall that here you have an additional possibility
/// to [`CallMode`]: you can not only take use the value
/// of a variable directly or take its adress, but also
/// use the value to which an adress points, i.e. dereference it.
///
/// Furthermore, `RefMode` is a bit more complicated
/// because it cannot simply by expressed by a postfix _or_ a prefix,
/// but for taking pointers in Rust you need both: like in `&mut a as *mut _`.
/// So instead of a [`Repr`](super::Repr) implementation, `RefMode`
/// provides functions [`prefix_cpp`](RefMode::prefix_cpp), [`postfix_cpp`](RefMode::postfix_cpp),
/// [`prefix_ruste`](RefMode::prefix_rust) and [`postfix_rust`](RefMode::postfix_rust)
/// that return simple static string slices.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum RefMode {
    Dereference,
    /// A Reference is cast to an immutable pointer.
    CastToPointer,
    /// A Reference ist cast to a mutable pointer.
    CastToPointerMut,
    /// The variable is taken directly
    Value,
    /// An immutable reference to the variable is taken
    Reference,
    /// A mutable reference to the variable is taken
    ReferenceMut,
    /// A const pointer to the variable is taken
    Pointer,
    /// A mutable pointer to the variable is taken
    PointerMut,
}

impl From<CallMode> for RefMode {
    fn from(other: CallMode) -> Self {
        match other {
            CallMode::Value => RefMode::Value,
            CallMode::Reference => RefMode::Reference,
            CallMode::ReferenceMut => RefMode::ReferenceMut,
            CallMode::Pointer => RefMode::Pointer,
            CallMode::PointerMut => RefMode::PointerMut,
        }
    }
}

impl RefMode {
    /// Returns whether the value is used in a mutable way.
    pub fn is_mutable(self) -> bool {
        match self {
            RefMode::Dereference
            | RefMode::Value
            | RefMode::Reference
            | RefMode::Pointer
            | RefMode::CastToPointer => false,
            RefMode::ReferenceMut | RefMode::PointerMut | RefMode::CastToPointerMut => true,
        }
    }
}

impl RefMode {
    /// The prefix used in C++ to express the reference mode `self`.
    pub fn prefix_cpp(&self) -> &'static str {
        match self {
            RefMode::Dereference => "*",
            RefMode::Value => "",
            RefMode::Reference => "",
            RefMode::ReferenceMut => "",
            RefMode::Pointer
            | RefMode::PointerMut
            | RefMode::CastToPointer
            | RefMode::CastToPointerMut => "&",
        }
    }
    /// The postfix used in C++ to express the reference mode `self`.
    pub fn postfix_cpp(&self) -> &'static str {
        ""
    }
    /// The prefix used in Rust to express the reference mode `self`.
    pub fn prefix_rust(&self) -> &'static str {
        match self {
            RefMode::Dereference => "*",
            RefMode::Value => "",
            RefMode::Reference => "&",
            RefMode::ReferenceMut => "&mut ",
            RefMode::Pointer => "&",
            RefMode::PointerMut => "&mut ",
            RefMode::CastToPointer | RefMode::CastToPointerMut => "",
        }
    }
    /// The postfix used in Rust to express the reference mode `self`.
    pub fn postfix_rust(&self) -> &'static str {
        match self {
            RefMode::Dereference | RefMode::Value | RefMode::Reference | RefMode::ReferenceMut => {
                ""
            }
            RefMode::Pointer | RefMode::CastToPointer => " as *const _",
            RefMode::PointerMut | RefMode::CastToPointerMut => " as *mut _",
        }
    }
}

/// This needs to be understood as "Reverting the given CallMode",
/// i.e. when you have a `PointerMut` as assignmode,
/// you know that for assigning you need to dereference a pointer.
///
/// So `AssignMode` is kind of mirror-like to [`CallMode`],
/// only that it does not contain constant references and pointers.
///
/// This works because you cannot write to the address of a value,
/// so you do not need a `Dereference` variant.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum AssignMode {
    /// Write directly to the value
    Value,
    /// Dereference a reference and write to it.
    ///
    /// Recall that in C++ this means you don't do nothing.
    ReferenceMut,
    /// Dereference a pointer and write to it.
    PointerMut,
}

impl<W: Write> Repr<W> for AssignMode {
    fn cpp(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        match self {
            AssignMode::Value => Ok(()),
            AssignMode::ReferenceMut => Ok(()),
            AssignMode::PointerMut => write!(w, "*"),
        }
    }
    fn rust(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        match self {
            AssignMode::Value => Ok(()),
            AssignMode::ReferenceMut => write!(w, "*"),
            AssignMode::PointerMut => write!(w, "*"),
        }
    }
}
