//! The `types` module contains the types which can be passed betweeen
//! the ffi-boundary.

mod converted_struct;
mod ffi;
mod function_pointer;
mod generic_one;
mod object;
mod primitive;
mod sliceable;
mod structure;

mod conversion_func_creator;

use std::collections::BTreeMap;
use std::fmt;
use std::io::{self, Write};
use std::iter;
use std::iter::FromIterator;

use crate::elements::functions::Parameter;
use crate::mangle;
use crate::{brace, paren, statement_cpp, statement_rust, token_cpp, token_rust};
use conversion_func_creator::ConversionFuncCreator;

use super::expressions::{FunctionCall, RValue};
use super::functions::{self, FunctionHead};
use super::references::{CallMode, RefMode};
use super::statements::Assignment;
use super::{Context, Repr};

pub use converted_struct::StructType;
pub use ffi::FFIType;
pub use function_pointer::FunctionPointer;
pub use generic_one::{GenericOneKind, GenericOneType};
pub use object::{ObjectCppType, ObjectRustType, ObjectType};
pub use primitive::PrimitiveType;
pub use sliceable::SliceableType;
pub use structure::StructFFIType;

/// In cutebinder, types are differentiated by how they need to be passed through
/// the ffi-boundary.
///
/// Primitive types may simply be passed by value, but some types
/// need to be decomposed or converted before or after being passed
/// through ffi.
///
/// Generally there are three directions in which a value can be passed
/// through the ffi-boundary: From C++ to Rust, from Rust to C++, or as
/// a return value fro Rust to C++.
///
/// In the most complicated case, for each direction
/// three pieces of information are necessary: How to prepare
/// a value before passing it through the boundary, how to resume it
/// from the primitive values passed, and the primitive types
/// that are involved in this process. Furthermore one needs
/// naming conventions about how to name the parameters
/// on the ffi-boundary.
///
/// Functions which can be called from ffi are called "interface functions".
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum Type {
    /// A type that is a primitive in both languages
    Primitive(PrimitiveType),
    /// A type that is only used on the ffi-boundary
    /// but cannot be specified by the user
    FFI(FFIType),
    /// A type that can be represented by a slice of elements
    /// of a primitive type.
    ///
    /// From C++ to rust these are passed as slices, from rust to C++ conversion
    /// functions are used to construct the qt-version of a type.
    Sliceable(SliceableType),
    /// A type that is converted into a transfer struct on the FFI boundary.
    ConvertedStruct(StructType),
    GenericOne(GenericOneType),
    /// A function pointer.
    Function(Box<FunctionPointer>),
    /// An FFI-safe struct
    Structure(StructFFIType),
    /// A QObject with a certain class-name
    Object(ObjectType),
    ObjectCpp(ObjectCppType),
    ObjectRust(ObjectRustType),
}

impl<W: Write> Repr<W> for Type {
    fn cpp(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        match self {
            Type::Primitive(ty) => ty.cpp(w, cont),
            Type::FFI(ty) => ty.cpp(w, cont),
            Type::Sliceable(ty) => ty.cpp(w, cont),
            Type::ConvertedStruct(ty) => ty.cpp(w, cont),
            Type::GenericOne(ty) => ty.cpp(w, cont),
            Type::Function(ty) => ty.cpp(w, cont),
            Type::Structure(ty) => ty.cpp(w, cont),
            Type::Object(ty) => ty.cpp(w, cont),
            Type::ObjectCpp(ty) => ty.cpp(w, cont),
            Type::ObjectRust(ty) => ty.cpp(w, cont),
        }
    }
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        match self {
            Type::Primitive(ty) => ty.rust(w, cont),
            Type::FFI(ty) => ty.rust(w, cont),
            Type::Sliceable(ty) => ty.rust(w, cont),
            Type::ConvertedStruct(ty) => ty.rust(w, cont),
            Type::GenericOne(ty) => ty.rust(w, cont),
            Type::Function(ty) => ty.rust(w, cont),
            Type::Structure(ty) => ty.rust(w, cont),
            Type::Object(ty) => ty.rust(w, cont),
            Type::ObjectCpp(ty) => ty.rust(w, cont),
            Type::ObjectRust(ty) => ty.rust(w, cont),
        }
    }
}

/// This implementation writes the representation
/// of a [`Type`] declaration which is referenced according to
/// a given [`CallMode`](super::references::CallMode).
impl<W: Write> Repr<W> for (&CallMode, &Type) {
    fn cpp(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        token_cpp!(w, cont, self.1, " ");
        token_cpp!(w, cont, self.0);
        Ok(())
    }
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        token_rust!(w, cont, self.0);
        token_rust!(w, cont, self.1);
        Ok(())
    }
}

/// This implementation writes the representation
/// of a [`Type`] declaration which is referenced according to
/// a given [`CallMode`](super::references::CallMode).
impl<W: Write> Repr<W> for (CallMode, Type) {
    fn cpp(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        (&self.0, &self.1).cpp(w, cont)
    }
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        (&self.0, &self.1).rust(w, cont)
    }
}

pub trait TypeTrait: Repr<Vec<u8>> {
    fn name_cpp(&self) -> String {
        let mut res = Vec::new();
        self.cpp(&mut res, &mut Context::new()).unwrap();
        String::from_utf8(res).unwrap()
    }
    fn name_rust(&self) -> String {
        let mut res = Vec::new();
        self.rust(&mut res, &mut Context::new()).unwrap();
        String::from_utf8(res).unwrap()
    }
    fn as_type(&self) -> Type;
    /// Tells into which type (and mode) `self` is converted on the interface stage
    /// in direction C++ -> Rust and C++ <- Rust.
    fn ffi_type_cpp_rust(&self, mode: CallMode) -> (CallMode, Type) {
        (mode.into_ffi_mode(), self.as_type())
    }

    /// Tells into which type (and mode) `self` is converted on the interface stage
    /// in direction Rust -> C++.
    fn ffi_type_rust_cpp(&self, mode: CallMode) -> (CallMode, Type) {
        self.ffi_type_cpp_rust(mode)
    }
    /// Tells which type the output parameter has to which the result will
    /// be written
    fn output_type(&self) -> Option<Type> {
        None
    }
    /// Tells how to decompose a variable on the C++ side with name
    /// `expr` that is to be referenced according to `mode`
    /// in order to pass it to the rust side.
    ///
    /// This returns an [`RValuePack`](super::expressions::RValuePack)
    /// because it may be necessary to decompose the variable into its parts.
    ///
    /// Used for direction C++ -> Rust, preparation.
    fn convert_cpp_rust(&self, expr: &str, mode: RefMode) -> RValue {
        RValue::new(expr.to_string(), mode)
    }
    /// Tells how to decompose a variable on the Rust side with name
    /// `expr` that is to be referenced according to `mode`
    /// in order to pass it to the C++ side.
    ///
    /// This returns an [`RValuePack`](super::expressions::RValuePack)
    /// because it may be necessary to decompose the variable into its parts.
    ///
    /// Used for directions Rust -> C++ and C++ <- Rust, preparation.
    fn convert_rust_cpp(&self, expr: RValue) -> RValue {
        expr
    }

    fn initialize_output_param(&self) -> Option<RValue> {
        None
    }
    fn set_output_param(
        &self,
        _expr: &str,
        _mode: RefMode,
        _w: &mut dyn Write,
        _cont: &mut Context,
    ) -> Result<(), io::Error> {
        Ok(())
    }

    /// Tells how to compose a variable on the rust side
    /// from the information passed through the ffi-boundary if the
    /// variable has name `expr` on the C++ side and is referenced according
    /// to `mode`.
    ///
    /// Used for direction C++ -> Rust, resuming.
    fn resume_rust(&self, expr: &str, mode: RefMode) -> RValue {
        RValue::new(expr.to_string(), mode)
    }
    /// Tells how to compose a variable on the C++ side
    /// from the information passed through the ffi-boundary if the
    /// variable has name `expr` on the Rust side and is referenced according
    /// to `mode`.
    ///
    /// Used for direction Rust -> C++, resuming.
    fn resume_cpp(&self, expr: &str, mode: RefMode) -> RValue {
        RValue::new(expr.to_string(), mode)
    }
    /// Returns the conversion functions necessary in order
    /// to pass the type from rust back to C++.
    ///
    /// Used for direction C++ <- Rust, preparation and declaration.
    fn conversion_functions(&self) -> BTreeMap<String, Type> {
        BTreeMap::new()
    }
    fn conversion_func_creator(&self) -> Option<ConversionFuncCreator> {
        None
    }
}

impl TypeTrait for Type {
    fn as_type(&self) -> Type {
        self.clone().into()
    }
    fn ffi_type_cpp_rust(&self, mode: CallMode) -> (CallMode, Type) {
        self.as_dyn().ffi_type_cpp_rust(mode)
    }

    fn ffi_type_rust_cpp(&self, mode: CallMode) -> (CallMode, Type) {
        self.as_dyn().ffi_type_rust_cpp(mode)
    }

    fn convert_cpp_rust(&self, expr: &str, mode: RefMode) -> RValue {
        self.as_dyn().convert_cpp_rust(expr, mode)
    }
    fn convert_rust_cpp(&self, expr: RValue) -> RValue {
        self.as_dyn().convert_rust_cpp(expr)
    }
    fn initialize_output_param(&self) -> Option<RValue> {
        self.as_dyn().initialize_output_param()
    }
    fn set_output_param(
        &self,
        expr: &str,
        mode: RefMode,
        w: &mut dyn Write,
        cont: &mut Context,
    ) -> Result<(), io::Error> {
        self.as_dyn().set_output_param(expr, mode, w, cont)
    }

    fn resume_rust(&self, expr: &str, mode: RefMode) -> RValue {
        self.as_dyn().resume_rust(expr, mode)
    }
    fn resume_cpp(&self, expr: &str, mode: RefMode) -> RValue {
        self.as_dyn().resume_cpp(expr, mode)
    }
    fn conversion_functions(&self) -> BTreeMap<String, Type> {
        self.as_dyn().conversion_functions()
    }
    fn output_type(&self) -> Option<Type> {
        self.as_dyn().output_type()
    }
    fn conversion_func_creator(&self) -> Option<ConversionFuncCreator> {
        self.as_dyn().conversion_func_creator()
    }
}

impl Type {
    pub fn as_dyn(&self) -> &dyn TypeTrait {
        match self {
            Type::Primitive(ty) => ty,
            Type::FFI(ty) => ty,
            Type::ConvertedStruct(ty) => ty,
            Type::Sliceable(ty) => ty,
            Type::Structure(ty) => ty,
            Type::GenericOne(ty) => ty,
            Type::Function(ty) => ty.as_ref(),
            Type::Object(ty) => ty,
            Type::ObjectCpp(ty) => ty,
            Type::ObjectRust(ty) => ty,
        }
    }
}
