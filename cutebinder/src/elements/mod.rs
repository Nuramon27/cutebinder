//! The elements module contains all the elements from which
//! the generated code is built.
//!
//! The fundamental elements are [`types`]. Furthermore, there are means
//! to reference types: Found in the [`references`] module.
//!
//! If you have types and know how to reference them,
//! you only need to add names and you get [`expressions`]
//! and [`statements`].
//!
//! But in the end, the thing you want to build with all that
//! are [`functions`].
//!
//! In the top level of this module, there are some items
//! that control how to generate code from these elements.
//! The whole clue of this module is, that all the fundamental elements, like a
//! [`Type`](types::Type) or the [head](functions::FunctionHead) of a function
//! are represented in a language agnostic form. How to convert them
//! to code is only decided in the final phase of code generation.
//!
//! This code generation is controlled by the [`Repr`] trait, whose functions
//! are usually invoked through the helper macros [`token_rust`],
//! [`token_cpp`], [`statement_rust`], [`statement_cpp`] and some more.

use std::fmt;
use std::io::{self, Write};
use std::ops::Deref;

pub mod expressions;
pub mod functions;
pub mod initializers;
pub mod references;
pub mod statements;
pub mod structs;
pub mod types;

/// The `Repr` trait expresses the ability of an element of code to be represented
/// in Rust _or_ in C++.
///
/// All the fundamental elements implement this trait.
///
/// It is a fundamental decision that the methods of this trait do not
/// return Strings, but instead write to a writer. I believe that this
/// makes their usage more flexible and in any case much more performant
/// than the naive approach of returning Strings.
///
/// That way, when composing a composed element out of multiple sub-elements,
/// in the composed element you simply call the `Repr` implementations
/// of the sub-elements one after another, maybe using the feature
/// of [`token_cpp`] and [`token_rust`] to precede or follow them by string literals.
/// If these methods would return Strings, you would have to compose them yourself
/// by invoking the `format!` macro. This would not only
/// feel like boilerplate, but also be a performance issue due to the permanent
/// new allocation of Strings.
///
/// Recall that the functions take a general writer. So if you really
/// need the representation of an element as a String, simply
/// write to one.
pub trait Repr<W: Write> {
    /// Write the C++ representation of `self` to the Writer `w`.
    fn cpp(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error>;
    /// Write the Rust representation of `self` to the Writer `w`.
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error>;
}

/// A `Context` controls formatting issues that depend on the location
/// where in a file of code an element is placed.
///
/// The most obvious information of that type is the indentation:
/// If a block of code is indented n spaces, then
/// every line of code needs to begin with this indentation anew.
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Context {
    /// The indentation level in tabs (= 4 spaces).
    indent: u16,
}

impl Context {
    /// Constructs a `Context` with empty indentation
    pub fn new() -> Self {
        Self::default()
    }
    /// Returns the indentation level in tabs (= 4 spaces).
    pub fn indent(&self) -> u16 {
        self.indent
    }
    /// Increments the indentation level by one tab.
    pub fn inc_indent(&mut self) {
        self.indent += 1
    }
    /// Decrements the indentation level by one tab.
    pub fn dec_indent(&mut self) {
        self.indent -= 1
    }
    /// Returns a Context with the indentation level decreased by one.
    ///
    /// This is necessary for things like `public:` statements in C++.
    pub fn previous(&mut self) -> Context {
        Context {
            indent: self.indent - 1,
        }
    }
}

/// Writes the C++ representation of `$elem$` to the Writer
/// `$w`, optionally preceded by a string literal `$pre`
/// or followed by a string literal `$post`.
///
/// # Example
///
/// ```
/// use std::io::{self, Write};
///
/// use cutebinder::token_cpp;
/// use cutebinder::elements::Context;
/// use cutebinder::elements::types::{Type, PrimitiveType::Void};
///
/// fn main() -> Result<(), std::io::Error> {
///     let mut res = Vec::<u8>::new();
///     let mut cont = Context::new();
///     token_cpp!(&mut res, &mut cont, r#"extern "C" "#, Type::from(Void), " ");
///     assert_eq!(String::from_utf8(res).unwrap().as_str(), r#"extern "C" void "#);
///     Ok(())
/// }
/// ```
#[macro_export]
macro_rules! token_cpp {
    ( $w:expr, $context:expr, $elem:expr ) => {
        $crate::elements::Repr::cpp(&$elem, $w, $context)?
    };
    ( $w:expr, $context:expr, $pre:literal, $elem:expr ) => {
        write!($w, $pre)?;
        $crate::elements::Repr::cpp(&$elem, $w, $context)?
    };
    ( $w:expr, $context:expr, $elem:expr, $post:literal ) => {
        $crate::elements::Repr::cpp(&$elem, $w, $context)?;
        write!($w, $post)?
    };
    ( $w:expr, $context:expr, $pre:literal, $elem:expr, $post:literal ) => {
        write!($w, $pre)?;
        $crate::elements::Repr::cpp(&$elem, $w, $context)?;
        write!($w, $post)?
    };
}

/// Writes the Rust representation of `$elem$` to the Writer
/// `$w`, optionally preceded by a string literal `$pre`
/// or followed by a string literal `$post`.
///
/// # Example
///
/// ```
/// use std::io::{self, Write};
///
/// use cutebinder::token_rust;
/// use cutebinder::elements::Context;
/// use cutebinder::elements::types::{Type, PrimitiveType::Void};
///
/// fn main() -> Result<(), std::io::Error> {
///     let mut res = Vec::<u8>::new();
///     let mut cont = Context::new();
///     token_rust!(&mut res, &mut cont, " -> ", Type::from(Void), " ");
///     assert_eq!(String::from_utf8(res).unwrap().as_str(), " -> () ");
///     Ok(())
/// }
/// ```
#[macro_export]
macro_rules! token_rust {
    ( $w:expr, $context:expr, $elem:expr ) => {
        $crate::elements::Repr::rust(&$elem, $w, $context)?
    };
    ( $w:expr, $context:expr, $pre:literal, $elem:expr ) => {
        write!($w, $pre)?;
        $crate::elements::Repr::rust(&$elem, $w, $context)?
    };
    ( $w:expr, $context:expr, $elem:expr, $post:literal ) => {
        $crate::elements::Repr::rust(&$elem, $w, $context)?;
        write!($w, $post)?
    };
    ( $w:expr, $context:expr, $pre:literal, $elem:expr, $post:literal ) => {
        write!($w, $pre)?;
        $crate::elements::Repr::rust(&$elem, $w, $context)?;
        write!($w, $post)?
    };
}

/// Writes the C++ representation of `$elem$ to the Writer `$w`,
/// with the correct indentation according to the `$context`
/// and followed by a semicolon and a new line.
#[macro_export]
macro_rules! statement_cpp {
    ( $w: expr, $context:expr, $elem:expr ) => {
        for _ in 0..$context.indent() {
            write!($w, "    ")?;
        }
        token_cpp!($w, $context, $elem, ";\n")
    };
}

/// Writes the Rust representation of `$elem$ to the Writer `$w`,
/// with the correct indentation according to the `$context`
/// and followed by a semicolon and a new line.
#[macro_export]
macro_rules! statement_rust {
    ( $w: expr, $context:expr, $elem:expr ) => {
        for _ in 0..$context.indent() {
            write!($w, "    ")?;
        }
        token_rust!($w, $context, $elem, ";\n");
    };
}

/// Precedes `$content` by the correct indentation according
/// to `$context`.
///
/// # Example
///
/// ```
/// use std::io::{self, Write};
///
/// use cutebinder::elements::Context;
/// use cutebinder::{indented, token_rust};
///
/// fn main() -> Result<(), std::io::Error> {
///     let mut cont = Context::new();
///     cont.inc_indent();
///     let mut res = Vec::<u8>::new();
///     indented!(&mut res, cont, token_rust!(&mut res, &mut cont, "#[no_mangle]"));
///     assert_eq!(String::from_utf8(res).unwrap().as_str(), "    #[no_mangle]");
///     Ok(())
/// }
/// ```
#[macro_export]
macro_rules! indented {
    ( $w: expr, $context:expr, $content:stmt $(, $post:literal )? ) => {
        for _ in 0..$context.indent() {
            write!($w, "    ")?;
        }
        $content
        $(
            write!($w, $post)?;
        )?
    };
}

/// Encloses `$content` in parentheses '()', possibly followed
/// by `$post`.
///
/// Recall that here `$content` is not an expression that implements
/// [`Repr`](crate::elements::Repr) but a final statement writing something to the writer
/// `$w`.
///
/// # Example
///
/// ```
/// use std::io::{self, Write};
///
/// use cutebinder::{paren, token_rust};
/// use cutebinder::elements::Context;
/// use cutebinder::elements::types::{Type, PrimitiveType::Quint16};
///
/// fn main() -> Result<(), std::io::Error> {
///     let mut res = Vec::<u8>::new();
///     let mut cont = Context::new();
///     paren!(&mut res, token_rust!(&mut res, &mut cont, vec![Quint16, Quint16]); ";");
///     assert_eq!(String::from_utf8(res).unwrap().as_str(), "(u16, u16);");
///     Ok(())
/// }
/// ```
#[macro_export]
macro_rules! paren {
    ($w:expr, $content:stmt; $( $post:literal )? ) => {
        write!($w, "(")?;
        $content
        write!($w, ")")?;
        $(
            write!($w, $post)?;
        )?
    };
}

/// Encloses `$content` in curly braces, with the indentation according
/// to `$context` increased by one tab and possibly followed by `$pre`.
///
/// Recall that here `$content` are not expressions that implement
/// [`Repr`](crate::elements::Repr) but a statements writing something to the writer
/// `$w`.
///
/// # Example
///
/// ```
/// use std::io::{self, Write};
///
/// use cutebinder::{brace, token_rust, statement_rust, indented};
/// use cutebinder::elements::Context;
/// use cutebinder::elements::types::{Type, PrimitiveType::Quint16};
/// use cutebinder::elements::references::AssignMode;
/// use cutebinder::elements::statements::Assignment;
///
/// fn main() -> Result<(), std::io::Error> {
///     let mut cont = Context::new();
///     let mut res = Vec::<u8>::new();
///     let assignment = Assignment::new("checked".to_string(), AssignMode::Value, "false");
///     brace!{
///         &mut res, cont,
///         indented!(&mut res, &mut cont, token_rust!(&mut res, &mut cont, "unsafe", " "));
///         statement_rust!(&mut res, &mut cont, assignment);
///     };
///     assert_eq!(
///         String::from_utf8(res).unwrap().as_str(),
///         concat!(
///             "unsafe {\n",
///             "    checked = false;\n",
///             "}\n"
///         )
///     );
///     Ok(())
/// }
/// ```
#[macro_export]
macro_rules! brace {
    ($w:expr, $context:expr, $( $pre:stmt )? $(; $content:stmt)* $(=> $postfix:literal)? $(;)?) => {
        for _ in 0..$context.indent() {
            write!($w, "    ")?;
        }
        $(
            $pre
        )?
        write!($w, "{{\n")?;
        $context.inc_indent();
        $(
            $content
        )*
        $context.dec_indent();
        for _ in 0..$context.indent() {
            write!($w, "    ")?;
        }
        write!($w, "}}")?;
        $(
            write!($w, $postfix)?;
        )?
        write!($w, "\n")?;
    };
}

impl<W: Write> Repr<W> for () {
    fn cpp(&self, _w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        Ok(())
    }
    fn rust(&self, _w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        Ok(())
    }
}

impl<T: Repr<W> + ?Sized, W: Write> Repr<W> for &T {
    fn cpp(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        (*self).cpp(w, cont)
    }
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        (*self).rust(w, cont)
    }
}

impl<T: Repr<W> + ?Sized, W: Write> Repr<W> for Box<T> {
    fn cpp(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        self.deref().cpp(w, cont)
    }
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        self.deref().rust(w, cont)
    }
}

impl<W: Write> Repr<W> for str {
    fn cpp(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        write!(w, "{}", self)
    }
    fn rust(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        write!(w, "{}", self)
    }
}

impl<W: Write> Repr<W> for String {
    fn cpp(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        write!(w, "{}", self)
    }
    fn rust(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        write!(w, "{}", self)
    }
}

/// The `Repr` imelementation on slices writes the elements of the
/// slice as a comma separated list, without a trailing comma.
impl<T: Repr<W>, W: Write> Repr<W> for [T] {
    fn cpp(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        let mut iter = self.into_iter();
        if let Some(it) = iter.next() {
            token_cpp!(w, cont, *it);
        }
        for it in iter {
            token_cpp!(w, cont, ", ", *it);
        }
        Ok(())
    }
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        let mut iter = self.into_iter();
        if let Some(it) = iter.next() {
            token_rust!(w, cont, *it);
        }
        for it in iter {
            token_rust!(w, cont, ", ", *it);
        }
        Ok(())
    }
}

/// The `Repr` imelementation on `Vec<T>` writes the elements of the
/// slice as a comma separated list, without a trailing comma.
impl<T: Repr<W>, W: Write> Repr<W> for Vec<T> {
    fn cpp(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        let mut iter = self.iter();
        if let Some(it) = iter.next() {
            token_cpp!(w, cont, *it);
        }
        for it in iter {
            token_cpp!(w, cont, ", ", *it);
        }
        Ok(())
    }
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        let mut iter = self.iter();
        if let Some(it) = iter.next() {
            token_rust!(w, cont, *it);
        }
        for it in iter {
            token_rust!(w, cont, ", ", *it);
        }
        Ok(())
    }
}
