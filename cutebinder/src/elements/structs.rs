//! The structs module contains representations of
//! [C++ classes](StructCpp) and [Rust structs](StructRust).

use std::fmt;
use std::io::{self, Write};

use bitflags::bitflags;

use crate::{brace, indented, statement_cpp, token_cpp, token_rust};

use super::functions::{FunctionBodyCpp, FunctionBodyInterfaceRust, FunctionHead};
use super::references::CallMode;
use super::types::Type;
use super::{Context, Repr};

bitflags! {
    /// Additional attributes to a function regarding
    /// things like visibility and safety.
    pub struct Attribute: u8 {
        const NONE   = 0x00;
        const REPR_C = 0x01;
    }
}

/// Either Public or Private
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Publicity {
    Private,
    Protected,
    Public,
}

/// A member variable of a structure.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct MemberVariable {
    /// The name of the variable
    pub name: String,
    /// The type of the variable
    pub ty: Type,
    /// The mode of the variable
    pub mode: CallMode,
    /// Whether its a public variable or not
    pub publicity: Publicity,
}

impl MemberVariable {
    pub fn new(name: String, ty: Type, mode: CallMode, publicity: Publicity) -> Self {
        Self {
            name,
            ty,
            mode,
            publicity,
        }
    }
}

impl<W: Write> Repr<W> for MemberVariable {
    fn cpp(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        if let Type::Function(head) = &self.ty {
            head.decl_cpp(&self.name, self.mode, w, cont)?;
        } else {
            token_cpp!(w, cont, (&self.mode, &self.ty), " ");
            token_cpp!(w, cont, self.name);
        }
        Ok(())
    }
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        match self.publicity {
            Publicity::Private => (),
            Publicity::Protected => panic!("Rust members cannot be protected!"),
            Publicity::Public => write!(w, "pub ")?,
        };
        token_rust!(w, cont, self.name, ": ");
        token_rust!(w, cont, (&self.mode, &self.ty));
        Ok(())
    }
}

/// A C++-class
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct StructCpp {
    /// The name of the class
    pub name: String,
    /// The class from which it inherits
    pub base_class: Option<String>,
    /// Whether this is a QObject with signals and slots
    pub is_object: bool,
    pub attributes: Attribute,
    pub member_vars_private: Vec<MemberVariable>,
    pub member_vars_protected: Vec<MemberVariable>,
    pub member_vars_public: Vec<MemberVariable>,
    pub member_funcs_private: Vec<FunctionBodyCpp>,
    pub member_funcs_protected: Vec<FunctionBodyCpp>,
    pub member_funcs_public: Vec<FunctionBodyCpp>,
    pub signals: Vec<FunctionHead>,
}

impl StructCpp {
    /// Creates a new C++ struct with nothing else than its name `name`.
    ///
    /// It can then be incrementally built via the builder functions
    /// [`object`](StructCpp::object), [`base_class`](StructCpp::base_class),
    /// [`member_var`](StructCpp::member_var), [`member_func`](StructCpp::member_func),
    /// [`signals`](StructCpp::signals)
    pub fn new(name: String) -> Self {
        StructCpp {
            name,
            base_class: None,
            attributes: Attribute::empty(),
            is_object: false,
            member_vars_private: Vec::new(),
            member_vars_protected: Vec::new(),
            member_vars_public: Vec::new(),
            member_funcs_private: Vec::new(),
            member_funcs_protected: Vec::new(),
            member_funcs_public: Vec::new(),
            signals: Vec::new(),
        }
    }
    /// Declares that `self` is QObject with signals and slots.
    pub fn object(mut self) -> Self {
        self.is_object = true;
        self
    }
    /// Declares the base class of `self`
    pub fn base_class(mut self, base_class: String) -> Self {
        self.base_class = Some(base_class);
        self
    }
    pub fn attribute(mut self, attribute: Attribute) -> Self {
        self.attributes |= attribute;
        self
    }
    /// Adds member variables `members` with `publicity`.
    pub fn member_var<I: IntoIterator<Item = MemberVariable>>(mut self, members: I) -> Self {
        for member in members {
            match member.publicity {
                Publicity::Private => self.member_vars_private.push(member),
                Publicity::Protected => self.member_vars_protected.push(member),
                Publicity::Public => self.member_vars_public.push(member),
            }
        }
        self
    }
    /// Adds member functions `members` with `publicity`.
    pub fn member_func<I: IntoIterator<Item = FunctionBodyCpp>>(
        mut self,
        publicity: Publicity,
        members: I,
    ) -> Self {
        match publicity {
            Publicity::Private => self.member_funcs_private.extend(members),
            Publicity::Protected => self.member_funcs_protected.extend(members),
            Publicity::Public => self.member_funcs_public.extend(members),
        }
        self
    }
    /// Adds `signals` to `self`.
    ///
    /// If `signals` is non-empty, this automatically turns `self` into a QObject.
    pub fn signals<I: IntoIterator<Item = FunctionHead>>(mut self, signals: I) -> Self {
        self.signals.extend(signals);
        if !self.signals.is_empty() {
            self.is_object = true;
        }
        self
    }
}

impl StructCpp {
    /// Writes the declaration of `self` to `w`.
    pub fn declaration<W: Write>(
        &self,
        cont: &mut Context,
        w: &mut W,
    ) -> Result<(), std::io::Error> {
        if let Some(base_class) = &self.base_class {
            indented!(
                w,
                cont,
                write!(w, "class {}: public {}", self.name, base_class)?,
                "\n"
            );
        } else {
            indented!(w, cont, write!(w, "class {}", self.name)?, "\n");
        }
        brace! {
            w, cont,
            ();
            if self.is_object {
                indented!(w, cont, write!(w, "Q_OBJECT")?, "\n");
            };
            indented!(w, cont.previous(), write!(w, "private:")?, "\n");
            for member_var in &self.member_vars_private {
                statement_cpp!(w, cont, member_var);
            };
            indented!(w, cont.previous(), write!(w, "protected:")?, "\n");
            for member_var in &self.member_vars_protected {
                statement_cpp!(w, cont, member_var);
            };
            indented!(w, cont.previous(), write!(w, "public:")?, "\n");
            for member_var in &self.member_vars_public {
                statement_cpp!(w, cont, member_var);
            };
            indented!(w, cont.previous(), write!(w, "private:")?, "\n");
            for member_func in &self.member_funcs_private {
                member_func.decl(w, cont)?;
            };
            indented!(w, cont.previous(), write!(w, "protected:")?, "\n");
            for member_func in &self.member_funcs_protected {
                member_func.decl(w, cont)?;
            };
            indented!(w, cont.previous(), write!(w, "public:")?, "\n");
            for member_func in &self.member_funcs_public {
                member_func.decl(w, cont)?;
            };
            indented!(w, cont.previous(), write!(w, "signals:")?, "\n");
            for signal in &self.signals {
                statement_cpp!(w, cont, signal);
            }=>";"
        };
        Ok(())
    }
    /// Writes the implementation of all member functions of `self` to `w`.
    pub fn implementation<W: Write>(
        &self,
        cont: &mut Context,
        w: &mut W,
    ) -> Result<(), std::io::Error> {
        for member_func in &self.member_funcs_private {
            member_func.block(w, cont)?;
        }
        for member_func in &self.member_funcs_protected {
            member_func.block(w, cont)?;
        }
        for member_func in &self.member_funcs_public {
            member_func.block(w, cont)?;
        }
        Ok(())
    }
}

/// A Rust struct
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct StructRust {
    /// The name of the struct
    pub name: String,
    pub attributes: Attribute,
    /// The member variables of the struct
    pub member_vars: Vec<MemberVariable>,
    /// The member functions of the struct
    pub member_funcs: Vec<FunctionBodyInterfaceRust>,
    /// Freestanding functions which are asoccioted with this struct.
    pub freestanding_funcs: Vec<FunctionBodyInterfaceRust>,
    pub derives: Vec<String>,
}

impl StructRust {
    /// Creates a new Rust struct with nothing else than its name `name`.
    ///
    /// It can then be incrementally built via the builder functions
    /// [`object`](StructCpp::object), [`base_class`](StructCpp::base_class),
    /// [`member_var`](StructCpp::member_var), [`member_func`](StructCpp::member_func),
    /// [`signals`](StructCpp::signals)
    pub fn new(name: String) -> Self {
        StructRust {
            name,
            attributes: Attribute::empty(),
            member_vars: Vec::new(),
            member_funcs: Vec::new(),
            freestanding_funcs: Vec::new(),
            derives: vec!["Debug".to_owned(), "Clone".to_owned()],
        }
    }
    pub fn attribute(mut self, attribute: Attribute) -> Self {
        self.attributes |= attribute;
        self
    }
    /// Adds member variables `members`.
    pub fn member_var<I: IntoIterator<Item = MemberVariable>>(mut self, members: I) -> Self {
        self.member_vars.extend(members);
        self
    }
    /// Adds member functions `members`.
    pub fn member_func<I: IntoIterator<Item = FunctionBodyInterfaceRust>>(
        mut self,
        members: I,
    ) -> Self {
        self.member_funcs.extend(members);
        self
    }
    /// Adds member functions `members`.
    pub fn freestanding<I: IntoIterator<Item = FunctionBodyInterfaceRust>>(
        mut self,
        members: I,
    ) -> Self {
        self.freestanding_funcs.extend(members);
        self
    }
    pub fn derive<I: IntoIterator<Item = String>>(mut self, derives: I) -> Self {
        self.derives.extend(derives);
        self
    }
}

impl StructRust {
    /// Writes the declaration of `self` to `w`.
    pub fn declaration<W: Write>(
        &self,
        cont: &mut Context,
        w: &mut W,
    ) -> Result<(), std::io::Error> {
        indented!(w, cont, token_rust!(w, cont, "#[derive("));
        for derive in &self.derives {
            write!(w, "{}, ", derive)?;
        }
        writeln!(w, ")]")?;
        if self.attributes.contains(Attribute::REPR_C) {
            indented!(w, cont, writeln!(w, "#[repr(C)]")?);
        }
        brace! {
            w, cont,
            write!(w, "pub struct {} ", self.name)?;
            for member_var in &self.member_vars {
                indented!(w, cont, token_rust!(w, cont, member_var), ",\n");
            };
        };
        Ok(())
    }
    /// Writes the implementation of all member functions of `self` to `w`.
    pub fn implementation<W: Write>(
        &self,
        cont: &mut Context,
        w: &mut W,
    ) -> Result<(), std::io::Error> {
        // Write member functions
        if !self.member_funcs.is_empty() {
            brace! {
                w, cont,
                write!(w, "impl {} ", self.name)?;
                for member_func in &self.member_funcs {
                    member_func.block(w, cont)?;
                };
            };
        }
        // Write freestanding functions
        for func in &self.freestanding_funcs {
            func.block(w, cont)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {

    use crate::elements::functions::{self, Parameter};

    use super::super::references::CallMode;
    use super::super::types::PrimitiveType;

    use super::*;

    #[test]
    fn struct_cpp() {
        let cpp_head = FunctionHead {
            name: "getNumber".to_string(),
            member_of: None,
            associate_of: None,
            parameters: vec![Parameter::new(
                "index".to_string(),
                PrimitiveType::Quint16.into(),
                CallMode::Value,
            )],
            return_mode: CallMode::Value,
            return_type: PrimitiveType::Float.into(),
            attributes: functions::Attribute::empty(),
        };
        let func = FunctionBodyCpp::CallEquivalent(cpp_head);

        let s = StructCpp::new("CppStruct".to_string())
            .member_var(vec![
                MemberVariable::new(
                    "a".to_string(),
                    PrimitiveType::Boolean.into(),
                    CallMode::Value,
                    Publicity::Private,
                ),
                MemberVariable::new(
                    "b".to_string(),
                    PrimitiveType::Qint16.into(),
                    CallMode::Pointer,
                    Publicity::Private,
                ),
            ])
            .member_func(Publicity::Public, vec![func]);

        let mut target = Vec::<u8>::new();
        s.declaration(&mut Context::new(), &mut target).unwrap();
        assert_eq!(
            String::from_utf8(target).unwrap(),
            r"class CppStruct
{
private:
    bool  a;
    qint16 const * b;
protected:
public:
private:
protected:
public:
    double getNumber(quint16 index);
signals:
};
"
        );

        let mut target = Vec::<u8>::new();
        s.implementation(&mut Context::new(), &mut target).unwrap();
        assert_eq!(
            String::from_utf8(target).unwrap(),
            r"double getNumber(quint16 index)
{
    return get_number(index);
}
"
        )
    }

    #[test]
    fn struct_rust() {
        let cpp_head = FunctionHead {
            name: "getNumber".to_string(),
            member_of: Some(("RustStruct".to_string(), CallMode::Reference)),
            associate_of: None,
            parameters: vec![Parameter::new(
                "index".to_string(),
                PrimitiveType::Quint16.into(),
                CallMode::Value,
            )],
            return_mode: CallMode::Value,
            return_type: PrimitiveType::Float.into(),
            attributes: functions::Attribute::empty(),
        };
        let func = FunctionBodyInterfaceRust::CallTraitItem(cpp_head);

        let s = StructRust::new("RustStruct".to_string())
            .member_var(vec![
                MemberVariable::new(
                    "a".to_string(),
                    PrimitiveType::Boolean.into(),
                    CallMode::Value,
                    Publicity::Private,
                ),
                MemberVariable::new(
                    "b".to_string(),
                    PrimitiveType::Qint16.into(),
                    CallMode::ReferenceMut,
                    Publicity::Private,
                ),
            ])
            .freestanding(vec![func]);

        let mut target = Vec::<u8>::new();
        s.declaration(&mut Context::new(), &mut target).unwrap();
        assert_eq!(
            String::from_utf8(target).unwrap(),
            r"#[derive(Debug, Clone, )]
pub struct RustStruct {
    a: bool,
    b: &mut i16,
}
"
        );

        let mut target = Vec::<u8>::new();
        s.implementation(&mut Context::new(), &mut target).unwrap();
        assert_eq!(
            String::from_utf8(target).unwrap(),
            r#"#[no_mangle]
pub unsafe extern "C" fn rust_struct_get_number(owner: *const RustStruct, index: u16) -> f64 {
    (*owner).get_number(index)
}
"#
        );
    }
}
