use std::fmt;
use std::io::{self, Write};

use crate::{token_cpp, token_rust};

use super::references::{AssignMode, CallMode};
use super::types::Type;
use super::{Context, Repr};

/// A return statement, i.e. a token preceded by the keyword `return`.
pub struct Ret<W: Write> {
    expr: Box<dyn Repr<W>>,
}

impl<W: Write> Ret<W> {
    pub fn from<T: Repr<W> + 'static>(other: T) -> Self {
        Ret {
            expr: Box::new(other),
        }
    }
}

impl<W: Write> Repr<W> for Ret<W> {
    fn cpp(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        write!(w, "return ")?;
        self.expr.cpp(w, cont)?;
        Ok(())
    }
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        write!(w, "return ")?;
        self.expr.cpp(w, cont)?;
        Ok(())
    }
}

/// An initialization statement, meaning a combined declaration and assignment.
pub struct Initialization<W: Write> {
    /// The name of the variable to initialize
    name: String,
    /// The reference mode belonging to `ty`
    mode: CallMode,
    /// The type of the variable to initialize.
    ty: Type,
    /// The expression with which the variable is initialized.
    expr: Box<dyn Repr<W>>,
}

impl<W: Write> Initialization<W> {
    /// Creates an `Initialization` with `name` and type `ty` of `mode`,
    /// initialized with expression `expr`.
    pub fn new<T: Repr<W> + 'static>(name: String, mode: CallMode, ty: Type, expr: T) -> Self {
        Initialization {
            name,
            mode,
            ty,
            expr: Box::new(expr),
        }
    }
}

impl<W: Write> Repr<W> for Initialization<W> {
    fn cpp(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        token_cpp!(w, cont, self.ty, " ");
        token_cpp!(w, cont, self.mode);
        token_cpp!(w, cont, self.name, "((");
        self.expr.cpp(w, cont)?;
        token_cpp!(w, cont, "))");
        Ok(())
    }
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        token_rust!(w, cont, "let ", self.name, " = ");
        self.expr.rust(w, cont)?;
        Ok(())
    }
}

/// An assignment statement, consisting of a left side
/// with a `name` and a `mode` and an expression `expr` on the right side.
pub struct Assignment<W: Write> {
    name: String,
    mode: AssignMode,
    expr: Box<dyn Repr<W>>,
}

impl<W: Write> Assignment<W> {
    /// Creates an `Assignment` assigning `expr` to the variable `name`
    /// which is dereferenced according to `mode`.
    pub fn new<T: Repr<W> + 'static>(name: String, mode: AssignMode, expr: T) -> Self {
        Assignment {
            name,
            mode,
            expr: Box::new(expr),
        }
    }
}

impl<W: Write> Repr<W> for Assignment<W> {
    fn cpp(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        token_cpp!(w, cont, self.mode);
        token_cpp!(w, cont, self.name, " = ");
        token_cpp!(w, cont, self.expr);
        Ok(())
    }
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        token_rust!(w, cont, self.mode);
        token_rust!(w, cont, self.name, " = ");
        token_rust!(w, cont, self.expr);
        Ok(())
    }
}
