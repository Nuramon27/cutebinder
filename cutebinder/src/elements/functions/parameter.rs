use std::io::{self, Write};

use crate::{token_cpp, token_rust};

use crate::elements::expressions::RValue;

use crate::elements::references::CallMode;

use crate::elements::types::{Type, TypeTrait};

use crate::elements::{Context, Repr};

/// A parameter of a function, consisting of a `name`,
/// a type and a call-mode.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Parameter {
    pub name: String,
    pub ty: Type,
    pub mode: CallMode,
    pub default: Option<RValue>,
}

/// Converts `mode` into the closest `CallMode` appropriate
/// for passing `ty` through the FFI boundary.
///
/// This is the corresponding pointer mode for references
/// and pointers, and a mutable pointer for a `CallMode::Value`.
///
/// Usually we disallow passing complicated types via unappropriate references.
/// However for Objects we sometimes need to act transparently
/// as if it was passed by value (because Constructors return Objects by value)
/// but have to use an appropriate mode on the FFI boundary.
pub fn mode_for_ffi(ty: &Type, mode: CallMode) -> CallMode {
    match ty {
        // Objects can only be passed by reference on the ffi boundary
        Type::Object(_) => mode.by_reference().into_ffi_mode(),
        _ => mode.into_ffi_mode(),
    }
}

impl Parameter {
    /// Constructs a parameter named `name`, of type `ty`, to be passe
    /// according to `mode`.
    pub fn new(name: String, ty: Type, mode: CallMode) -> Self {
        Parameter {
            name,
            ty,
            mode,
            default: None,
        }
    }
    /// Converts the parameters `elems` into their FFI representation
    /// for the direction C++ <-> Rust.
    pub fn convert_parameters_ffi_cpp_rust(elems: &[Parameter]) -> Vec<Parameter> {
        elems
            .iter()
            .map(|param| {
                let (mode, ty) = param.ty.ffi_type_cpp_rust(param.mode);
                Parameter {
                    name: param.name.clone(),
                    ty,
                    mode,
                    default: None,
                }
            })
            .collect()
    }
    /// Converts the parameters `elems` into their FFI representation
    /// for the direction Rust -> C++.
    pub fn convert_parameters_ffi_rust_cpp(elems: &[Parameter]) -> Vec<Parameter> {
        elems
            .iter()
            .map(|param| {
                let (mode, ty) = param.ty.ffi_type_rust_cpp(param.mode);
                Parameter {
                    name: param.name.clone(),
                    ty,
                    mode,
                    default: None,
                }
            })
            .collect()
    }
}

impl<W: Write> Repr<W> for Parameter {
    fn cpp(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        if let Type::Function(head) = &self.ty {
            head.decl_cpp(&self.name, self.mode, w, cont)?;
        } else {
            token_cpp!(w, cont, self.ty, " ");
            token_cpp!(w, cont, self.mode);
            token_cpp!(w, cont, self.name);
            if let Some(default) = &self.default {
                token_cpp!(w, cont, " = ", default);
            }
        }
        Ok(())
    }
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        if self.name == "self" {
            token_rust!(w, cont, self.mode);
            token_rust!(w, cont, self.name);
        } else {
            token_rust!(w, cont, self.name, ": ");
            token_rust!(w, cont, self.mode);
            token_rust!(w, cont, self.ty);
        }
        Ok(())
    }
}
