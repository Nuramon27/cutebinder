use std::iter::{self};

use std::io::{self, Write};

use heck::SnakeCase;

use crate::mangle;
use crate::{brace, indented, paren, statement_cpp, statement_rust, token_cpp, token_rust};

use crate::elements::expressions::{FunctionCall, RValue};

use crate::elements::references::{AssignMode, CallMode, RefMode};
use crate::elements::statements::{Assignment, Initialization};
use crate::elements::types::{PrimitiveType::Void, Type::Primitive, TypeTrait};

use crate::elements::Context;

use super::{FunctionHead, SignalCollection};

/// The implementation of a function on the Rust side.
///
/// In the simplest case, the only information you need to write the body
/// of such a function is the head of the function. These are functions that
/// simply "call through" the corresponding function in the next stage,
/// i.e. either from Interface to Rust or from Interface to C++.
///
/// In contrast to [`FunctionBodyCpp`], a [`FunctionBodyInterfaceRust`]
/// always belongs to the Interface stage -- hence the name.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum FunctionBodyInterfaceRust {
    /// Calls an equivalent trait function on the Rust stage,
    /// resuming the arguments and decomposing the return value.
    CallTraitItem(FunctionHead),
    /// Calls a Qt-signal on the Interface stage, decomposing the arguments.
    CallSignal(FunctionHead),
    /// Calls an equivalent constructor on the Rust stage
    /// resuming the arguments and turning the return value into a pointer.
    ConstructorExtern {
        type_name: String,
        head: FunctionHead,
        constructible_children: SignalCollection,
    },
    /// Takes an object created on the Rust stage and constructs
    /// a corresponding object on the C++ stage.
    Constructor {
        type_name: String,
    },
    DestructorExtern {
        type_name: String,
    },
    Destructor {
        type_name: String,
    },
}

impl FunctionBodyInterfaceRust {
    /// Writes the full definition of the function to `w`, respecting context `cont`.
    pub fn block<W: Write>(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        match self {
            FunctionBodyInterfaceRust::CallTraitItem(head) => {
                // Call the Rust function as a member function, if necessary.
                let call = if head.member_of.is_some() {
                    FunctionCall::member(
                        head.name.to_snake_case(),
                        RValue::new("owner".to_string(), RefMode::Dereference),
                        head.parameters
                            .iter()
                            .map(|param| param.ty.resume_rust(&param.name, RefMode::Value).into())
                            .collect(),
                    )
                } else {
                    FunctionCall::new(
                        head.name.to_snake_case(),
                        head.parameters
                            .iter()
                            .map(|param| param.ty.resume_rust(&param.name, RefMode::Value).into())
                            .collect(),
                    )
                };
                indented!(w, cont, token_rust!(w, cont, "#[no_mangle]"), "\n");
                brace! {
                    w, cont, token_rust!(w, cont, head.clone().into_rust_interface_head(), " ");
                    match head.return_type {
                        Primitive(Void) => { statement_rust!(w, cont, call); },
                        _ => if head.return_type.output_type().is_some() {
                            statement_rust!(w, cont, Initialization::new(
                                "ret".to_string(),
                                CallMode::Value, head.return_type.clone(), call
                            ));
                            // todo: This has to be adapted to generic types
                            head.return_type.set_output_param("ret", RefMode::Value, w, cont)?;
                        } else { indented!(w, cont, token_rust!(w, cont, head.return_type.convert_rust_cpp(RValue::call(call, RefMode::Value))), "\n"); },
                    };
                };
            }
            FunctionBodyInterfaceRust::CallSignal(head) => {
                // Decmpose the arguments and prepend the `self` pointer, which is
                // stored in the member `ptr`.
                let arguments = iter::once(
                    RValue::new("self.ptr".to_string(), RefMode::Value).into(),
                )
                .chain(head.parameters.iter().map(|param| {
                    param
                        .ty
                        .convert_rust_cpp(RValue::new(param.name.clone(), param.mode.into()))
                }));
                let call = FunctionCall::new(
                    format!("(self.{})", head.name.to_snake_case()),
                    arguments.collect(),
                );
                brace! {
                    // Needs work
                    w, cont, token_rust!(w, cont, head.clone().into_rust_signal_caller_head(), " ");
                    brace! {
                        w, cont,
                        token_rust!(w, cont, "unsafe ");
                        statement_rust!(w, cont, call);
                    };
                };
            }
            FunctionBodyInterfaceRust::ConstructorExtern {
                type_name,
                head,
                constructible_children: _,
            } => {
                let call_underlying_constructor = FunctionCall::new(
                    format!("{}::{}", type_name, head.name),
                    head.parameters
                        .iter()
                        .map(|param| param.ty.resume_rust(&param.name, param.mode.into()).into())
                        .chain(iter::once(
                            RValue::new("signal_struct".to_string(), RefMode::Value).into(),
                        ))
                        .collect(),
                );
                let call_underlying_constructor = FunctionCall::new(
                    "Box::into_raw".to_owned(),
                    vec![RValue::call(
                        FunctionCall::new(
                            "Box::new".to_owned(),
                            vec![RValue::call(call_underlying_constructor, RefMode::Value)],
                        ),
                        RefMode::Value,
                    )],
                );
                let head = head.clone().into_constructor_cpp_on_rust();

                indented!(w, cont, token_rust!(w, cont, "#[no_mangle]"), "\n");
                brace! {
                    w, cont,
                    token_rust!(w, cont, head.into_rust_interface_head(), " ");
                    indented!(w, cont, token_rust!(w, cont, call_underlying_constructor), "\n");
                };
            }
            FunctionBodyInterfaceRust::Constructor { type_name } => {
                let call_cpp_constructor = FunctionCall::new(
                    format!("(self.{})", mangle::rust_constructor_name()),
                    vec![RValue::new(
                        format!("new_{}", mangle::field_name_of_underlying()),
                        RefMode::CastToPointerMut,
                    )
                    .into()],
                );
                let copy = Initialization::new(
                    "mut emitter".to_owned(),
                    CallMode::Value,
                    Void.into(),
                    "self.emitter.clone()".to_owned(),
                );
                let assignment = Assignment::<W>::new(
                    "emitter.ptr".to_owned(),
                    AssignMode::Value,
                    call_cpp_constructor,
                );
                let owned_set_to_true = Assignment::<W>::new(
                    "emitter.owned".to_owned(),
                    AssignMode::Value,
                    RValue::new("true".to_owned(), RefMode::Value),
                );

                let head = FunctionHead::collection_constructor_on_rust(type_name);
                brace! {
                    w, cont,
                    token_rust!(w, cont, head, " ");
                    statement_rust!(w, cont, token_rust!(w, cont, copy));
                    brace! {
                        w, cont,
                        token_rust!(w, cont, "unsafe");
                        statement_rust!(w, cont, assignment);
                        statement_rust!(w, cont, owned_set_to_true);
                    };
                    indented!(w, cont, token_rust!(w, cont, "emitter"), "\n");
                }
            }
            FunctionBodyInterfaceRust::DestructorExtern { type_name } => {
                let head = FunctionHead::destructor_cpp(&type_name);
                let reconstruct_box = FunctionCall::new(
                    "Box::from_raw".to_owned(),
                    vec![RValue::new("owner".to_owned(), RefMode::Value)],
                );
                indented!(w, cont, token_rust!(w, cont, "#[no_mangle]"), "\n");
                brace! {
                    w, cont,
                    token_rust!(w, cont, head.into_rust_interface_head(), " ");
                    brace! {
                        w, cont,
                        token_rust!(w, cont, "unsafe");
                        statement_rust!(w, cont, token_rust!(w, cont, reconstruct_box));
                    }
                }
            }
            FunctionBodyInterfaceRust::Destructor { type_name } => {
                let head = FunctionHead::destructor_rust(&type_name);
                let call = FunctionCall::new(
                    format!("(self.{})", mangle::destructor_name()),
                    vec![RValue::new("self.ptr".to_owned(), RefMode::Value)],
                );
                brace! {
                    w, cont,
                    token_rust!(w, cont, head);
                    brace! {
                        w, cont,
                        token_rust!(w, cont, "if self.owned && !self.ptr.is_null() ");
                        brace! {
                            w, cont,
                            token_rust!(w, cont, "unsafe ");
                            indented!(w, cont, token_rust!(w, cont, call));
                        };
                    };
                }
            }
        };
        Ok(())
    }
}
