use std::iter::{self};

use heck::SnakeCase;

use crate::mangle;

use crate::elements::expressions::{FunctionCall, RValue, StructCreation};

use crate::elements::references::{CallMode, RefMode};

use crate::elements::structs::{self, MemberVariable, Publicity, StructCpp, StructRust};
use crate::elements::types::{FFIType, PrimitiveType, StructFFIType, Type, TypeTrait};

use super::{FunctionBodyCpp, FunctionBodyInterfaceRust, FunctionHead};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct SignalCollection {
    pub type_name: String,
    pub constructors: Vec<FunctionHead>,
    pub signals: Vec<FunctionHead>,
    pub children: Vec<SignalCollection>,
}

impl SignalCollection {
    pub fn as_struct_cpp(&self) -> StructCpp {
        StructCpp::new(mangle::mangle_signal_collection(&self.type_name))
            .attribute(structs::Attribute::REPR_C)
            .member_var(iter::once(MemberVariable {
                name: mangle::rust_constructor_name(),
                ty: FunctionHead::constructor_rust(&self.type_name)
                    .into_rust_interface_head()
                    .funcptr_type()
                    .into(),
                mode: CallMode::Pointer,
                publicity: Publicity::Public,
            }))
            .member_var(iter::once(MemberVariable {
                name: "emitter".to_owned(),
                ty: StructFFIType(mangle::mangle_emitter(&self.type_name)).into(),
                mode: CallMode::Value,
                publicity: Publicity::Public,
            }))
            .member_var(self.children.iter().map(|child| MemberVariable {
                name: child.type_name.clone().to_snake_case(),
                ty: StructFFIType(mangle::mangle_signal_collection(&child.type_name)).into(),
                mode: CallMode::Value,
                publicity: Publicity::Public,
            }))
    }
    pub fn emitter_cpp(&self) -> StructCpp {
        StructCpp::new(mangle::mangle_emitter(&self.type_name))
            .attribute(structs::Attribute::REPR_C)
            .member_var(iter::once(MemberVariable {
                name: mangle::field_name_of_underlying(),
                ty: FFIType::FFIObject.into(),
                mode: CallMode::PointerMut,
                publicity: Publicity::Public,
            }))
            .member_var(iter::once(MemberVariable {
                name: "owned".to_owned(),
                ty: PrimitiveType::Boolean.into(),
                mode: CallMode::Value,
                publicity: Publicity::Public,
            }))
            .member_var(iter::once(MemberVariable {
                name: mangle::destructor_name(),
                ty: Type::Function(Box::new(
                    FunctionHead::destructor_rust(&self.type_name)
                        .into_interface_rust_to_cpp()
                        .funcptr_type(),
                )),
                mode: CallMode::Pointer,
                publicity: Publicity::Public,
            }))
            .member_var(self.signals.iter().map(|signal| {
                MemberVariable {
                    name: mangle::mangle_rust_signal(&signal.name),
                    ty: Type::Function(Box::new(
                        signal
                            .clone()
                            .into_rust_signal_caller_head()
                            .into_interface_rust_to_cpp()
                            .funcptr_type(),
                    )),
                    mode: CallMode::Pointer,
                    publicity: Publicity::Public,
                }
            }))
    }
    pub fn as_struct_rust(&self) -> StructRust {
        StructRust::new(mangle::mangle_signal_collection(&self.type_name))
            .attribute(structs::Attribute::REPR_C)
            .member_var(iter::once(MemberVariable {
                name: mangle::rust_constructor_name(),
                ty: FunctionHead::constructor_rust(&self.type_name)
                    .into_rust_interface_head()
                    .funcptr_type()
                    .into(),
                mode: CallMode::Value,
                publicity: Publicity::Public,
            }))
            .member_var(iter::once(MemberVariable {
                name: "emitter".to_owned(),
                ty: StructFFIType(mangle::mangle_emitter(&self.type_name)).into(),
                mode: CallMode::Value,
                publicity: Publicity::Public,
            }))
            .member_var(self.children.iter().map(|child| MemberVariable {
                name: child.type_name.clone().to_snake_case(),
                ty: StructFFIType(mangle::mangle_signal_collection(&child.type_name)).into(),
                mode: CallMode::Value,
                publicity: Publicity::Public,
            }))
            .member_func(iter::once(FunctionBodyInterfaceRust::Constructor {
                type_name: self.type_name.clone(),
            }))
    }
    pub fn emitter_rust(&self) -> StructRust {
        StructRust::new(mangle::mangle_emitter(&self.type_name))
            .attribute(structs::Attribute::REPR_C)
            .member_var(iter::once(MemberVariable {
                name: "ptr".to_owned(),
                ty: FFIType::FFIObject.into(),
                mode: CallMode::PointerMut,
                publicity: Publicity::Public,
            }))
            .member_var(iter::once(MemberVariable {
                name: "owned".to_owned(),
                ty: PrimitiveType::Boolean.into(),
                mode: CallMode::Value,
                publicity: Publicity::Private,
            }))
            .member_var(iter::once(MemberVariable {
                name: mangle::destructor_name(),
                ty: Type::Function(Box::new(
                    FunctionHead::destructor_rust(&self.type_name)
                        .into_interface_rust_to_cpp()
                        .funcptr_type(),
                )),
                mode: CallMode::Value,
                publicity: Publicity::Private,
            }))
            .member_var(self.signals.iter().map(|signal| {
                MemberVariable {
                    name: mangle::mangle_rust_signal(&signal.name),
                    ty: Type::Function(Box::new(
                        signal
                            .clone()
                            .into_rust_signal_caller_head()
                            .into_interface_rust_to_cpp()
                            .funcptr_type(),
                    )),
                    mode: CallMode::Value,
                    publicity: Publicity::Private,
                }
            }))
            .member_func(iter::once(FunctionBodyInterfaceRust::Destructor {
                type_name: self.type_name.clone(),
            }))
            .member_func(self.signals.iter().map(|signal| {
                FunctionBodyInterfaceRust::CallSignal(signal.clone().name_to_snake_case())
            }))
    }
    fn instantiate_emitter(self: &SignalCollection) -> RValue {
        let this_ptr = "self_struct".to_owned();
        RValue::struct_creation(StructCreation {
            name: String::new(),
            fields: iter::once((String::new(), RValue::new(this_ptr, RefMode::Value)))
                .chain(iter::once((
                    String::new(),
                    RValue::new("false".to_owned(), RefMode::Value),
                )))
                .chain(iter::once((
                    String::new(),
                    RValue::lambda_cpp(FunctionBodyCpp::DestructorForRust {
                        type_name: self.type_name.clone(),
                    }),
                )))
                .chain(self.signals.iter().map(|signal| {
                    (
                        String::new(),
                        RValue::lambda_cpp(FunctionBodyCpp::CallUnderlyingViaLambda(
                            signal.clone().name_to_mixed_case(),
                        )),
                    )
                }))
                .collect(),
        })
    }
    pub fn instantiate_cpp(&self) -> RValue {
        RValue::struct_creation(StructCreation {
            name: String::new(),
            fields: iter::once((
                String::new(),
                RValue::lambda_cpp(FunctionBodyCpp::CallUnderlyingViaLambda(
                    FunctionHead::constructor_rust(&self.type_name),
                )),
            ))
            .chain(iter::once((String::new(), self.instantiate_emitter())))
            .chain(self.children.iter().map(|child| {
                (
                    String::new(),
                    RValue::call(
                        FunctionCall::new(
                            format!("{}::buildSignalCollection", child.type_name),
                            vec![RValue::new("nullptr".to_owned(), RefMode::Value)],
                        ),
                        RefMode::Value,
                    ),
                )
            }))
            .collect(),
        })
    }
}
