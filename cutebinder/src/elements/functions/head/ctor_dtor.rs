use crate::mangle;

use crate::elements::references::CallMode;

use crate::elements::types::{
    ObjectCppType, ObjectRustType, ObjectType, PrimitiveType::Void, StructFFIType, Type, TypeTrait,
};

use super::super::Parameter;
use super::{Attribute, FunctionHead};

impl FunctionHead {
    /// Does the tweaks necessary to go from an abstract C++ constructor
    /// to the signature of the corresponding constructor on the C++ side.
    pub fn into_constructor_cpp_on_cpp(mut self) -> Self {
        self.name = self
            .associate_of
            .clone()
            .expect("Constructors must be members");

        self
    }
    /// Does the tweaks necessary to go from an abstract constructor
    /// to the signature of the corresponding constructor on the interface stage,
    /// as seen from C++.
    ///
    /// Since Objects have different names on both sides (the type `ObjectA`
    /// on the Rust side is actually `ObjectARust` on the C++ side.)
    /// there is a special version of this function on the C++ side.
    /// The function on the rust side is obtained by using
    /// [`into_constructor_cpp_on_rust`](FunctionHead::into_constructor_cpp_on_rust)
    /// and passing it through
    /// [`into_rust_interface_head`](FunctionHead::into_rust_interface_head).
    pub fn into_constructor_cpp_on_cpp_interface(mut self) -> Self {
        let associate_of = self
            .associate_of
            .clone()
            .expect("Constructors must be members");
        self.parameters.push(Parameter::new(
            "signal_struct".to_owned(),
            StructFFIType(mangle::mangle_signal_collection(&associate_of)).into(),
            CallMode::Value,
        ));
        self
    }
    /// Does the tweaks necessary to go from an abstract C++ constructor
    /// to the signature of the corresponding constructor on the Rust side.
    pub fn into_constructor_cpp_on_rust(mut self) -> Self {
        let associate_of = self
            .associate_of
            .clone()
            .expect("Constructors must be members");
        self.return_type = ObjectType(
            self.associate_of
                .clone()
                .expect("Constructors must be members"),
        )
        .into();
        self.return_mode = CallMode::Value;
        self.parameters.push(Parameter {
            name: "signal_struct".to_string(),
            ty: StructFFIType(mangle::mangle_signal_collection(&associate_of)).into(),
            mode: CallMode::Value,
            default: None,
        });

        self
    }
    /// Returns the constructor for `type_name`, which constructs
    /// the C++ object from an already created object on the Rust side.
    pub fn constructor_rust(type_name: &str) -> Self {
        FunctionHead {
            name: type_name.to_owned(),
            member_of: None,
            associate_of: Some(type_name.to_owned()),
            parameters: vec![Parameter {
                name: format!("new_{}", mangle::field_name_of_underlying()),
                ty: ObjectRustType(type_name.to_owned()).into(),
                mode: CallMode::PointerMut,
                default: None,
            }],
            return_type: ObjectCppType(type_name.to_owned()).into(),
            return_mode: CallMode::PointerMut,
            attributes: Attribute::empty(),
        }
    }
    /// Returns the signature of the member function on the SignalCollection
    /// which takes a Rust version of `type_name` und creates the corresponding
    /// C++ object from it (via [`constructor_rust_on_rust`](FunctionHead::constructor_rust_on_rust)).
    pub fn collection_constructor_on_rust(type_name: &str) -> Self {
        FunctionHead {
            name: "produce_emitter".to_owned(),
            member_of: Some((
                mangle::mangle_signal_collection(type_name),
                CallMode::Reference,
            )),
            associate_of: None,
            parameters: vec![Parameter {
                name: format!("new_{}", mangle::field_name_of_underlying()),
                ty: ObjectRustType(type_name.to_owned()).into(),
                mode: CallMode::ReferenceMut,
                default: None,
            }],
            return_type: StructFFIType(mangle::mangle_emitter(type_name)).into(),
            return_mode: CallMode::Value,
            attributes: Attribute::PUB,
        }
    }
    pub fn destructor_cpp(type_name: &str) -> Self {
        FunctionHead {
            name: format!("~{}", type_name),
            member_of: Some((type_name.to_owned(), CallMode::ReferenceMut)),
            associate_of: None,
            parameters: Vec::new(),
            return_type: Void.into(),
            return_mode: CallMode::Value,
            attributes: Attribute::PUB | Attribute::VIRTUAL | Attribute::OVERRIDE,
        }
    }
    pub fn destructor_rust(type_name: &str) -> Self {
        FunctionHead {
            name: mangle::destructor_name(),
            member_of: Some((type_name.to_owned(), CallMode::ReferenceMut)),
            associate_of: None,
            parameters: Vec::new(),
            return_type: Void.into(),
            return_mode: CallMode::Value,
            attributes: Attribute::PUB,
        }
    }
}
