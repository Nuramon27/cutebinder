pub mod ctor_dtor;

use std::io::{self, Write};

use bitflags::bitflags;
use heck::{MixedCase, SnakeCase};

use crate::mangle;
use crate::{brace, indented, paren, statement_cpp, statement_rust, token_cpp, token_rust};

use crate::elements::expressions::RValue;

use crate::elements::references::{CallMode, RefMode};

use crate::elements::types::{
    FunctionPointer, ObjectCppType, ObjectRustType, ObjectType, PrimitiveType::Void, Type,
    TypeTrait,
};

use crate::elements::{Context, Repr};

use super::{parameter, Parameter};

bitflags! {
    /// Additional attributes to a function regarding
    /// things like visibility and safety.
    pub struct Attribute: u8 {
        /// An unsafe function
        const UNSAFE       = 0x02;
        /// A function called according to the "C" ABI
        const EXTERN_C_ABI = 0x04;
        /// A public function
        const PUB          = 0x08;
        /// A virtual C++ member function.
        const VIRTUAL      = 0x20;
        const OVERRIDE     = 0x40;
        const STATIC       = 0x80;

        const INTERFACE_FUNC =
            Attribute::UNSAFE.bits
            | Attribute::EXTERN_C_ABI.bits
            | Attribute::PUB.bits;
    }
}

/// The head of a function which contains information about its name,
/// parameters, return type and attributes.
///
/// # On Constructors
///
/// Constructing objects is a bit finicky because an object always has to exist
/// in two versions: One on the C++ stage, containing only a pointer to the Rust stage
/// and one on the Rust stage defined by the user and holding the actual data.
///
/// Both hold pointers to the other version, so no matter from which side
/// you construct them, in both cases you have to initialize
/// the pointers of both sides.
///
/// Furthermore, the Rust object needs pointers to signal caller for any signal
/// which have to be passed to its constructor.
///
/// When constructing an object from the C++ stage, this is done via a usual
/// call of equivalent functions. The `this` pointer can already be passed
/// to the underlying constructor on the Rust stage, together with an Emitter
/// holding all signals.
///
/// When constructing an object from the Rust stage, the most work will be done
/// by a completely user-defined constructor, but at the end we have to take
/// this object, create a C++ object from it (with a reference to the rust object)
/// and then initialize the pointer of the emitter with this pointer.
///
/// Because all these constructors have quite different signatures
/// there are transformation functions defined on `FunctionHead` which transform
/// an abstract constructor into the corresponding representations.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct FunctionHead {
    /// The name of the function
    pub(crate) name: String,
    /// If the function is a member function, the name of the
    /// owner and how it is referenced
    pub(crate) member_of: Option<(String, CallMode)>,
    /// If the function is a member function without a self argument,
    /// this is the name of the owner.
    ///
    /// Ignored if [`member_of`](FunctionHead::member_of) is `Some`.
    pub(crate) associate_of: Option<String>,
    /// The parameters of the function
    pub(crate) parameters: Vec<Parameter>,
    /// The mode by which the result is returned
    pub(crate) return_mode: CallMode,
    /// The return type of the function
    pub(crate) return_type: Type,
    /// Additional attributes
    pub(crate) attributes: Attribute,
}

impl FunctionHead {
    /// Returns the type of the function as a function pointer.
    pub fn funcptr_type(&self) -> FunctionPointer {
        FunctionPointer {
            params: self
                .parameters
                .iter()
                .map(|param| (param.mode, param.ty.clone()))
                .collect(),
            return_type: (self.return_mode, self.return_type.clone()),
            attributes: self.attributes,
        }
    }
    /// Convert the function head to a C++ function pointer.
    pub fn as_funcptr_cpp(&self) -> RValue {
        let name = if let Some((owner, _)) = &self.member_of {
            format!("{}::{}", owner, self.name)
        } else {
            self.name.clone()
        };
        RValue::new(name, RefMode::Pointer)
    }
    /// Convert the function head to a Rust function pointer.
    pub fn as_funcptr_rust(&self) -> RValue {
        let name = if let Some((owner, _)) = &self.member_of {
            format!("{}::{}", owner, self.name)
        } else {
            self.name.clone()
        };
        RValue::new(name, RefMode::Value)
    }
    pub fn name_to_mixed_case(mut self) -> Self {
        self.name = self.name.to_mixed_case();
        self
    }
    pub fn name_to_snake_case(mut self) -> Self {
        self.name = self.name.to_snake_case();
        self
    }

    /// Returns the signature of `self` in the corresponding Object trait.
    pub fn into_trait_item(mut self) -> Self {
        self.name = self.name.to_snake_case();
        self.attributes.remove(Attribute::PUB);

        self
    }

    /// Convert the head into a version of itself which can be used
    /// outside the scope of its owner's class.
    ///
    /// In C++, when declaring a member function inside its class,
    /// the class name must not be repeated. But when defining it outside
    /// this scope, the class name has to be added in the fashion
    /// {_class-name_}::{_function-name_}. This is expressed by converting
    /// it to a new `FunctionHead`.
    pub fn into_freestanding(self) -> Self {
        FunctionHead {
            associate_of: self.associate_of.clone(),
            name: if let Some(owner) = self
                .member_of
                .as_ref()
                .map(|(m, _)| m)
                .or(self.associate_of.as_ref())
            {
                format!("{}::{}", owner, self.name)
            } else {
                self.name
            },
            member_of: self.member_of,
            parameters: self.parameters,
            return_mode: self.return_mode,
            return_type: self.return_type,
            attributes: self.attributes
                & !Attribute::VIRTUAL
                & !Attribute::OVERRIDE
                & !Attribute::STATIC,
        }
    }
    /// Convert the head into the head of the corresponding function
    /// on the interface stage.
    ///
    /// When defined on the interface stage, the name of the owning class
    /// needs to be prepended and the arguments need to be extended
    /// to their ffi-versions.
    pub fn into_rust_interface_head(self) -> Self {
        // Expand the parameters
        let mut parameters = Parameter::convert_parameters_ffi_cpp_rust(&self.parameters);
        // If the function is a member function, prepend the `self` pointer
        // to the argument list.
        if let Some((owner, self_mode)) = &self.member_of {
            parameters.insert(
                0,
                Parameter::new(
                    "owner".to_string(),
                    ObjectRustType(owner.clone()).into(),
                    self_mode.into_ffi_mode(),
                ),
            );
        }
        let conversion_functions = self.return_type.conversion_functions();
        // If the return value is passed by mutable arguments, the actual
        // return type of the interface function is `Void`.
        if let Some(output_type) = self.return_type.output_type() {
            parameters.push(Parameter {
                name: "output".to_string(),
                mode: CallMode::PointerMut,
                ty: output_type,
                default: None,
            });
        }

        let return_mode;
        let return_type;
        if !conversion_functions.is_empty() {
            return_mode = CallMode::Value;
            return_type = Void.into();
        } else {
            let (new_return_mode, new_return_type) =
                self.return_type.ffi_type_rust_cpp(self.return_mode);
            return_mode = new_return_mode;
            return_type = new_return_type;
        }
        for (name, conv_func) in conversion_functions {
            parameters.push(Parameter {
                name: mangle::mangle_generic_type_to_name(&name),
                mode: CallMode::Value,
                ty: conv_func,
                default: None,
            });
        }
        FunctionHead {
            name: if let Some(owner) = self.member_of.map(|(m, _)| m).or(self.associate_of) {
                format!(
                    "{}_{}",
                    owner.to_snake_case(),
                    if self.name.starts_with('~') {
                        mangle::destructor_name()
                    } else {
                        self.name.to_snake_case()
                    }
                )
            } else {
                self.name.to_snake_case()
            },
            member_of: None,
            associate_of: None,
            parameters,
            return_mode,
            return_type,
            attributes: Attribute::INTERFACE_FUNC,
        }
    }
    /// Convert the head of a signal caller into the head of a corresponding
    /// function on the Rust stage.
    pub fn into_rust_signal_caller_head(self) -> Self {
        FunctionHead {
            name: self.name.to_snake_case(),
            member_of: self.member_of.or(self
                .associate_of
                .as_ref()
                .map(|assoc| (assoc.clone(), CallMode::ReferenceMut))),
            ..self
        }
    }
    /// Convert the head of a Rust -> C++ function into the head of a corresponding
    /// function on the interface stage.
    ///
    /// This simply converts parameters and changes the name to "[]"
    /// in order to turn it into a lambda function.
    pub fn into_interface_rust_to_cpp(self) -> Self {
        let mut parameters = Parameter::convert_parameters_ffi_rust_cpp(&self.parameters);
        if let Some((owner, self_mode)) = &self.member_of {
            parameters.insert(
                0,
                Parameter::new(
                    "owner".to_string(),
                    ObjectCppType(owner.to_owned()).into(),
                    self_mode.into_ffi_mode(),
                ),
            );
        }
        let (return_mode, return_type) = self.return_type.ffi_type_rust_cpp(self.return_mode);
        FunctionHead {
            name: "[]".to_string(),
            member_of: None,
            associate_of: None,
            parameters,
            return_mode,
            return_type,
            attributes: Attribute::EXTERN_C_ABI,
        }
    }
}

impl<W: Write> Repr<W> for FunctionHead {
    fn cpp(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        if self.attributes.contains(Attribute::VIRTUAL) {
            token_cpp!(w, cont, "virtual ");
        }
        if self.attributes.contains(Attribute::STATIC) {
            token_cpp!(w, cont, "static ");
        }
        // Only print the return type and mode if the function is not a constructor
        // or a lambda function
        if !(self
            .member_of
            .as_ref()
            .map(|(m, _)| m)
            .or(self.associate_of.as_ref())
            .map(|member_class| self.name.ends_with(member_class))
            .unwrap_or(false)
            || self.name == "[]")
        {
            token_cpp!(w, cont, self.return_type, " ");
            token_cpp!(w, cont, self.return_mode);
        }
        token_cpp!(w, cont, self.name);
        paren!(w, token_cpp!(w, cont, self.parameters[..]););
        if let Some((_, CallMode::Reference)) = self.member_of {
            token_cpp!(w, cont, "const ")
        }
        if self.attributes.contains(Attribute::OVERRIDE) {
            token_cpp!(w, cont, "override ");
        }
        Ok(())
    }
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        if self.attributes.contains(Attribute::PUB) {
            token_rust!(w, cont, "pub ");
        }
        if self.attributes.contains(Attribute::UNSAFE) {
            token_rust!(w, cont, "unsafe ");
        }
        if self.attributes.contains(Attribute::EXTERN_C_ABI) {
            token_rust!(w, cont, r#"extern "C" "#);
        }
        token_rust!(w, cont, "fn ", self.name);

        let mut params = if let Some((type_name, mode)) = &self.member_of {
            vec![Parameter {
                name: "self".to_owned(),
                ty: ObjectRustType(type_name.clone()).into(),
                mode: *mode,
                default: None,
            }]
        } else {
            Vec::new()
        };
        params.extend(self.parameters.iter().cloned());
        paren!(w, token_rust!(w, cont, params););
        if self.return_type != Void.into() {
            token_rust!(w, cont, " -> ", (&self.return_mode, &self.return_type));
        }
        Ok(())
    }
}
