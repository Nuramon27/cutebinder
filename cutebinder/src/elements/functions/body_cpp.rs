use std::iter::{self};

use std::io::{self, Write};

use heck::SnakeCase;

use crate::mangle;
use crate::{brace, indented, paren, statement_cpp, statement_rust, token_cpp, token_rust};

use crate::elements::expressions::{FunctionCall, RValue};

use crate::elements::references::{AssignMode, CallMode, RefMode};
use crate::elements::statements::{Assignment, Initialization, Ret};
use crate::elements::types::{
    ObjectCppType, ObjectRustType, ObjectType, PrimitiveType::Void, StructFFIType, Type,
    Type::Primitive, TypeTrait,
};

use crate::elements::Context;

use super::{Attribute, FunctionHead, Parameter, SignalCollection};

/// The implementation of a function on the C++ side.
///
/// In the simplest case, the only information you need to write the body
/// of such a function is the head of the function. These are functions that
/// simply "call through" the corresponding function in the next stage,
/// i.e. either from C++ to Interface or from Interface to C++.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum FunctionBodyCpp {
    /// Calls an equivalent function on the interface stage,
    /// decomposing the arguments and resuming the return value.
    CallEquivalent(FunctionHead),
    /// Expresses the call to an equivalent function on the C++ stage from
    /// the Rust stage via a Lambda function.
    ///
    /// This is used for signals and constructors on the Rust stage.
    CallUnderlyingViaLambda(FunctionHead),
    /// Constructs an object from the C++ stage, calling an equivalent
    /// constructor on the Rust stage of the underlying
    /// Rust object.
    Constructor {
        head: FunctionHead,
        //signals: Vec<FunctionHead>,
        constructible_children: SignalCollection,
    },
    /// Constructos an object from an already constructed
    /// underlying Rust object.
    ///
    /// Since the Rust object is the only one caring about the arguments
    /// to its constructor, this constructor has only one parameter which is the
    /// underlying Rust object.
    ConstructorForRust {
        type_name: String,
    },
    Destructor {
        type_name: String,
    },
    DestructorForRust {
        type_name: String,
    },
    BuildSignalCollection {
        signals: SignalCollection,
    },
}

impl FunctionBodyCpp {
    /// Returns the function on the interface stage which will be called by
    /// `self`, if any
    pub fn interface_head(&self) -> Option<FunctionHead> {
        match self {
            FunctionBodyCpp::CallEquivalent(head) => Some(head.clone().into_rust_interface_head()),
            FunctionBodyCpp::CallUnderlyingViaLambda(_) => None,
            FunctionBodyCpp::Constructor { head, .. } => {
                Some(head.clone().into_rust_interface_head())
            }
            FunctionBodyCpp::ConstructorForRust { .. } => None,
            FunctionBodyCpp::DestructorForRust { .. } => None,
            FunctionBodyCpp::Destructor { type_name } => {
                Some(FunctionHead::destructor_cpp(&type_name).into_rust_interface_head())
            }
            FunctionBodyCpp::BuildSignalCollection { .. } => None,
        }
    }
    pub fn head(&self) -> Option<FunctionHead> {
        match self {
            FunctionBodyCpp::CallEquivalent(head) => Some(head.clone()),
            // Lambda functions don't need a declaration
            FunctionBodyCpp::CallUnderlyingViaLambda(_)
            | FunctionBodyCpp::DestructorForRust { .. } => None,
            FunctionBodyCpp::Constructor { head, .. } => {
                let mut head = head.clone().into_constructor_cpp_on_cpp();
                head.parameters.push(Parameter {
                    name: "parent".to_string(),
                    ty: ObjectCppType("QObject".to_owned()).into(),
                    mode: CallMode::PointerMut,
                    default: Some(RValue::new("nullptr".to_owned(), RefMode::Value)),
                });
                Some(head)
            }
            FunctionBodyCpp::ConstructorForRust { type_name } => {
                Some(FunctionHead::constructor_rust(type_name))
            }
            FunctionBodyCpp::Destructor { type_name } => {
                Some(FunctionHead::destructor_cpp(type_name))
            }
            FunctionBodyCpp::BuildSignalCollection { signals } => Some(FunctionHead {
                name: "buildSignalCollection".to_owned(),
                member_of: None,
                associate_of: Some(signals.type_name.clone()),
                parameters: vec![Parameter::new(
                    "self_struct".to_owned(),
                    ObjectCppType(signals.type_name.clone()).into(),
                    CallMode::PointerMut,
                )],
                return_type: StructFFIType(mangle::mangle_signal_collection(&signals.type_name))
                    .into(),
                return_mode: CallMode::Value,
                attributes: Attribute::PUB | Attribute::STATIC,
            }),
        }
    }
    /// Writes the declaration of the function to `w`, respecting context `cont`.
    pub fn decl<W: Write>(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        if let Some(head) = self.head() {
            statement_cpp!(w, cont, head);
        }
        Ok(())
    }
    /// Writes the full definition of the function to `w`, respecting context `cont`.
    pub fn block<W: Write>(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        match self {
            FunctionBodyCpp::CallEquivalent(head) => {
                // If the function is a member function, take the `self` value as the
                // first argument
                let mut arguments: Vec<RValue> = if head.member_of.is_some() {
                    vec![RValue::new(
                        format!("this->{}", mangle::field_name_of_underlying()),
                        RefMode::Value,
                    )
                    .into()]
                } else {
                    Vec::new()
                };
                // Add the ffi-versions of the remaining arguments
                arguments.extend(
                    head.parameters
                        .iter()
                        .map(|param| param.ty.convert_cpp_rust(&param.name, RefMode::Value)),
                );
                // If the return type needs special treatment, add a mutable return
                // argument and a conversion function as additional arguments.
                if head.return_type.output_type().is_some() {
                    arguments.push(RValue::new("output".to_string(), RefMode::PointerMut).into());
                }
                for (conv_function, _) in head.return_type.conversion_functions() {
                    arguments.push(RValue::new(conv_function, RefMode::Value).into());
                }

                let return_mode = match head.return_mode {
                    CallMode::Reference | CallMode::ReferenceMut => RefMode::Dereference,
                    CallMode::Value | CallMode::Pointer | CallMode::PointerMut => RefMode::Value,
                };
                let call = RValue::call(
                    FunctionCall::new(
                        if let Some((owner, _)) = &head.member_of {
                            format!("{}_{}", owner.to_snake_case(), head.name.to_snake_case())
                        } else {
                            head.name.to_snake_case()
                        },
                        arguments,
                    ),
                    return_mode,
                );

                indented!(
                    w,
                    cont,
                    token_cpp!(w, cont, head.clone().into_freestanding()),
                    "\n"
                );
                brace! {
                    w, cont,
                    ();
                    // Write the head
                    match head.return_type {
                        // Call the function and either return the result,
                        // assign it to the output parameter or do nothing,
                        // depending on the return type.
                        Primitive(Void) => { statement_cpp!(w, cont, call); },
                        _ => if let Some(output_type) = head.return_type.output_type() {
                            statement_cpp!(w, cont, Initialization::new(
                                "output".to_string(), CallMode::Value, output_type,
                                head.return_type.initialize_output_param().unwrap()
                            ));
                            statement_cpp!(w, cont, call);
                            statement_cpp!(w, cont, Ret::from("output".to_string()));
                        } else {
                            statement_cpp!(w, cont, Ret::from(call));
                        },
                    };
                };
            }

            FunctionBodyCpp::CallUnderlyingViaLambda(head) => {
                // Call the Qt-signal, resuming the arguments.
                let call = if head.member_of.is_some() {
                    FunctionCall::member(
                        head.name.clone(),
                        RValue::new("owner".to_string(), RefMode::Dereference),
                        head.parameters
                            .iter()
                            .map(|param| param.ty.resume_cpp(&param.name, RefMode::Value).into())
                            .collect(),
                    )
                } else if let Some(associate_of) = &head.associate_of {
                    FunctionCall::new(
                        if associate_of == &head.name {
                            // In this case, the function is a constructor called from the Rust stage
                            // so a new object has to be allocated.
                            format!("new {}", head.name)
                        } else {
                            // Otherwise it is simply a call to a signal.
                            format!("{}::{}", associate_of, head.name)
                        },
                        head.parameters
                            .iter()
                            .map(|param| param.ty.resume_cpp(&param.name, RefMode::Value).into())
                            .collect(),
                    )
                } else {
                    panic!("Signals must be members");
                };
                brace! {
                    w, cont,
                    // Write head.
                    token_cpp!(w, cont, head.clone().into_interface_rust_to_cpp(), " ");
                    // And body.
                    if head.return_type != Void.into() {
                        indented!(w, cont, token_cpp!(w, cont, "return ", call, ";"));
                    } else {
                        statement_cpp!(w, cont, call);
                    };
                }
            }

            FunctionBodyCpp::Constructor {
                head,
                constructible_children: _,
            } => {
                let initialize_base = FunctionCall::new(
                    "QObject".to_owned(),
                    vec![RValue::new("parent".to_owned(), RefMode::Value).into()],
                );
                let call_underlying = FunctionCall::new(
                    mangle::field_name_of_underlying(),
                    vec![RValue::call(
                        FunctionCall::new(
                            head.clone().into_rust_interface_head().name,
                            head.parameters
                                .iter()
                                .map(|param| {
                                    param.ty.convert_cpp_rust(&param.name, param.mode.into())
                                })
                                .chain(iter::once(RValue::call(
                                    FunctionCall::new(
                                        "buildSignalCollection".to_owned(),
                                        vec![RValue::new("this".to_owned(), RefMode::Value)],
                                    ),
                                    RefMode::Value,
                                )))
                                .collect(),
                        ),
                        RefMode::Value,
                    )
                    .into()],
                );
                let mut head = head.clone().into_constructor_cpp_on_cpp();
                // The constructor on the C++ stage always takes an additional parent
                // which is not passed to the Rust stage, but only used
                // to initialize the base class.
                head.parameters.push(Parameter {
                    name: "parent".to_string(),
                    ty: ObjectCppType("QObject".to_owned()).into(),
                    mode: CallMode::PointerMut,
                    default: None,
                });
                indented!(
                    w,
                    cont,
                    token_cpp!(w, cont, head.clone().into_freestanding()),
                    " :\n"
                );
                cont.inc_indent();
                indented!(w, cont, token_cpp!(w, cont, initialize_base), ",\n");
                indented!(w, cont, token_cpp!(w, cont, call_underlying), "\n");
                cont.dec_indent();
                brace! {
                    w, cont,
                    ();
                }
            }
            FunctionBodyCpp::ConstructorForRust { type_name } => {
                let initialize_base = FunctionCall::new(
                    "QObject".to_owned(),
                    // Objects created from the Rust stage do not have a parent.
                    vec![RValue::new("nullptr".to_owned(), RefMode::Value).into()],
                );
                let init_model = FunctionCall::new(
                    mangle::field_name_of_underlying(),
                    vec![RValue::new(
                        format!("new_{}", mangle::field_name_of_underlying()),
                        RefMode::Value,
                    )
                    .into()],
                );

                let head = FunctionHead::constructor_rust(type_name);
                indented!(
                    w,
                    cont,
                    token_cpp!(w, cont, head.clone().into_freestanding()),
                    ":\n"
                );
                cont.inc_indent();
                indented!(w, cont, token_cpp!(w, cont, initialize_base), ",\n");
                indented!(w, cont, token_cpp!(w, cont, init_model), "\n");
                cont.dec_indent();
                brace! {
                    w, cont,
                    ();
                }
            }
            FunctionBodyCpp::Destructor { type_name } => {
                let call_underlying_destructor = FunctionCall::new(
                    format!(
                        "{}_{}",
                        type_name.to_snake_case(),
                        mangle::destructor_name()
                    ),
                    vec![RValue::new(
                        mangle::field_name_of_underlying(),
                        RefMode::Value,
                    )],
                );
                brace! {
                    w, cont,
                    token_cpp!(w, cont, FunctionHead::destructor_cpp(&type_name).into_freestanding());
                    brace! {
                        w, cont,
                        token_cpp!(w, cont, format!("if ({} != nullptr)", mangle::field_name_of_underlying()));
                        statement_cpp!(w, cont, token_cpp!(w, cont, call_underlying_destructor));
                    };
                }
            }
            FunctionBodyCpp::DestructorForRust { type_name } => {
                let head = FunctionHead::destructor_rust(&type_name);
                let nullification = Assignment::new(
                    format!("owner->{}", mangle::field_name_of_underlying()),
                    AssignMode::Value,
                    RValue::new("nullptr".to_owned(), RefMode::Value),
                );

                brace! {
                    w, cont,
                    // Write head.
                    token_cpp!(w, cont, head.clone().into_interface_rust_to_cpp(), " ");
                    statement_cpp!(w, cont, nullification);
                    statement_cpp!(w, cont, "delete owner");
                }
            }
            FunctionBodyCpp::BuildSignalCollection { signals } => {
                let head = FunctionBodyCpp::BuildSignalCollection {
                    signals: signals.clone(),
                }
                .head();
                let build = signals.instantiate_cpp();
                brace! {
                    w, cont,
                    // Write head.
                    token_cpp!(w, cont, head.unwrap().into_freestanding(), " ");
                    token_cpp!(w, cont, "return ", build, ";\n");
                }
            }
        }
        Ok(())
    }
}
