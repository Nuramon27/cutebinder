//! The expressions module contains structs that represent differend
//! kinds of expressions, with [`RValue`]s being the most
//! fundamental ones.

use std::fmt;
use std::io::{self, Write};
use std::iter::FromIterator;

use crate::{brace, indented, paren, token_cpp, token_rust};

use super::functions::{FunctionBodyCpp, FunctionHead, Parameter};
use super::references::{CallMode, RefMode};
use super::types::Type;
use super::{Context, Repr};

/// The item from which an rvalue is derived.
///
/// A full [`RValue`] consists of such an item
/// and a [reference mode](super::references::RefMode). For example,
/// in `&mut a`, this item would be "a".
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
enum RValueCore {
    /// An rvalue that is given as a simple string.
    Text(String),
    /// The result of a function call.
    Call(FunctionCall),
    StructCreation(StructCreation),
    LambdaCpp(FunctionBodyCpp),
    ClosureRust(Vec<String>, (CallMode, Type), Box<RValue>),
}

impl<W: Write> Repr<W> for RValueCore {
    fn cpp(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        match self {
            RValueCore::Text(s) => {
                token_cpp!(w, cont, *s);
            }
            RValueCore::Call(call) => {
                token_cpp!(w, cont, *call);
            }
            RValueCore::StructCreation(creation) => {
                token_cpp!(w, cont, creation);
            }
            RValueCore::LambdaCpp(body) => {
                body.block(w, cont)?;
            }
            RValueCore::ClosureRust(..) => {
                panic!("A rust closure cannot be used on the rust side.")
            }
        }
        Ok(())
    }
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        match self {
            RValueCore::Text(s) => {
                token_rust!(w, cont, *s);
            }
            RValueCore::Call(call) => {
                token_rust!(w, cont, *call);
            }
            RValueCore::StructCreation(creation) => {
                token_rust!(w, cont, creation);
            }
            RValueCore::LambdaCpp(_) => {
                panic!("A C++-lambda function cannot be used on the rust side.")
            }
            RValueCore::ClosureRust(head, return_type, expr) => {
                token_rust!(w, cont, "|", head, "|");
                token_rust!(w, cont, " -> ", return_type);
                token_rust!(w, cont, "{{", expr, "}}");
            }
        }
        Ok(())
    }
}

/// An rvalue, i.e. something from which you can read.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct RValue {
    /// The item from which the value is taken
    right_hand: RValueCore,
    /// How the item is referenced
    mode: RefMode,
}

impl RValue {
    /// Constructs an `RValue` that references the item `name` according
    /// to `mode`.
    pub fn new(name: String, mode: RefMode) -> Self {
        RValue {
            right_hand: RValueCore::Text(name),
            mode,
        }
    }
    /// Constructs an `RValue` that references the result of the function
    /// call `call` according to `mode`.
    pub fn call(call: FunctionCall, mode: RefMode) -> Self {
        RValue {
            right_hand: RValueCore::Call(call),
            mode,
        }
    }
    pub fn struct_creation(creation: StructCreation) -> Self {
        RValue {
            right_hand: RValueCore::StructCreation(creation),
            mode: RefMode::Value,
        }
    }
    /// Constructs an `RValue` that references the C++-lambda function `lambda`.
    pub fn lambda_cpp(lambda: FunctionBodyCpp) -> Self {
        RValue {
            right_hand: RValueCore::LambdaCpp(lambda),
            mode: RefMode::Value,
        }
    }
    pub fn closure_rust(
        parameters: Vec<String>,
        return_type: (CallMode, Type),
        expr: RValue,
    ) -> Self {
        RValue {
            right_hand: RValueCore::ClosureRust(parameters, return_type, Box::new(expr)),
            mode: RefMode::Value,
        }
    }
}

impl<W: Write> Repr<W> for RValue {
    fn cpp(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        token_cpp!(w, cont, self.mode.prefix_cpp());
        token_cpp!(w, cont, self.right_hand);
        token_cpp!(w, cont, self.mode.postfix_cpp());
        Ok(())
    }
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        token_rust!(w, cont, self.mode.prefix_rust());
        token_rust!(w, cont, self.right_hand);
        token_rust!(w, cont, self.mode.postfix_rust());
        Ok(())
    }
}

/// The result of a function call.
///
/// A function call has a name, a list of arguments
/// and an optional `self` value if it is a member function.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct FunctionCall {
    /// The name of the function
    name: FunctionName,
    /// The `self` value of the call, if it is a member function.
    this: Option<Box<RValue>>,
    /// The list of arguments.
    arguments: Vec<RValue>,
}

/// The name of a function.
///
/// Function names cannot simply be represented as `String`s since
/// due to qualification, the actual name to write down can differ
/// depending on where the function is written.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
enum FunctionName {
    /// The name as a simple `String`
    Text(String),
    /// The function is the constructor of an underlying type.
    ///
    /// This variant enables to treat Constructors in C++ and in Rust
    /// in a common fashion.
    Constructor(Type),
}

impl FunctionCall {
    /// Create a non-member `FunctionCall` with a simple `name`
    /// and a list of `arguments`.
    pub fn new(name: String, arguments: Vec<RValue>) -> Self {
        FunctionCall {
            name: FunctionName::Text(name),
            this: None,
            arguments,
        }
    }
    /// Create a `FunctionCall` of a member function with a simple `name`,
    /// a `self` value `this` and a list of `arguments`.
    pub fn member(name: String, this: RValue, arguments: Vec<RValue>) -> Self {
        FunctionCall {
            name: FunctionName::Text(name),
            this: Some(Box::new(this)),
            arguments,
        }
    }
    /// Create a constructor of type `ty` with a list of `arguments`.
    pub fn constructor(ty: Type, arguments: Vec<RValue>) -> Self {
        FunctionCall {
            name: FunctionName::Constructor(ty),
            this: None,
            arguments,
        }
    }
}

impl<W: Write> Repr<W> for FunctionCall {
    fn cpp(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        if let Some(this) = &self.this {
            token_cpp!(w, cont, this.right_hand);
            match this.mode {
                RefMode::Dereference => {
                    token_cpp!(w, cont, "->");
                }
                RefMode::Value
                | RefMode::Reference
                | RefMode::ReferenceMut
                | RefMode::Pointer
                | RefMode::PointerMut
                | RefMode::CastToPointer
                | RefMode::CastToPointerMut => {
                    token_cpp!(w, cont, ".");
                }
            }
        }
        match &self.name {
            FunctionName::Text(name) => {
                token_cpp!(w, cont, *name);
            }
            FunctionName::Constructor(ty) => {
                token_cpp!(w, cont, *ty);
            }
        }
        paren!(w, token_cpp!(w, cont, self.arguments[..]););
        Ok(())
    }
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        if let Some(this) = &self.this {
            match this.mode {
                RefMode::Dereference => {
                    paren!(w, token_rust!(w, cont, "*", this.right_hand););
                }
                RefMode::Value
                | RefMode::Reference
                | RefMode::ReferenceMut
                | RefMode::Pointer
                | RefMode::PointerMut
                | RefMode::CastToPointer
                | RefMode::CastToPointerMut => {
                    token_rust!(w, cont, this.right_hand);
                }
            }
            token_rust!(w, cont, ".");
        }
        match &self.name {
            FunctionName::Text(name) => {
                token_rust!(w, cont, *name);
            }
            FunctionName::Constructor(ty) => {
                token_rust!(w, cont, *ty, "::new");
            }
        };
        paren!(w, token_rust!(w, cont, &self.arguments[..]););
        Ok(())
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct StructCreation {
    pub name: String,
    pub fields: Vec<(String, RValue)>,
}

impl StructCreation {
    pub fn trivial_from_field_names<I: IntoIterator<Item = String>>(
        name: String,
        fields: I,
    ) -> Self {
        StructCreation {
            fields: fields
                .into_iter()
                .map(|name| {
                    (
                        name.clone(),
                        RValue {
                            mode: RefMode::Value,
                            right_hand: RValueCore::Text(name),
                        },
                    )
                })
                .collect(),
            name,
        }
    }
}

impl<W: Write> Repr<W> for StructCreation {
    fn cpp(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        let mut fields_iter = self.fields.iter().peekable();
        brace! {
            w, cont,
            ();
            while let Some((_, value)) = fields_iter.next()  {
                let line_ending = if fields_iter.peek().is_some() {
                    ",\n"
                } else {
                    "\n"
                };
                indented!(w, cont, token_cpp!(w, cont, value));
                token_cpp!(w, cont, line_ending);
            };
        }

        Ok(())
    }
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        brace! {
            w, cont,
            token_rust!(w, cont, self.name, " ");
            for (name, value) in self.fields.iter() {
                if value.mode == RefMode::Value && matches!(&value.right_hand, RValueCore::Text(n) if n == name) {
                    token_rust!(w, cont, name, ",\n");
                } else {
                    indented!(w, cont, token_rust!(w, cont, name), " : ");
                    token_rust!(w, cont, value, ",\n");
                }
            };
        }

        Ok(())
    }
}
