use std::collections::BTreeMap;
use std::fmt;
use std::io::{self, Write};
use std::iter;
use std::iter::FromIterator;

use crate::elements::functions::Parameter;
use crate::mangle;
use crate::{brace, paren, statement_cpp, statement_rust, token_cpp, token_rust};

use crate::elements::{
    expressions::{FunctionCall, RValue},
    functions::{self, FunctionHead},
    references::{CallMode, RefMode},
    statements::Assignment,
    {Context, Repr},
};

use super::{Type, TypeTrait};

/// A function pointer.
///
/// Although they look complex, passing function pointers
/// through ffi is actually quite easy since they
/// can be passed by value in all directions.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct FunctionPointer {
    pub params: Vec<(CallMode, Type)>,
    pub return_type: (CallMode, Type),
    pub attributes: super::functions::Attribute,
}

impl From<FunctionPointer> for Type {
    fn from(other: FunctionPointer) -> Self {
        Type::Function(Box::new(other))
    }
}

impl FunctionPointer {
    pub fn decl_cpp<W: Write>(
        &self,
        name: &str,
        mode: CallMode,
        w: &mut W,
        cont: &mut Context,
    ) -> Result<(), io::Error> {
        token_cpp!(w, cont, self.return_type);
        token_cpp!(w, cont, " (", mode.as_mutable());
        token_cpp!(w, cont, name, ") ");
        paren!(w, token_cpp!(w, cont, self.params[..]););
        Ok(())
    }
}

impl<W: Write> Repr<W> for FunctionPointer {
    fn cpp(&self, _w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        panic!("A function pointer type cannot easily be represented in C++");
    }
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        if self
            .attributes
            .contains(super::functions::Attribute::EXTERN_C_ABI)
        {
            token_rust!(w, cont, r#"extern "C" "#);
        }
        write!(w, "fn ")?;
        paren!(w, token_rust!(w, cont, self.params[..]););
        token_rust!(w, cont, " -> ", self.return_type);
        Ok(())
    }
}

impl TypeTrait for FunctionPointer {
    fn as_type(&self) -> Type {
        self.clone().into()
    }
}
