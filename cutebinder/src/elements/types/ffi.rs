use std::collections::BTreeMap;
use std::fmt;
use std::io::{self, Write};
use std::iter;
use std::iter::FromIterator;

use crate::elements::functions::Parameter;
use crate::mangle;
use crate::{brace, paren, statement_cpp, statement_rust, token_cpp, token_rust};

use crate::elements::{
    expressions::{FunctionCall, RValue},
    functions::{self, FunctionHead},
    references::{CallMode, RefMode},
    statements::Assignment,
    {Context, Repr},
};

use super::{Type, TypeTrait};

/// A type that is only used on the ffi-boundary, but in the public
/// functions.
///
/// Thes are pure "libc-types" like `CInt` as well
/// as "empty ffi-types" like `FFIString`.
///
/// The latter are types that sometimes need to be passed
/// as a pointer through the ffi-boundary but can only
/// be used from the C++ side.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum FFIType {
    CUChar,
    CInt,
    CFloat,
    FFIObject,
    FFIOptionDummy,
    FFIStringDummy,
    FFIStringPartsQt,
    FFIStringPartsRust,
    FFIByteArrayDummy,
    FFIByteArrayPartsQt,
    FFIByteArrayPartsRust,
    FFIPoint,
    FFIPointF,
    FFISize,
    FFISizeF,
    FFIRect,
    FFIRectF,
    FFIDate,
    FFITime,
    FFIDateTime,
}

impl From<FFIType> for Type {
    fn from(other: FFIType) -> Self {
        Type::FFI(other)
    }
}

impl<W: Write> Repr<W> for FFIType {
    fn cpp(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        let res = match self {
            FFIType::CUChar => "unsigned char",
            FFIType::CInt => "int",
            FFIType::CFloat => "double",
            FFIType::FFIOptionDummy => "Option",
            FFIType::FFIStringDummy => "QString",
            FFIType::FFIStringPartsQt => "QStringParts",
            FFIType::FFIStringPartsRust => "RustStringParts",
            FFIType::FFIObject => "QObject",
            FFIType::FFIByteArrayDummy => "QByteArray",
            FFIType::FFIByteArrayPartsQt => "QByteArrayParts",
            FFIType::FFIByteArrayPartsRust => "RustByteArrayParts",
            FFIType::FFIPoint => "FFIPoint",
            FFIType::FFIPointF => "FFIPointF",
            FFIType::FFISize => "FFIPoint",
            FFIType::FFISizeF => "FFIPointF",
            FFIType::FFIRect => "FFIRect",
            FFIType::FFIRectF => "FFIRectF",
            FFIType::FFIDate => "FFIDate",
            FFIType::FFITime => "FFITime",
            FFIType::FFIDateTime => "FFIDateTime",
        };
        write!(w, "{}", res)?;
        Ok(())
    }
    fn rust(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        let res = match self {
            FFIType::CUChar => "u8",
            FFIType::CInt => "c_int",
            FFIType::CFloat => "c_double",
            FFIType::FFIObject => "void",
            FFIType::FFIOptionDummy => "OptionDummy",
            FFIType::FFIStringDummy => "QString",
            FFIType::FFIStringPartsQt => "QStringParts",
            FFIType::FFIStringPartsRust => "RustStringParts",
            FFIType::FFIByteArrayDummy => "QByteArray",
            FFIType::FFIByteArrayPartsQt => "QByteArrayParts",
            FFIType::FFIByteArrayPartsRust => "RustByteArrayParts",
            FFIType::FFIPoint => "FFIPoint",
            FFIType::FFIPointF => "FFIPointF",
            FFIType::FFISize => "FFISize",
            FFIType::FFISizeF => "FFISizeF",
            FFIType::FFIRect => "FFIRect",
            FFIType::FFIRectF => "FFIRectF",
            FFIType::FFIDate => "FFIDate",
            FFIType::FFITime => "FFITime",
            FFIType::FFIDateTime => "FFIDateTime",
        };
        write!(w, "{}", res)?;
        Ok(())
    }
}

impl TypeTrait for FFIType {
    fn as_type(&self) -> Type {
        self.clone().into()
    }
}
