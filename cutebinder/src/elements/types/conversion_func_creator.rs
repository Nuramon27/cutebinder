use std::io::{self, Write};

use crate::elements::expressions::RValue;
use crate::elements::Context;
use crate::elements::{
    expressions::FunctionCall,
    functions::{self, FunctionHead, Parameter},
    references::{AssignMode, CallMode, RefMode},
    statements::Assignment,
};
use crate::{brace, paren, statement_cpp, statement_rust, token_cpp, token_rust};

use super::{PrimitiveType, Type, TypeTrait};

pub enum ConversionFuncCreator {
    Enum {
        name: String,
        inner: Type,
        disc_assignment_left: String,
        disc_assignment_right: RValue,
        variant_assignment: FunctionCall,
    },
}

impl ConversionFuncCreator {
    fn head(&self) -> Option<(String, FunctionHead)> {
        todo!()
        /*match self {
            ConversionFuncCreator::Enum { name, disc_assignment_left, disc_assignment_right, variant_assignment, inner} => {
            let type_rust_cpp = inner.ffi_type_rust_cpp(CallMode::Value);
        let output_type = inner.output_type()?;
        Some((format!("rust_cpp_{}_{}", self.name_cpp(), inner.name_cpp()), FunctionHead {
            name: format!("rust_cpp_{}_{}", self.name_cpp(), self.inner.name_cpp()),
            member_of: None,
            associate_of: None,
            parameters: vec![
                Parameter {
                    name: "target".to_string(),
                    ty: output_type,
                    mode: CallMode::PointerMut,
                    default: None
                },
                Parameter {
                    name: "source".to_string(),
                    ty: type_rust_cpp.1,
                    mode: type_rust_cpp.0,
                    default: None
                },
            ],
            return_mode: CallMode::Value,
            return_type: PrimitiveType::Void.into(),
            attributes: functions::Attribute::EXTERN_C_ABI,
        }))*/
    }
    fn conversion_func_to_create<W: Write>(
        &self,
        w: &mut W,
        cont: &mut Context,
    ) -> Result<(), io::Error> {
        match self {
            ConversionFuncCreator::Enum {
                name: _,
                disc_assignment_left: _,
                disc_assignment_right: _,
                variant_assignment: _,
                inner,
            } => {
                let assignment = Assignment::new(
                    "target->is_some".to_string(),
                    AssignMode::Value,
                    RValue::new("source.is_some".to_string(), RefMode::Value),
                );
                let call = FunctionCall::new(
                    format!("rust_cpp_{}", inner.name_cpp()),
                    vec![
                        RValue::new("target->some".to_string(), RefMode::PointerMut),
                        RValue::new("source->some".to_string(), RefMode::Value),
                    ],
                );
                brace! {
                    w, cont,
                    statement_cpp!(w, cont, assignment);
                    token_cpp!(w, cont, "if (target->is_some)");
                    statement_cpp!(w, cont, call);
                }
            }
        }
        Ok(())
    }
}
