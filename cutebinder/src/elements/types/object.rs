use std::collections::BTreeMap;
use std::fmt;
use std::io::{self, Write};
use std::iter;
use std::iter::FromIterator;

use crate::elements::functions::Parameter;
use crate::mangle;
use crate::{brace, paren, statement_cpp, statement_rust, token_cpp, token_rust};

use crate::elements::{
    expressions::{FunctionCall, RValue},
    functions::{self, FunctionHead},
    references::{CallMode, RefMode},
    statements::Assignment,
    {Context, Repr},
};

use super::{ffi::FFIType, Type, TypeTrait};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct ObjectType(pub String);

impl From<ObjectType> for Type {
    fn from(other: ObjectType) -> Self {
        Type::Object(other)
    }
}

impl<W: Write> Repr<W> for ObjectType {
    fn cpp(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        write!(w, "{}", self.0)
    }
    fn rust(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        write!(w, "{}", self.0)
    }
}

impl TypeTrait for ObjectType {
    fn as_type(&self) -> Type {
        self.clone().into()
    }
    fn ffi_type_cpp_rust(&self, mode: CallMode) -> (CallMode, Type) {
        (
            mode.by_reference().into_ffi_mode(),
            ObjectRustType(self.0.clone()).into(),
        )
    }
    fn ffi_type_rust_cpp(&self, mode: CallMode) -> (CallMode, Type) {
        (
            mode.by_reference().into_ffi_mode(),
            ObjectRustType(self.0.clone()).into(),
        )
    }
    fn convert_cpp_rust(&self, expr: &str, _mode: RefMode) -> RValue {
        RValue::new(format!("{}->underlying", expr), RefMode::Value)
    }
    /*fn resume_cpp(&self, expr: &str, mode: RefMode) -> RValue {
        RValue::call(FunctionCall::member(
            "object_qt".to_owned(),
            RValue::new(expr.to_owned(), RefMode::Value),
            Vec::new()
        ), RefMode::Value)
    }*/
    fn resume_rust(&self, expr: &str, _mode: RefMode) -> RValue {
        RValue::call(
            FunctionCall::new(
                "std::ptr::read".to_owned(),
                vec![RValue::new(expr.to_owned(), RefMode::Value)],
            ),
            RefMode::Value,
        )
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct ObjectCppType(pub String);

impl From<ObjectCppType> for Type {
    fn from(other: ObjectCppType) -> Self {
        Type::ObjectCpp(other)
    }
}

impl<W: Write> Repr<W> for ObjectCppType {
    fn cpp(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        write!(w, "{}", self.0)
    }
    fn rust(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        write!(w, "{}", mangle::mangle_object_for_rust(&self.0))
    }
}

impl TypeTrait for ObjectCppType {
    fn as_type(&self) -> Type {
        self.clone().into()
    }
    fn ffi_type_cpp_rust(&self, mode: CallMode) -> (CallMode, Type) {
        (
            mode.by_reference().into_ffi_mode(),
            ObjectCppType(self.0.clone()).into(),
        )
    }
    fn ffi_type_rust_cpp(&self, mode: CallMode) -> (CallMode, Type) {
        (
            mode.by_reference().into_ffi_mode(),
            ObjectCppType(self.0.clone()).into(),
        )
    }
    fn convert_cpp_rust(&self, expr: &str, _mode: RefMode) -> RValue {
        RValue::new(expr.to_owned(), RefMode::Value)
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct ObjectRustType(pub String);

impl From<ObjectRustType> for Type {
    fn from(other: ObjectRustType) -> Self {
        Type::ObjectRust(other)
    }
}

impl<W: Write> Repr<W> for ObjectRustType {
    fn cpp(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        write!(w, "{}", mangle::mangle_object_for_cpp(&self.0))
    }
    fn rust(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        write!(w, "{}", self.0)
    }
}

impl TypeTrait for ObjectRustType {
    fn as_type(&self) -> Type {
        self.clone().into()
    }
    fn convert_cpp_rust(&self, expr: &str, _mode: RefMode) -> RValue {
        RValue::new(format!("{}->underlying", expr), RefMode::Value)
    }
}
