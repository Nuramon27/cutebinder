use std::collections::BTreeMap;
use std::fmt;
use std::io::{self, Write};
use std::iter;
use std::iter::FromIterator;

use crate::elements::functions::Parameter;
use crate::mangle;
use crate::{brace, paren, statement_cpp, statement_rust, token_cpp, token_rust};

use crate::elements::{
    expressions::{FunctionCall, RValue},
    functions::{self, FunctionHead},
    references::{CallMode, RefMode},
    statements::Assignment,
    {Context, Repr},
};

use super::{FFIType, Type, TypeTrait};

/// A type that is always converted into a transfer
/// struct on the FFI boundary, and converted back on the
/// other side.
///
/// These transfer struct typically already have appropriate
/// conversion functions defined, which are called via the `Into`
/// trait on the rust side and via Conversion Constructors
/// on the C++ side.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum StructType {
    QPoint,
    QPointF,
    QSize,
    QSizeF,
    QRect,
    QRectF,
    QDate,
    QTime,
    QDateTime,
}

impl From<StructType> for Type {
    fn from(other: StructType) -> Self {
        Type::ConvertedStruct(other)
    }
}

impl std::fmt::Display for StructType {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), fmt::Error> {
        let res = match self {
            StructType::QPoint => "point",
            StructType::QPointF => "pointf",
            StructType::QSize => "size",
            StructType::QSizeF => "sizef",
            StructType::QRect => "rect",
            StructType::QRectF => "rectf",
            StructType::QDate => "date",
            StructType::QTime => "time",
            StructType::QDateTime => "datetime",
        };
        write!(f, "{}", res)
    }
}

impl<W: Write> Repr<W> for StructType {
    fn cpp(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        write!(w, "{}", self.qt_type())
    }
    fn rust(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        let res = match self {
            StructType::QPoint => "FFIPoint",
            StructType::QSize => "FFISize",
            StructType::QPointF => "FFIPointF",
            StructType::QSizeF => "FFISizeF",
            StructType::QRect => "FFIRect",
            StructType::QRectF => "FFIRectF",
            StructType::QDate => "NaiveDate",
            StructType::QTime => "NaiveTime",
            StructType::QDateTime => "NaiveDateTime",
        };
        write!(w, "{}", res)
    }
}

impl TypeTrait for StructType {
    fn as_type(&self) -> Type {
        self.clone().into()
    }
    fn ffi_type_cpp_rust(&self, _mode: CallMode) -> (CallMode, Type) {
        (
            CallMode::Value,
            match self {
                StructType::QPoint => FFIType::FFIPoint,
                StructType::QPointF => FFIType::FFIPointF,
                StructType::QSize => FFIType::FFISize,
                StructType::QSizeF => FFIType::FFISizeF,
                StructType::QRect => FFIType::FFIRect,
                StructType::QRectF => FFIType::FFIRectF,
                StructType::QDate => FFIType::FFIDate,
                StructType::QTime => FFIType::FFITime,
                StructType::QDateTime => FFIType::FFIDateTime,
            }
            .into(),
        )
    }
    fn convert_cpp_rust(&self, expr: &str, _mode: RefMode) -> RValue {
        RValue::new(format!("{}({})", self.ffi_name(), expr), RefMode::Value)
    }
    fn convert_rust_cpp(&self, expr: RValue) -> RValue {
        RValue::call(
            FunctionCall::member("into".to_owned(), expr, Vec::new()),
            RefMode::Value,
        )
    }

    fn resume_rust(&self, expr: &str, _mode: RefMode) -> RValue {
        RValue::new(format!("{}.into()", expr), RefMode::Value)
    }
    fn resume_cpp(&self, expr: &str, _mode: RefMode) -> RValue {
        RValue::new(format!("{}({})", self.qt_type(), expr), RefMode::Value)
    }
}

impl StructType {
    /// The representation of the type on the C++ side.
    fn qt_type(&self) -> &'static str {
        match self {
            StructType::QPoint => "QPoint",
            StructType::QPointF => "QPointF",
            StructType::QSize => "QSize",
            StructType::QSizeF => "QSizeF",
            StructType::QRect => "QRect",
            StructType::QRectF => "QRectF",
            StructType::QDate => "QDate",
            StructType::QTime => "QTime",
            StructType::QDateTime => "QDateTime",
        }
    }
    /// The name of the transfer struct.
    fn ffi_name(&self) -> &'static str {
        match self {
            StructType::QPoint => "FFIPoint",
            StructType::QPointF => "FFIPointF",
            StructType::QSize => "FFIPoint",
            StructType::QSizeF => "FFIPointF",
            StructType::QRect => "FFIRect",
            StructType::QRectF => "FFIRectF",
            StructType::QDate => "FFIDate",
            StructType::QTime => "FFITime",
            StructType::QDateTime => "FFIDateTime",
        }
    }
}
