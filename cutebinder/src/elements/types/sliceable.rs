use std::collections::BTreeMap;
use std::fmt;
use std::io::{self, Write};
use std::iter;
use std::iter::FromIterator;

use crate::elements::functions::Parameter;
use crate::mangle;
use crate::{brace, paren, statement_cpp, statement_rust, token_cpp, token_rust};

use crate::elements::{
    expressions::{FunctionCall, RValue},
    functions::{self, FunctionHead},
    references::{CallMode, RefMode},
    statements::Assignment,
    {Context, Repr},
};

use super::{FFIType, FunctionPointer, PrimitiveType, Type, TypeTrait};

/// A type that can be represented as a slice of elements
/// that may be passed by value through ffi.
///
/// When passed through the ffi-boundary, a sliceable type is treated like
/// a [`StructType`]: It is converted into a transfer struct
/// in this case holding a pointer to the actual data.
///
/// When a function returns a `SliceableType`, the interface function
/// does not use return values, but instead mutable arguments for returning
/// the value. So the interface function gains additional parameters, whose types
/// can be obtained from the function [`output_types`](SliceableType::output_types).
///
/// This is necessary because the allocation for the C++ representation
/// of the type can only be done by a C++ function.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum SliceableType {
    /// A `String`
    QString,
    /// A `Vec` of bytes.
    QByteArray,
}

impl From<SliceableType> for Type {
    fn from(other: SliceableType) -> Self {
        Type::Sliceable(other)
    }
}

impl<W: Write> Repr<W> for SliceableType {
    fn cpp(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        write!(w, "{}", self.qt_type())?;
        Ok(())
    }
    fn rust(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        let res = match self {
            SliceableType::QString => "String",
            SliceableType::QByteArray => "Vec<i8>",
        };
        write!(w, "{}", res)?;
        Ok(())
    }
}

impl std::fmt::Display for SliceableType {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), fmt::Error> {
        let res = match self {
            SliceableType::QString => "qstring",
            SliceableType::QByteArray => "qbytearray",
        };
        write!(f, "{}", res)
    }
}

impl TypeTrait for SliceableType {
    fn as_type(&self) -> Type {
        self.clone().into()
    }
    fn ffi_type_cpp_rust(&self, _mode: CallMode) -> (CallMode, Type) {
        (
            CallMode::Value,
            match self {
                SliceableType::QString => FFIType::FFIStringPartsQt,
                SliceableType::QByteArray => FFIType::FFIByteArrayPartsQt,
            }
            .into(),
        )
    }
    fn ffi_type_rust_cpp(&self, _mode: CallMode) -> (CallMode, Type) {
        (
            CallMode::Value,
            match self {
                SliceableType::QString => FFIType::FFIStringPartsRust,
                SliceableType::QByteArray => FFIType::FFIByteArrayPartsRust,
            }
            .into(),
        )
    }
    fn output_type(&self) -> Option<Type> {
        Some(match self {
            SliceableType::QString => FFIType::FFIStringDummy.into(),
            SliceableType::QByteArray => FFIType::FFIByteArrayDummy.into(),
        })
    }

    fn convert_cpp_rust(&self, expr: &str, _mode: RefMode) -> RValue {
        RValue::new(
            format!("{}({})", self.ffi_name_cpp_rust(), expr),
            RefMode::Value,
        )
    }
    fn convert_rust_cpp(&self, expr: RValue) -> RValue {
        let method_name = match self {
            &SliceableType::QByteArray => "as_slice",
            &SliceableType::QString => "as_str",
        };
        RValue::call(
            FunctionCall::member(
                "into".to_owned(),
                RValue::call(
                    FunctionCall::member(method_name.to_owned(), expr, Vec::new()),
                    RefMode::Value,
                ),
                Vec::new(),
            ),
            RefMode::Value,
        )
    }

    fn initialize_output_param(&self) -> Option<RValue> {
        Some(RValue::call(
            FunctionCall::new("QString".to_owned(), Vec::new()),
            RefMode::Value,
        ))
    }
    fn set_output_param(
        &self,
        expr: &str,
        _mode: RefMode,
        mut w: &mut dyn Write,
        cont: &mut Context,
    ) -> Result<(), io::Error> {
        let expr = RValue::call(
            FunctionCall::new(
                self.conv_rust_cpp().0,
                vec![
                    RValue::new("output".to_string(), RefMode::Value).into(),
                    self.convert_rust_cpp(RValue::new(expr.to_owned(), RefMode::Value)),
                ],
            ),
            RefMode::Value,
        );
        statement_rust!(&mut w, cont, expr);
        Ok(())
    }

    fn resume_rust(&self, expr: &str, _mode: RefMode) -> RValue {
        RValue::new(format!("{}.into()", expr), RefMode::Value)
    }
    fn resume_cpp(&self, expr: &str, _mode: RefMode) -> RValue {
        RValue::new(format!("{}({})", self.qt_type(), expr), RefMode::Value)
    }

    fn conversion_functions(&self) -> BTreeMap<String, Type> {
        iter::once(self.conv_rust_cpp()).collect()
    }
}

impl SliceableType {
    /// The C++ representation of this type
    fn qt_type(&self) -> &'static str {
        match self {
            SliceableType::QString => "QString",
            SliceableType::QByteArray => "QByteArray",
        }
    }
    /// The name of the transfer struct that is used
    /// for the direction C++ <-> Rust.
    fn ffi_name_cpp_rust(&self) -> &'static str {
        match self {
            SliceableType::QString => "QStringParts",
            SliceableType::QByteArray => "QByteArrayParts",
        }
    }

    /// Tells which which types the additional parameters have
    /// that are necessary when a function returns the sliceable type `self`.
    ///
    /// Used for direction C++ <- Rust, declaration.
    pub fn conv_rust_cpp(self) -> (String, Type) {
        (
            format!("rust_cpp_{}", self),
            match self {
                SliceableType::QString => FunctionPointer {
                    params: vec![
                        (CallMode::PointerMut, FFIType::FFIStringDummy.into()),
                        (CallMode::Value, FFIType::FFIStringPartsRust.into()),
                    ],
                    return_type: (CallMode::Value, PrimitiveType::Void.into()),
                    attributes: super::functions::Attribute::EXTERN_C_ABI,
                }
                .into(),
                SliceableType::QByteArray => FunctionPointer {
                    params: vec![
                        (CallMode::PointerMut, FFIType::FFIByteArrayDummy.into()),
                        (CallMode::Value, FFIType::FFIByteArrayPartsRust.into()),
                    ],
                    return_type: (CallMode::Value, PrimitiveType::Void.into()),
                    attributes: super::functions::Attribute::EXTERN_C_ABI,
                }
                .into(),
            },
        )
    }
}
