use std::collections::BTreeMap;
use std::fmt;
use std::io::{self, Write};
use std::iter;
use std::iter::FromIterator;

use crate::elements::functions::Parameter;
use crate::mangle;
use crate::{brace, paren, statement_cpp, statement_rust, token_cpp, token_rust};

use crate::elements::{
    expressions::{FunctionCall, RValue},
    functions::{self, FunctionHead},
    references::{CallMode, RefMode},
    statements::Assignment,
    {Context, Repr},
};

use super::{FunctionPointer, PrimitiveType, Type, TypeTrait};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct GenericOneType {
    pub ty: GenericOneKind,
    pub mode: CallMode,
    pub inner: Box<Type>,
}

impl From<GenericOneType> for Type {
    fn from(other: GenericOneType) -> Self {
        Type::GenericOne(other)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum GenericOneKind {
    Option,
    OptionSemiFFI,
    OptionFFI,
}

impl std::fmt::Display for GenericOneKind {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), fmt::Error> {
        let res = match self {
            GenericOneKind::Option => "option",
            GenericOneKind::OptionSemiFFI => "option_semi_ffi",
            GenericOneKind::OptionFFI => "option_ffi",
        };
        write!(f, "{}", res)
    }
}

impl<W: Write> Repr<W> for GenericOneType {
    fn cpp(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        match self.ty {
            GenericOneKind::Option => write!(w, "Option"),
            GenericOneKind::OptionSemiFFI => write!(w, "Option"),
            GenericOneKind::OptionFFI => write!(w, "OptionFFI"),
        }?;
        write!(w, "<")?;
        (&self.mode, self.inner.as_ref()).cpp(w, cont)?;
        write!(w, ">")?;
        Ok(())
    }
    fn rust(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        match self.ty {
            GenericOneKind::Option => write!(w, "Option"),
            GenericOneKind::OptionSemiFFI => write!(w, "OptionFFI"),
            GenericOneKind::OptionFFI => write!(w, "OptionFFI"),
        }?;
        write!(w, "<")?;
        (&self.mode, self.inner.as_ref()).rust(w, cont)?;
        write!(w, ">")?;
        Ok(())
    }
}

impl GenericOneType {
    fn conversion_func_to_create_head(&self) -> Option<(String, FunctionHead)> {
        let type_rust_cpp = self.ffi_type_rust_cpp(CallMode::Value);
        let output_type = self.inner.output_type()?;
        Some((
            format!("rust_cpp_{}_{}", self.name_cpp(), self.inner.name_cpp()),
            FunctionHead {
                name: format!("rust_cpp_{}_{}", self.name_cpp(), self.inner.name_cpp()),
                member_of: None,
                associate_of: None,
                parameters: vec![
                    Parameter {
                        name: "target".to_string(),
                        ty: output_type,
                        mode: CallMode::PointerMut,
                        default: None,
                    },
                    Parameter {
                        name: "source".to_string(),
                        ty: type_rust_cpp.1,
                        mode: type_rust_cpp.0,
                        default: None,
                    },
                ],
                return_mode: CallMode::Value,
                return_type: PrimitiveType::Void.into(),
                attributes: functions::Attribute::EXTERN_C_ABI,
            },
        ))
    }
    fn conversion_func_to_create<W: Write>(
        &self,
        w: &mut W,
        cont: &mut Context,
    ) -> Result<(), io::Error> {
        if self.inner.output_type().is_some() {
            let assignment = Assignment::new(
                "target->is_some".to_string(),
                crate::elements::references::AssignMode::Value,
                RValue::new("source.is_some".to_string(), RefMode::Value),
            );
            let call = FunctionCall::new(
                format!("rust_cpp_{}", self.inner.name_cpp()),
                vec![
                    RValue::new("target->some".to_string(), RefMode::PointerMut),
                    RValue::new("source->some".to_string(), RefMode::Value),
                ],
            );
            brace! {
                w, cont,
                statement_cpp!(w, cont, assignment);
                token_cpp!(w, cont, "if (target->is_some)");
                statement_cpp!(w, cont, call);
            }
        }
        Ok(())
    }
}

impl TypeTrait for GenericOneType {
    fn as_type(&self) -> Type {
        self.clone().into()
    }
    fn ffi_type_cpp_rust(&self, _mode: CallMode) -> (CallMode, Type) {
        let ty = match self.ty {
            GenericOneKind::Option | GenericOneKind::OptionSemiFFI | GenericOneKind::OptionFFI => {
                GenericOneKind::OptionFFI
            }
        };
        let (mode, inner) = self.inner.ffi_type_cpp_rust(self.mode);
        (
            CallMode::Value,
            GenericOneType {
                ty,
                mode,
                inner: Box::new(inner),
            }
            .into(),
        )
    }
    fn ffi_type_rust_cpp(&self, _mode: CallMode) -> (CallMode, Type) {
        let ty = match self.ty {
            GenericOneKind::Option | GenericOneKind::OptionSemiFFI | GenericOneKind::OptionFFI => {
                GenericOneKind::OptionFFI
            }
        };
        let (mode, inner) = self.inner.ffi_type_rust_cpp(self.mode);
        (
            CallMode::Value,
            GenericOneType {
                ty,
                mode,
                inner: Box::new(inner),
            }
            .into(),
        )
    }
    fn output_type(&self) -> Option<Type> {
        if self.inner.output_type().is_some() {
            Some(
                GenericOneType {
                    ty: GenericOneKind::OptionSemiFFI,
                    mode: self.mode,
                    inner: self.inner.clone(),
                }
                .into(),
            )
        } else {
            None
        }
    }
    fn convert_cpp_rust(&self, expr: &str, _mode: RefMode) -> RValue {
        match self.ty {
            GenericOneKind::Option | GenericOneKind::OptionSemiFFI | GenericOneKind::OptionFFI => {
                RValue::new(format!("{}", expr), RefMode::Value)
            }
        }
    }
    fn convert_rust_cpp(&self, expr: RValue) -> RValue {
        match self.inner.as_ref() {
            Type::Sliceable(ty) => RValue::call(
                FunctionCall::member(
                    "into".to_owned(),
                    RValue::call(
                        FunctionCall::member(
                            "map".to_owned(),
                            RValue::call(
                                FunctionCall::member("as_ref".to_owned(), expr, Vec::new()),
                                RefMode::Reference,
                            ),
                            vec![RValue::closure_rust(
                                vec!["a".to_owned()],
                                ty.ffi_type_rust_cpp(self.mode),
                                ty.convert_rust_cpp(RValue::new("a".to_owned(), RefMode::Value)),
                            )],
                        ),
                        RefMode::Value,
                    ),
                    Vec::new(),
                ),
                RefMode::Value,
            ),
            _ => RValue::call(
                FunctionCall::member("into".to_owned(), expr, Vec::new()),
                RefMode::Value,
            ),
        }
    }
    fn initialize_output_param(&self) -> Option<RValue> {
        let initialization_inner = self.inner.initialize_output_param()?;
        Some(match self.ty {
            GenericOneKind::Option | GenericOneKind::OptionSemiFFI | GenericOneKind::OptionFFI => {
                RValue::call(
                    FunctionCall::constructor(self.clone().into(), vec![initialization_inner]),
                    RefMode::Value,
                )
            }
        })
    }
    fn set_output_param(
        &self,
        expr: &str,
        _mode: RefMode,
        mut w: &mut dyn Write,
        cont: &mut Context,
    ) -> Result<(), io::Error> {
        if let Some(conv_rust_cpp) = self.conv_rust_cpp() {
            let expr = RValue::call(
                FunctionCall::new(
                    mangle::mangle_generic_type_to_name(&conv_rust_cpp.0),
                    vec![
                        RValue::new("output".to_string(), RefMode::Value).into(),
                        self.convert_rust_cpp(RValue::new(expr.to_owned(), RefMode::Value)),
                    ],
                ),
                RefMode::Value,
            );
            statement_rust!(&mut w, cont, expr);
        }
        Ok(())
    }

    fn resume_rust(&self, expr: &str, _mode: RefMode) -> RValue {
        RValue::new(format!("{}.inner_into()", expr), RefMode::Value)
    }
    fn resume_cpp(&self, expr: &str, _mode: RefMode) -> RValue {
        match self.ty {
            GenericOneKind::Option | GenericOneKind::OptionSemiFFI | GenericOneKind::OptionFFI => {
                RValue::new(
                    format!("Option<{}>({})", self.inner.name_cpp(), expr),
                    RefMode::Value,
                )
            }
        }
    }
    fn conversion_functions(&self) -> BTreeMap<String, Type> {
        self.conv_rust_cpp().into_iter().collect()
    }
}

impl GenericOneType {
    fn conv_rust_cpp(&self) -> Option<(String, Type)> {
        if let Some(output_type) = self.output_type() {
            Some((
                format!(
                    "rust_cpp_{}<{}, {}>",
                    self.ty,
                    self.inner.name_cpp(),
                    self.inner.ffi_type_rust_cpp(self.mode).1.name_cpp()
                ),
                FunctionPointer {
                    params: vec![
                        (CallMode::PointerMut, output_type),
                        self.ffi_type_rust_cpp(CallMode::Value),
                    ],
                    return_type: (CallMode::Value, PrimitiveType::Void.into()),
                    attributes: super::functions::Attribute::EXTERN_C_ABI,
                }
                .into(),
            ))
        } else {
            None
        }
    }
}
