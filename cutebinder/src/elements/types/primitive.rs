use std::collections::BTreeMap;
use std::fmt;
use std::io::{self, Write};
use std::iter;
use std::iter::FromIterator;

use crate::elements::functions::Parameter;
use crate::mangle;
use crate::{brace, paren, statement_cpp, statement_rust, token_cpp, token_rust};

use crate::elements::{
    expressions::{FunctionCall, RValue},
    functions::{self, FunctionHead},
    references::{CallMode, RefMode},
    statements::Assignment,
    {Context, Repr},
};

use super::{Type, TypeTrait};

/// A primitive type that is passed by value in all directions.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum PrimitiveType {
    Void,
    Boolean,
    QChar,
    Quint8,
    Quint16,
    Quint32,
    Quint64,
    Quintptr,
    Qint8,
    Qint16,
    Qint32,
    Qint64,
    Qintptr,
    Float,
}

impl From<PrimitiveType> for Type {
    fn from(other: PrimitiveType) -> Self {
        Type::Primitive(other)
    }
}

impl<W: Write> Repr<W> for PrimitiveType {
    fn cpp(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        let res = match self {
            PrimitiveType::Void => "void",
            PrimitiveType::Boolean => "bool",
            PrimitiveType::QChar => "QChar",
            PrimitiveType::Quint8 => "quint8",
            PrimitiveType::Quint16 => "quint16",
            PrimitiveType::Quint32 => "quint32",
            PrimitiveType::Quint64 => "quint64",
            PrimitiveType::Quintptr => "quintptr",
            PrimitiveType::Qint8 => "qint8",
            PrimitiveType::Qint16 => "qint16",
            PrimitiveType::Qint32 => "qint32",
            PrimitiveType::Qint64 => "qint64",
            PrimitiveType::Qintptr => "qintptr",
            PrimitiveType::Float => "double",
        };
        write!(w, "{}", res)?;
        Ok(())
    }
    fn rust(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        let res = match self {
            PrimitiveType::Void => "()",
            PrimitiveType::Boolean => "bool",
            PrimitiveType::QChar => "u16",
            PrimitiveType::Quint8 => "u8",
            PrimitiveType::Quint16 => "u16",
            PrimitiveType::Quint32 => "u32",
            PrimitiveType::Quint64 => "u64",
            PrimitiveType::Quintptr => "usize",
            PrimitiveType::Qint8 => "i8",
            PrimitiveType::Qint16 => "i16",
            PrimitiveType::Qint32 => "i32",
            PrimitiveType::Qint64 => "i64",
            PrimitiveType::Qintptr => "isize",
            PrimitiveType::Float => "f64",
        };
        write!(w, "{}", res)?;
        Ok(())
    }
}

impl TypeTrait for PrimitiveType {
    fn as_type(&self) -> Type {
        self.clone().into()
    }
}
