use std::collections::BTreeMap;
use std::fmt;
use std::io::{self, Write};
use std::iter;
use std::iter::FromIterator;

use crate::elements::functions::Parameter;
use crate::mangle;
use crate::{brace, paren, statement_cpp, statement_rust, token_cpp, token_rust};

use crate::elements::{
    expressions::{FunctionCall, RValue},
    functions::{self, FunctionHead},
    references::{CallMode, RefMode},
    statements::Assignment,
    {Context, Repr},
};

use super::{Type, TypeTrait};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct StructFFIType(pub String);

impl From<StructFFIType> for Type {
    fn from(other: StructFFIType) -> Self {
        Type::Structure(other)
    }
}

impl<W: Write> Repr<W> for StructFFIType {
    fn cpp(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        write!(w, "{}", self.0)
    }
    fn rust(&self, w: &mut W, _cont: &mut Context) -> Result<(), io::Error> {
        write!(w, "{}", self.0)
    }
}

impl TypeTrait for StructFFIType {
    fn as_type(&self) -> Type {
        self.clone().into()
    }
}
