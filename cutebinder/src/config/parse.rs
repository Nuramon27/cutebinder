//! This module contains what is necessary to parse the .toml-file
//! which declares the interface between Qt and rust.

use std::collections::BTreeMap;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::fmt;
use std::path::PathBuf;
use std::str::FromStr;

use lazy_static::lazy_static;
use regex::Regex;
use serde::{
    de::{Deserializer, Visitor},
    Deserialize, Serialize,
};

use crate::elements::functions::Parameter;
use crate::elements::references::CallMode;
use crate::elements::types::{self, Type};

/// The immediate result of reading in the interface declaration.
#[derive(Debug, Clone)]
#[derive(Deserialize)]
pub struct Main0_1_0 {
    /// The name which shoud be given to the C++-Bindings files.
    pub cpp_file: String,
    /// The directory in which the generated C++-Bindings files should be placed.
    pub cpp_dir: String,
    /// Configuration of the generated rust-files.
    pub rust: Rust,
    /// The list of objects on the interface
    pub objects: HashMap<String, Object0_1_0>,
}

/// The configuration of the generated rust-files
#[derive(Debug, Clone)]
#[derive(Deserialize)]
pub struct Rust {
    /// The directory of the crate which contains the rust logic.
    pub dir: String,
    /// The name of the module which should contain the rust-side
    /// of the bindings.
    pub interface_module: String,
    /// The name of the module which contains the implementation of
    /// the objects on the interface.
    ///
    /// Typically one wants to scatter these over many modules,
    /// so the module named here should contain `pub use`-statement
    /// for each of them.
    pub implementation_module: String,
}

#[derive(Debug, Clone)]
#[derive(Deserialize)]
pub struct Object0_1_0 {
    #[serde(default)]
    pub constructors: Vec<Function0_1_0>,
    #[serde(default)]
    pub constructible_children: Vec<String>,
    #[serde(default)]
    pub functions: Vec<Function0_1_0>,
    #[serde(default)]
    pub signals: Vec<Function0_1_0>,
}

#[derive(Debug, Clone)]
pub struct Function0_1_0 {
    pub name: String,
    pub member: Option<CallMode>,
    pub parameters: Vec<Parameter>,
    pub return_type: TypeWithMode0_1_0,
}

#[derive(Debug, Clone)]
pub struct TypeWithMode0_1_0 {
    pub ty: Type,
    pub mode: CallMode,
}

#[derive(Debug, Clone)]
pub enum Parameter0_1_0 {
    Standard { name: String, ty: TypeWithMode0_1_0 },
    This(CallMode),
}

lazy_static! {
    /// Regex to distinguish a mutable reference from a none-mutable one
    static ref ANDMUT_REG: Regex = Regex::new(r"&\s*mut\s+").unwrap();
    /// Regex to parse a type with a reference mode
    static ref TYPEWITHMODE_REG: Regex = Regex::new(concat!(
        r#"^(?P<mode>(&\s*mut\s|&)?)\s*"#,
        r#"(?P<type>[[[:alnum:]]_<>(&\s*mut\s|&)]+)$"#,
    )).unwrap();
    /// Regex to parse a `self`-parameter with a reference mode
    static ref SELF_REG: Regex = Regex::new(r"^(?P<mode>(&|&\s*mut\s)?)\s*self$").unwrap();
    /// Regex to parse a function.
    ///
    /// The `parameters` capture will contain everything between the parentheses.
    /// It must further be split on the commata and each type parsed with [`TYPEWITHMODE_REG`].
    static ref FUNCTION_REG: Regex = Regex::new(concat!(
        r"^(?P<name>[[[:alnum:]]_]+)\s*",
        r"\((?P<parameters>[^\(\)]*)\)",
        r"(\s*->\s*(?P<return_type>[[[:alnum:]]_<>(&\s*mut\s|&)]+))?$",
    )).unwrap();
    /// Regex to parse a type with generic type parameters.
    ///
    /// The `params` capture will contain everything between the parentheses.
    /// It must further be split on the commata and each type parsed individually.
    static ref GENERIC_PARAM_REG: Regex = Regex::new(concat!(
        r"^(?P<name>[[[:alnum:]]_]+)\s*",
        r"<\s*(?P<params>.+)>$",
    )).unwrap();
}

impl FromStr for TypeWithMode0_1_0 {
    type Err = ParseFunctionError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use crate::elements::types::{
            FFIType, GenericOneKind, GenericOneType, ObjectCppType, ObjectRustType, ObjectType,
            PrimitiveType, SliceableType, StructType,
        };
        let s = s.trim();
        if let Some(capture) = TYPEWITHMODE_REG.captures(s) {
            if let (Some(mode), Some(ty)) = (capture.name("mode"), capture.name("type")) {
                let mode = if mode.as_str().is_empty() {
                    CallMode::Value
                } else if mode.as_str() == "&" {
                    CallMode::Reference
                } else if ANDMUT_REG.is_match(mode.as_str()) {
                    CallMode::ReferenceMut
                } else {
                    return Err(ParseFunctionError::BadReferenceMode(
                        mode.as_str().to_owned(),
                    ));
                };

                let ty: Type = match ty.as_str() {
                    "void" => PrimitiveType::Void.into(),
                    "bool" => PrimitiveType::Boolean.into(),
                    "char" => PrimitiveType::QChar.into(),
                    "u8" => PrimitiveType::Quint8.into(),
                    "u16" => PrimitiveType::Quint16.into(),
                    "u32" => PrimitiveType::Quint32.into(),
                    "u64" => PrimitiveType::Quint64.into(),
                    "usize" => PrimitiveType::Quintptr.into(),
                    "i8" => PrimitiveType::Qint8.into(),
                    "i16" => PrimitiveType::Qint16.into(),
                    "i32" => PrimitiveType::Qint32.into(),
                    "i64" => PrimitiveType::Qint64.into(),
                    "isize" => PrimitiveType::Qintptr.into(),
                    "f64" => PrimitiveType::Float.into(),
                    "String" => SliceableType::QString.into(),
                    "Point" => StructType::QPoint.into(),
                    "PointF" => StructType::QPointF.into(),
                    "Size" => StructType::QSize.into(),
                    "SizeF" => StructType::QSizeF.into(),
                    "Rect" => StructType::QRect.into(),
                    "RectF" => StructType::QRectF.into(),
                    "Date" => StructType::QDate.into(),
                    "Time" => StructType::QTime.into(),
                    "DateTime" => StructType::QDateTime.into(),
                    // Now we have matched for all simple and known types.
                    // What remains are generic types and custom objects.
                    // The latter must also be specified as `Object<[Name]>`
                    // i.e. like a generic type.
                    s => {
                        if let Some(captures) = GENERIC_PARAM_REG.captures(s) {
                            if let (Some(name), Some(params)) =
                                (captures.name("name"), captures.name("params"))
                            {
                                match name.as_str().trim() {
                                    "Option" => {
                                        let inner =
                                            TypeWithMode0_1_0::from_str(params.as_str().trim())?;
                                        GenericOneType {
                                            ty: GenericOneKind::Option,
                                            mode: inner.mode,
                                            inner: Box::new(inner.ty),
                                        }
                                        .into()
                                    }
                                    "Object" => {
                                        ObjectType(params.as_str().trim().to_owned()).into()
                                    }
                                    "ObjectCpp" => {
                                        ObjectCppType(params.as_str().trim().to_owned()).into()
                                    }
                                    "ObjectRust" => {
                                        ObjectRustType(params.as_str().trim().to_owned()).into()
                                    }
                                    _ => {
                                        return Err(ParseFunctionError::BadType(
                                            ty.as_str().to_owned(),
                                        ))
                                    }
                                }
                            } else {
                                return Err(ParseFunctionError::BadType(ty.as_str().to_owned()));
                            }
                        } else {
                            return Err(ParseFunctionError::BadType(ty.as_str().to_owned()));
                        }
                    }
                };

                return Ok(TypeWithMode0_1_0 { mode, ty });
            }
        }
        Err(ParseFunctionError::BadTypeDescription(s.to_owned()))
    }
}

impl FromStr for Parameter0_1_0 {
    type Err = ParseFunctionError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s = s.trim();
        if let Some(mode) = SELF_REG.captures(s).and_then(|cap| cap.name("mode")) {
            let mode = if mode.as_str().is_empty() {
                CallMode::Value
            } else if mode.as_str() == "&" {
                CallMode::Reference
            } else if ANDMUT_REG.is_match(mode.as_str()) {
                CallMode::ReferenceMut
            } else {
                return Err(ParseFunctionError::BadReferenceMode(
                    mode.as_str().to_owned(),
                ));
            };
            Ok(Parameter0_1_0::This(mode))
        } else if let (Some(name), Some(type_desc)) = {
            let mut it = s.split_terminator(':');
            (it.next(), it.next())
        } {
            Ok(Parameter0_1_0::Standard {
                name: name.to_owned(),
                ty: TypeWithMode0_1_0::from_str(type_desc)?,
            })
        } else {
            Err(ParseFunctionError::BadParameter(s.to_owned()))
        }
    }
}

impl FromStr for Function0_1_0 {
    type Err = ParseFunctionError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s = s.trim();
        if let Some(caps) = FUNCTION_REG.captures(s) {
            if let (Some(name), Some(parameters_match)) =
                (caps.name("name"), caps.name("parameters"))
            {
                let mut parameters = Vec::new();
                let mut member = None;
                for param in parameters_match.as_str().split(',') {
                    if !param.is_empty() {
                        match Parameter0_1_0::from_str(param)? {
                            Parameter0_1_0::Standard { name, ty } => {
                                let param = Parameter::new(name, ty.ty, ty.mode);
                                if param.mode != CallMode::Value
                                    && !matches!(
                                        param.ty,
                                        Type::Primitive(_)
                                            | Type::Object(_)
                                            | Type::ObjectRust(_)
                                            | Type::ObjectCpp(_)
                                    )
                                {
                                    return Err(
                                        ParseFunctionError::NonPrimitiveParameterByReference(param),
                                    );
                                }
                                parameters.push(param)
                            }
                            Parameter0_1_0::This(mode) => {
                                member = Some(mode);
                            }
                        }
                    }
                }

                let return_type = if let Some(return_type) = caps.name("return_type") {
                    TypeWithMode0_1_0::from_str(return_type.as_str())?
                } else {
                    TypeWithMode0_1_0 {
                        ty: types::PrimitiveType::Void.into(),
                        mode: CallMode::Value,
                    }
                };
                if return_type.mode != CallMode::Value
                    && !matches!(
                        return_type.ty,
                        Type::Primitive(_)
                            | Type::Object(_)
                            | Type::ObjectRust(_)
                            | Type::ObjectCpp(_)
                    )
                {
                    return Err(ParseFunctionError::NonPrimitiveByReferenceReturned(
                        return_type,
                    ));
                }
                return Ok(Function0_1_0 {
                    name: name.as_str().to_owned(),
                    member,
                    parameters,
                    return_type,
                });
            }
        }
        Err(ParseFunctionError::BadFunction(s.to_owned()))
    }
}

struct Function0_1_0Visitor;

impl<'de> Visitor<'de> for Function0_1_0Visitor {
    type Value = Function0_1_0;

    fn expecting(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "A function signature which would be valid in Rust")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        Function0_1_0::from_str(v).map_err(|e| serde::de::Error::custom(format!("{:?}", e)))
    }

    fn visit_string<E>(self, v: String) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        self.visit_str(v.as_str())
    }
}

impl<'de> Deserialize<'de> for Function0_1_0 {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_str(Function0_1_0Visitor)
    }
}

#[derive(Debug, Clone)]
pub enum ParseFunctionError {
    BadTypeDescription(String),
    BadReferenceMode(String),
    BadType(String),
    BadParameter(String),
    BadFunction(String),
    NonPrimitiveParameterByReference(Parameter),
    NonPrimitiveByReferenceReturned(TypeWithMode0_1_0),
}

#[cfg(test)]
mod tests {
    use toml::from_str;

    use super::*;

    #[test]
    fn self_regex() {
        assert_eq!(
            SELF_REG
                .captures("self")
                .unwrap()
                .name("mode")
                .unwrap()
                .as_str(),
            ""
        );
        assert_eq!(
            SELF_REG
                .captures("&self")
                .unwrap()
                .name("mode")
                .unwrap()
                .as_str(),
            "&"
        );
        assert!(ANDMUT_REG.is_match(
            SELF_REG
                .captures("&mut self")
                .unwrap()
                .name("mode")
                .unwrap()
                .as_str()
        ));
    }

    #[test]
    fn typewithmode_regex() {
        assert_eq!(
            TYPEWITHMODE_REG
                .captures("f32")
                .unwrap()
                .name("mode")
                .unwrap()
                .as_str(),
            ""
        );
        assert_eq!(
            TYPEWITHMODE_REG
                .captures("f32")
                .unwrap()
                .name("type")
                .unwrap()
                .as_str(),
            "f32"
        );
        assert_eq!(
            TYPEWITHMODE_REG
                .captures("&f32")
                .unwrap()
                .name("mode")
                .unwrap()
                .as_str(),
            "&"
        );
        assert_eq!(
            TYPEWITHMODE_REG
                .captures("&f32")
                .unwrap()
                .name("type")
                .unwrap()
                .as_str(),
            "f32"
        );
        assert_eq!(
            TYPEWITHMODE_REG
                .captures("& f32")
                .unwrap()
                .name("mode")
                .unwrap()
                .as_str(),
            "&"
        );
        assert_eq!(
            TYPEWITHMODE_REG
                .captures("& f32")
                .unwrap()
                .name("type")
                .unwrap()
                .as_str(),
            "f32"
        );
        assert_eq!(
            TYPEWITHMODE_REG
                .captures("&mut f32")
                .unwrap()
                .name("mode")
                .unwrap()
                .as_str()
                .trim(),
            "&mut"
        );
        assert_eq!(
            TYPEWITHMODE_REG
                .captures("&mut f32")
                .unwrap()
                .name("type")
                .unwrap()
                .as_str(),
            "f32"
        );
    }

    #[test]
    fn generic_regex() {
        assert_eq!(
            GENERIC_PARAM_REG
                .captures("Option<&mut String>")
                .unwrap()
                .name("name")
                .unwrap()
                .as_str(),
            "Option"
        );
        assert_eq!(
            GENERIC_PARAM_REG
                .captures("Option<&mut String>")
                .unwrap()
                .name("params")
                .unwrap()
                .as_str(),
            "&mut String"
        );
    }

    #[test]
    fn parse_function() {
        println!("{:?}", Function0_1_0::from_str(r#"get_value()"#).unwrap());
        println!(
            "{:?}",
            Function0_1_0::from_str(r#"get_value(&self, convert: bool) -> &i32"#).unwrap()
        );
    }
}
