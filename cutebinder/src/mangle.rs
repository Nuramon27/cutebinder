//! This module contains functions which define certain transformations between names
//! as they appear in the toml-file, in rust source code or in C++ source code.
//!
//! As an example, the underlying rust struct the user creates for some object
//! `Object` is simply called `Object`, but on the C++ side this struct gets the name
//! `ObjectRust`.
#![allow(dead_code)]

use heck::{CamelCase, MixedCase, SnakeCase};

pub fn mangle_rust_signal(name: &str) -> String {
    name.to_snake_case()
}

pub fn mangle_emitter(name: &str) -> String {
    format!("{}Emitter", name.to_camel_case())
}

pub fn mangle_signal_collection(name: &str) -> String {
    format!("{}Collection", name.to_camel_case())
}

pub fn mangle_object_for_cpp(name: &str) -> String {
    format!("{}Rust", name.to_camel_case())
}

pub fn mangle_object_for_rust(_name: &str) -> String {
    format!("void")
}

pub fn mangle_ctor_for_rust(name: &str) -> String {
    format!("{}_new_extern", name.to_snake_case())
}

pub fn mangle_ctor_in_collection(name: &str) -> String {
    format!("{}_func", name.to_snake_case())
}

pub fn rust_constructor_name() -> String {
    "extern_new".to_string()
}

pub fn field_name_of_underlying() -> String {
    "underlying".to_string()
}

pub fn destructor_name() -> String {
    "destructor".to_owned()
}

pub fn mangle_generic_type_to_name(name: &str) -> String {
    name.replace("<", "_")
        .replace(">", "")
        .replace(", ", "_")
        .replace(",", "_")
        .to_lowercase()
}
