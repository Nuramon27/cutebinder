use std::path::PathBuf;

use structopt::StructOpt;

/// The struct taking the command line arguments.
#[derive(Debug, Clone, PartialEq, Eq, Hash, StructOpt)]
struct Opt {
    /// The location of the "bindings.*" file.
    pub binding: PathBuf,
}

fn main() {
    let _args = Opt::from_args();
}
