//! Contains methods of [`Object`](super::Object) which generate Code
//! for the rust side.

use std::cell::RefCell;
use std::collections::{BTreeMap, HashMap};
use std::convert::{TryFrom, TryInto};
use std::fs::OpenOptions;
use std::io::BufWriter;
use std::io::{self, Write};
use std::iter;
use std::iter::FromIterator;
use std::path::{Path, PathBuf};
use std::rc::Rc;

use heck::CamelCase;
use itertools::Itertools;

use crate::config;
use crate::elements::expressions::{FunctionCall, RValue};
use crate::elements::functions::{
    self, FunctionBodyCpp, FunctionBodyInterfaceRust, FunctionHead, Parameter, SignalCollection,
};
use crate::elements::initializers::Initializer;
use crate::elements::references::{CallMode, RefMode};
use crate::elements::structs::{MemberVariable, Publicity, StructCpp, StructRust};
use crate::elements::types::{
    FFIType, FunctionPointer, ObjectCppType, ObjectRustType, ObjectType, PrimitiveType::Void, Type,
};
use crate::elements::Context;
use crate::mangle;
use crate::{brace, indented, paren, statement_cpp, statement_rust, token_cpp, token_rust};

use super::Object;

impl Object {
    pub fn impl_rust<W: Write>(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        self.trait_rust(w, cont)?;
        for func in self.functions_rust() {
            func.block(w, cont)?;
        }
        let signal_collection = self.signal_collection();
        signal_collection.emitter_rust().declaration(cont, w)?;
        signal_collection.emitter_rust().implementation(cont, w)?;
        signal_collection.as_struct_rust().declaration(cont, w)?;
        signal_collection.as_struct_rust().implementation(cont, w)?;

        Ok(())
    }

    fn trait_rust<W: Write>(&self, w: &mut W, cont: &mut Context) -> Result<(), io::Error> {
        let additional_functions = self.additional_interface_functions();
        brace! {
            w, cont,
            indented!(w, cont, token_rust!(w, cont, format!("pub trait {}Trait", self.name)));
            for func in &self.constructors {
                let func = func.clone().into_constructor_cpp_on_rust().into_trait_item();

                indented!(w, cont, token_rust!(w, cont, func), ";\n");
            };
            for func in additional_functions.iter().chain(&self.member_functions) {
                let func = func.clone().into_trait_item();
                indented!(w, cont, token_rust!(w, cont, func), ";\n");
            };
        }

        Ok(())
    }

    pub fn functions_rust(&self) -> Vec<FunctionBodyInterfaceRust> {
        self.constructors
            .iter()
            .map(|func| {
                let mut head = func.clone().name_to_snake_case();
                head.name = format!("{}", head.name);
                FunctionBodyInterfaceRust::ConstructorExtern {
                    head,
                    type_name: self.name.clone(),
                    constructible_children: self.signal_collection(),
                }
            })
            .chain(iter::once(FunctionBodyInterfaceRust::DestructorExtern {
                type_name: self.name.clone(),
            }))
            .chain(self.member_functions.iter().map(|func| {
                FunctionBodyInterfaceRust::CallTraitItem(func.clone().name_to_snake_case())
            }))
            .collect()
    }
}
