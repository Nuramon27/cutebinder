//! Contains methods of [`Object`](super::Object) which generate Code
//! for the C++ side.

use std::cell::RefCell;
use std::collections::{BTreeMap, HashMap};
use std::convert::{TryFrom, TryInto};
use std::fs::OpenOptions;
use std::io::BufWriter;
use std::io::{self, Write};
use std::iter;
use std::iter::FromIterator;
use std::path::{Path, PathBuf};
use std::rc::Rc;

use heck::CamelCase;
use itertools::Itertools;

use crate::config;
use crate::elements::expressions::{FunctionCall, RValue};
use crate::elements::functions::{
    self, FunctionBodyCpp, FunctionBodyInterfaceRust, FunctionHead, Parameter, SignalCollection,
};
use crate::elements::initializers::Initializer;
use crate::elements::references::{CallMode, RefMode};
use crate::elements::structs::{MemberVariable, Publicity, StructCpp, StructRust};
use crate::elements::types::{
    FFIType, FunctionPointer, ObjectCppType, ObjectRustType, ObjectType, PrimitiveType::Void, Type,
};
use crate::elements::Context;
use crate::mangle;
use crate::{brace, indented, paren, statement_cpp, statement_rust, token_cpp, token_rust};

use super::Object;

impl Object {
    pub fn impl_header_cpp<W: Write>(
        &self,
        w: &mut W,
        cont: &mut Context,
    ) -> Result<(), io::Error> {
        statement_cpp!(
            w,
            cont,
            format!("class {}", mangle::mangle_object_for_cpp(&self.name))
        );
        self.object_cpp().declaration(cont, w)?;
        self.extern_functions_cpp(w, cont)?;
        self.signal_collection()
            .emitter_cpp()
            .declaration(cont, w)?;
        self.signal_collection()
            .as_struct_cpp()
            .declaration(cont, w)?;

        Ok(())
    }

    pub fn impl_source_cpp<W: Write>(
        &self,
        w: &mut W,
        cont: &mut Context,
    ) -> Result<(), io::Error> {
        let struc = self.object_cpp();
        struc.implementation(cont, w)?;

        Ok(())
    }

    pub fn object_cpp(&self) -> StructCpp {
        StructCpp::new(self.name.clone())
            .object()
            .base_class("QObject".to_owned())
            .member_var(iter::once(MemberVariable {
                name: mangle::field_name_of_underlying(),
                ty: ObjectRustType(self.name.clone()).into(),
                mode: CallMode::PointerMut,
                publicity: Publicity::Protected,
            }))
            .member_func(
                Publicity::Public,
                self.constructors
                    .iter()
                    .map(|func| FunctionBodyCpp::Constructor {
                        head: func.clone(),
                        constructible_children: self.signal_collection(),
                    }),
            )
            .member_func(
                Publicity::Public,
                iter::once(FunctionBodyCpp::ConstructorForRust {
                    type_name: self.name.clone(),
                }),
            )
            .member_func(
                Publicity::Public,
                iter::once(FunctionBodyCpp::Destructor {
                    type_name: self.name.clone(),
                }),
            )
            .member_func(
                Publicity::Public,
                self.member_functions
                    .iter()
                    .map(|func| FunctionBodyCpp::CallEquivalent(func.clone().name_to_mixed_case())),
            )
            .member_func(
                Publicity::Public,
                iter::once(FunctionBodyCpp::BuildSignalCollection {
                    signals: self.signal_collection(),
                }),
            )
            .signals(
                self.signals
                    .iter()
                    .map(|func| func.clone().name_to_mixed_case()),
            )
    }

    fn extern_functions_cpp<W: Write>(
        &self,
        w: &mut W,
        cont: &mut Context,
    ) -> Result<(), io::Error> {
        let additional_functions = self.additional_interface_functions();
        brace! {
            w, cont,
            indented!(w, cont, token_cpp!(w, cont, r#"extern "C" "#));
            for func in &self.constructors {
                indented!(w, cont, token_cpp!(w, cont, func.clone().into_constructor_cpp_on_cpp_interface().into_rust_interface_head()), ";\n");
            };
            statement_cpp!(w, cont, token_cpp!(w, cont, FunctionHead::destructor_cpp(&self.name).into_rust_interface_head()));
            for func in additional_functions.iter().chain(&self.member_functions) {
                if let Some(interface_head) = FunctionBodyCpp::CallEquivalent(func.clone()).interface_head() {
                    indented!(w, cont, token_cpp!(w, cont, interface_head), ";\n");
                }
            };
        }

        Ok(())
    }
}
