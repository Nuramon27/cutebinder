//! Contains the [`Object`] struct.

mod cpp;
mod rust;

use std::cell::RefCell;
use std::collections::{BTreeMap, HashMap};
use std::convert::{TryFrom, TryInto};
use std::fs::OpenOptions;
use std::io::BufWriter;
use std::io::{self, Write};
use std::iter;
use std::iter::FromIterator;
use std::path::{Path, PathBuf};
use std::rc::Rc;

use heck::CamelCase;
use itertools::Itertools;

use crate::config;
use crate::elements::expressions::{FunctionCall, RValue};
use crate::elements::functions::{
    self, FunctionBodyCpp, FunctionBodyInterfaceRust, FunctionHead, Parameter, SignalCollection,
};
use crate::elements::initializers::Initializer;
use crate::elements::references::{CallMode, RefMode};
use crate::elements::structs::{MemberVariable, Publicity, StructCpp, StructRust};
use crate::elements::types::{
    FFIType, FunctionPointer, ObjectCppType, ObjectRustType, ObjectType, PrimitiveType::Void, Type,
};
use crate::elements::Context;
use crate::mangle;
use crate::{brace, indented, paren, statement_cpp, statement_rust, token_cpp, token_rust};

use super::ConversionError;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Object {
    /// The name of the object
    pub name: String,
    //properties: Vec<Property>,
    /// The signatures of the constructors with which this object can be created
    pub constructors: Vec<FunctionHead>,
    /// The child objects which may be created by this object
    /// from the rust side.
    pub constructible_children: Vec<Rc<RefCell<Object>>>,
    /// The member functions of the object
    pub member_functions: Vec<FunctionHead>,
    /// The signals this object can emit
    pub signals: Vec<FunctionHead>,
}

impl Object {
    /// Constructs this object with `name` from `other`,
    /// ignoring its children for the time.
    ///
    /// The children will be added later by traversing the tree of constructible
    /// children after all objects are created.
    pub fn try_new_without_children(
        name: String,
        other: config::Object0_1_0,
    ) -> Result<Self, ConversionError> {
        let mut constructors = Vec::with_capacity(other.constructors.len());
        for func in other.constructors {
            if func.member.is_some() {
                return Err(ConversionError::ConstructorHasSelf(func));
            }
            if !(matches!(&func.return_type.ty, Type::Primitive(Void))
                || matches!(&func.return_type.ty, Type::Object(ObjectType(name)) if name == &func.name))
            {
                return Err(ConversionError::NonsensicalReturnOfConstructor(func));
            }
            constructors.push(FunctionHead {
                name: func.name,
                member_of: None,
                associate_of: Some(name.clone()),
                parameters: func.parameters,
                return_mode: CallMode::Value,
                return_type: ObjectType(name.clone()).into(),
                attributes: functions::Attribute::PUB,
            })
        }
        let mut member_functions = Vec::with_capacity(other.functions.len());
        for func in other.functions {
            member_functions.push(FunctionHead {
                name: func.name,
                member_of: func.member.map(|mode| (name.clone(), mode)),
                associate_of: Some(name.clone()),
                parameters: func.parameters,
                return_mode: func.return_type.mode,
                return_type: func.return_type.ty,
                attributes: functions::Attribute::PUB,
            })
        }
        let mut signals = Vec::with_capacity(other.signals.len());
        for func in other.signals {
            if func.return_type.ty != Void.into() {
                return Err(ConversionError::SignalWithReturnValue(func));
            }
            signals.push(FunctionHead {
                name: func.name,
                member_of: func.member.map(|mode| (name.clone(), mode)),
                associate_of: Some(name.clone()),
                parameters: func.parameters,
                return_mode: CallMode::Value,
                return_type: Void.into(),
                attributes: functions::Attribute::PUB,
            })
        }

        Ok(Object {
            name,
            constructors,
            constructible_children: Vec::new(),
            member_functions,
            signals,
        })
    }
}

impl Object {
    fn additional_interface_functions(&self) -> Vec<FunctionHead> {
        vec![FunctionHead {
            name: "object_qt".to_owned(),
            member_of: Some((self.name.clone(), CallMode::ReferenceMut)),
            associate_of: None,
            parameters: Vec::new(),
            return_mode: CallMode::PointerMut,
            return_type: FFIType::FFIObject.into(),
            attributes: functions::Attribute::PUB,
        }]
    }

    pub fn signal_collection(&self) -> SignalCollection {
        SignalCollection {
            type_name: self.name.clone(),
            signals: self.signals.clone(),
            constructors: self.constructors.clone(),
            children: self
                .constructible_children
                .iter()
                .map(|child| RefCell::borrow(&child).signal_collection())
                .collect(),
        }
    }
}

/// Checks whether there are circles in the graph of constructible children of `objects`.
pub fn check_for_circles(
    objects: &HashMap<String, config::Object0_1_0>,
) -> Result<(), ConversionError> {
    let mut current_children: HashMap<_, _> = objects
        .iter()
        .map(|(name, obj)| (name.clone(), obj.constructible_children.clone()))
        .collect();
    loop {
        let old_children = std::mem::take(&mut current_children);
        for (name, children) in old_children {
            current_children.insert(name.clone(), Vec::new());
            for child in &children {
                current_children.get_mut(&name).unwrap().extend(
                    objects
                        .get(child)
                        .ok_or(ConversionError::ChildIsNotAnObject(child.clone()))?
                        .constructible_children
                        .clone(),
                );
            }
            if children.iter().any(|child| child == &name) {
                return Err(ConversionError::CircleInConstructibleChildren(name));
            }
        }
        if current_children.values().all(Vec::is_empty) {
            return Ok(());
        }
    }
}

/// Returns the names of `objects` ordered that way that the leaf elements in the tree of constructible
/// children of `objects` come first.
///
/// # Attention
///
/// May loop forever if the graph of constructible children contains circles. If in doubt
/// call [`check_for_circles`] first.
pub fn get_ordering_by_constructible_children(
    objects: &HashMap<String, config::Object0_1_0>,
) -> Result<Vec<String>, ConversionError> {
    let mut children_in_order = Vec::new();
    let mut current_children = Vec::from_iter(objects.iter().filter_map(|(name, obj)| {
        if obj.constructible_children.is_empty() {
            Some(name.to_owned())
        } else {
            None
        }
    }));
    while !current_children.is_empty() {
        children_in_order.extend(std::mem::take(&mut current_children));
        current_children = Vec::from_iter(objects.iter().filter_map(|(name, obj)| {
            if !children_in_order.contains(name)
                && obj
                    .constructible_children
                    .iter()
                    .all(|child| children_in_order.contains(child))
            {
                Some(name.to_owned())
            } else {
                None
            }
        }));
    }
    Ok(children_in_order)
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Property {
    name: String,
    ty: Type,
    mode: CallMode,
}
