//! Contains the [`Main`] struct.

mod cpp;
mod rust;

use std::cell::RefCell;
use std::collections::{BTreeMap, HashMap};
use std::convert::{TryFrom, TryInto};
use std::fs::OpenOptions;
use std::io::BufWriter;
use std::io::{self, Write};
use std::iter;
use std::iter::FromIterator;
use std::path::{Path, PathBuf};
use std::rc::Rc;

use heck::CamelCase;
use itertools::Itertools;

use crate::config;
use crate::elements::expressions::{FunctionCall, RValue};
use crate::elements::functions::{
    self, FunctionBodyCpp, FunctionBodyInterfaceRust, FunctionHead, Parameter, SignalCollection,
};
use crate::elements::initializers::Initializer;
use crate::elements::references::{CallMode, RefMode};
use crate::elements::structs::{MemberVariable, Publicity, StructCpp, StructRust};
use crate::elements::types::{
    FFIType, FunctionPointer, ObjectCppType, ObjectRustType, ObjectType, PrimitiveType::Void, Type,
};
use crate::elements::Context;
use crate::mangle;
use crate::{brace, indented, paren, statement_cpp, statement_rust, token_cpp, token_rust};

use super::{
    object::{check_for_circles, get_ordering_by_constructible_children, Object},
    ConversionError,
};

/// The collection of meta-information and the declared objects.
#[derive(Debug, Clone)]
pub struct Main {
    /// The name which shoud be given to the C++-Bindings files.
    pub cpp_file: String,
    /// The directory in which the generated C++-Bindings files should be placed.
    pub cpp_dir: String,
    /// Configuration of the generated rust-files.
    pub rust: config::parse::Rust,
    /// The objects ordered in such a way that the leaf elements in the tree of constructible
    /// children of come first.
    ///
    /// It contains the objects in a `Rc<RefCell<_>>` because the objects will
    /// not only be stored in this `Vec`, but each object will contain all its
    /// constructible children as well.
    pub objects: Vec<(String, Rc<RefCell<Object>>)>,
}

impl TryFrom<config::Main0_1_0> for Main {
    type Error = ConversionError;
    /// Creates a `Main` from `other`, analyzes the tree of constructible
    /// children and adds to each object all of its children.
    fn try_from(mut other: config::Main0_1_0) -> Result<Self, Self::Error> {
        other.objects = other
            .objects
            .into_iter()
            .map(|(name, mut obj)| {
                for child in &mut obj.constructible_children {
                    *child = child.to_camel_case();
                }
                (name.to_camel_case(), obj)
            })
            .collect();
        check_for_circles(&other.objects)?;
        let children_in_order = get_ordering_by_constructible_children(&other.objects)?;

        // Create the objects, but first without their children.
        let mut objects: HashMap<String, Rc<RefCell<Object>>> = children_in_order
            .iter()
            .map(
                |name| -> Result<(String, Rc<RefCell<Object>>), ConversionError> {
                    let object = other.objects.get(name).unwrap();
                    Ok((
                        name.clone(),
                        Rc::new(RefCell::new(Object::try_new_without_children(
                            name.clone(),
                            object.clone(),
                        )?)),
                    ))
                },
            )
            .try_collect()?;

        // Add to each object a copy of its children.
        for name in &children_in_order {
            let child_objects = other
                .objects
                .get(name)
                .unwrap()
                .constructible_children
                .iter()
                .map(|child| objects.get(child).unwrap().clone())
                .collect_vec();
            RefCell::borrow_mut(objects.get(name).unwrap()).constructible_children = child_objects;
        }

        Ok(Main {
            cpp_file: other.cpp_file,
            cpp_dir: other.cpp_dir,
            rust: other.rust,
            objects: children_in_order
                .into_iter()
                .map(|name| {
                    let obj = objects.remove(&name).unwrap();
                    (name, obj)
                })
                .collect(),
        })
    }
}

impl Main {
    /// Writes the C++-bindings files and the rust-interface file.
    pub fn write(&self, base_path: &Path) -> Result<(), std::io::Error> {
        self.write_cpp(base_path)?;
        self.write_rust(base_path)?;
        Ok(())
    }
}
