//! Contains the methods of [`Main`](super::Main) which generate Code
//! for the rust side.

use std::cell::RefCell;
use std::collections::{BTreeMap, HashMap};
use std::convert::{TryFrom, TryInto};
use std::fs::OpenOptions;
use std::io::BufWriter;
use std::io::{self, Write};
use std::iter;
use std::iter::FromIterator;
use std::path::{Path, PathBuf};
use std::rc::Rc;

use heck::CamelCase;
use itertools::Itertools;

use crate::config;
use crate::elements::expressions::{FunctionCall, RValue};
use crate::elements::functions::{
    self, FunctionBodyCpp, FunctionBodyInterfaceRust, FunctionHead, Parameter, SignalCollection,
};
use crate::elements::initializers::Initializer;
use crate::elements::references::{CallMode, RefMode};
use crate::elements::structs::{MemberVariable, Publicity, StructCpp, StructRust};
use crate::elements::types::{
    FFIType, FunctionPointer, ObjectCppType, ObjectRustType, ObjectType, PrimitiveType::Void, Type,
};
use crate::elements::Context;
use crate::mangle;
use crate::{brace, indented, paren, statement_cpp, statement_rust, token_cpp, token_rust};

use super::Main;

impl Main {
    pub fn write_rust(&self, base_path: &Path) -> Result<(), std::io::Error> {
        {
            let mut interface_path = base_path.join(&self.rust.dir).join("src");
            for module in self.rust.interface_module.split("::") {
                interface_path.push(module);
            }
            interface_path.set_extension("rs");
            let mut file = BufWriter::new(
                OpenOptions::new()
                    .write(true)
                    .create(true)
                    .truncate(true)
                    //.open(&format!("{}.rs", self.rust.interface_module))?);
                    .open(interface_path)?,
            );

            for line in Self::preamble_rust() {
                writeln!(file, "{}", line)?;
            }
            writeln!(file, "use crate::{}::*;", self.rust.implementation_module)?;

            for (_, object) in &self.objects {
                let mut cont = Context::new();
                RefCell::borrow(object).impl_rust(&mut file, &mut cont)?;
            }

            file.flush()?;
        }

        Ok(())
    }
    fn preamble_rust() -> &'static [&'static str] {
        &["#![allow(unused)]", "use cutebinder_helper::prelude::*;"]
    }
}
