//! Contains the methods of [`Main`](super::Main) which generate Code
//! for the C++ side.

use std::cell::RefCell;
use std::collections::{BTreeMap, HashMap};
use std::convert::{TryFrom, TryInto};
use std::fs::OpenOptions;
use std::io::BufWriter;
use std::io::{self, Write};
use std::iter;
use std::iter::FromIterator;
use std::path::{Path, PathBuf};
use std::rc::Rc;

use heck::CamelCase;
use itertools::Itertools;

use crate::config;
use crate::elements::expressions::{FunctionCall, RValue};
use crate::elements::functions::{
    self, FunctionBodyCpp, FunctionBodyInterfaceRust, FunctionHead, Parameter, SignalCollection,
};
use crate::elements::initializers::Initializer;
use crate::elements::references::{CallMode, RefMode};
use crate::elements::structs::{MemberVariable, Publicity, StructCpp, StructRust};
use crate::elements::types::{
    FFIType, FunctionPointer, ObjectCppType, ObjectRustType, ObjectType, PrimitiveType::Void, Type,
};
use crate::elements::Context;
use crate::mangle;
use crate::{brace, indented, paren, statement_cpp, statement_rust, token_cpp, token_rust};

use super::Main;

impl Main {
    pub fn write_cpp(&self, base_path: &Path) -> Result<(), std::io::Error> {
        let helper_header = include_str!("../../../cpp/helper.hpp");
        let helper_source = include_str!("../../../cpp/helper.cpp");
        let cpp_dir = base_path.join(&self.cpp_dir);
        std::fs::create_dir_all(&cpp_dir)?;
        {
            std::fs::write(cpp_dir.join("helper.hpp"), helper_header)?;
            std::fs::write(cpp_dir.join("helper.cpp"), helper_source)?;
        }
        {
            let mut header_file = BufWriter::new(
                OpenOptions::new()
                    .write(true)
                    .create(true)
                    .truncate(true)
                    .open(cpp_dir.join(&self.cpp_file).with_extension("hpp"))?,
            );

            for line in Self::preamble_cpp_header() {
                writeln!(header_file, "{}", line)?;
            }

            for (_, object) in &self.objects {
                writeln!(header_file, "class {};", RefCell::borrow(object).name)?;
                writeln!(
                    header_file,
                    "class {};",
                    mangle::mangle_emitter(&RefCell::borrow(object).name)
                )?;
                writeln!(
                    header_file,
                    "class {};",
                    mangle::mangle_signal_collection(&RefCell::borrow(object).name)
                )?;
            }

            for (_, object) in &self.objects {
                let mut cont = Context::new();
                RefCell::borrow(object).impl_header_cpp(&mut header_file, &mut cont)?;
            }
            writeln!(header_file, "#endif // BINDINGS_H")?;
            header_file.flush()?;
        }

        {
            let mut source_file = BufWriter::new(
                OpenOptions::new()
                    .write(true)
                    .create(true)
                    .truncate(true)
                    .open(cpp_dir.join(&self.cpp_file).with_extension("cpp"))?,
            );

            for line in Self::preamble_cpp_source() {
                writeln!(source_file, "{}", line)?;
            }

            for (_, object) in &self.objects {
                let mut cont = Context::new();
                RefCell::borrow(object).impl_source_cpp(&mut source_file, &mut cont)?;
            }
            source_file.flush()?;
        }

        Ok(())
    }
    fn preamble_cpp_header() -> &'static [&'static str] {
        &[
            "#ifndef BINDINGS_H",
            "#define BINDINGS_H",
            "",
            "#include <QObject>",
            r#"#include "helper.hpp""#,
        ]
    }
    fn preamble_cpp_source() -> &'static [&'static str] {
        &[r#"#include "Bindings.hpp""#, r#"#include "helper.hpp""#]
    }
}
