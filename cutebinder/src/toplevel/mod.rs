mod main;
mod object;

use std::cell::RefCell;
use std::collections::{BTreeMap, HashMap};
use std::convert::{TryFrom, TryInto};
use std::fs::OpenOptions;
use std::io::BufWriter;
use std::io::{self, Write};
use std::iter;
use std::iter::FromIterator;
use std::path::{Path, PathBuf};
use std::rc::Rc;

use heck::CamelCase;
use itertools::Itertools;

use crate::config;
use crate::elements::expressions::{FunctionCall, RValue};
use crate::elements::functions::{
    self, FunctionBodyCpp, FunctionBodyInterfaceRust, FunctionHead, Parameter, SignalCollection,
};
use crate::elements::initializers::Initializer;
use crate::elements::references::{CallMode, RefMode};
use crate::elements::structs::{MemberVariable, Publicity, StructCpp, StructRust};
use crate::elements::types::{
    FFIType, FunctionPointer, ObjectCppType, ObjectRustType, ObjectType, PrimitiveType::Void, Type,
};
use crate::elements::Context;
use crate::mangle;
use crate::{brace, indented, paren, statement_cpp, statement_rust, token_cpp, token_rust};
use object::{check_for_circles, get_ordering_by_constructible_children, Object};

pub use main::Main;

#[derive(Debug, Clone)]
pub enum ConversionError {
    ParseFunction(config::parse::ParseFunctionError),
    Serde(toml::ser::Error),
    ConstructorHasSelf(config::parse::Function0_1_0),
    NonsensicalReturnOfConstructor(config::parse::Function0_1_0),
    SignalWithReturnValue(config::parse::Function0_1_0),
    ChildIsNotAnObject(String),
    CircleInConstructibleChildren(String),
}

impl From<config::parse::ParseFunctionError> for ConversionError {
    fn from(other: config::parse::ParseFunctionError) -> Self {
        ConversionError::ParseFunction(other)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::elements::functions::{self, FunctionHead, Parameter};
    use crate::elements::types::{PrimitiveType, SliceableType};

    #[test]
    fn object_simple() {
        let object = Object {
            name: "Master".to_string(),
            constructors: vec![FunctionHead {
                name: "from_string".to_string(),
                member_of: None,
                associate_of: Some("Master".to_string()),
                parameters: vec![Parameter {
                    name: "me".to_string(),
                    ty: SliceableType::QString.into(),
                    mode: CallMode::Value,
                    default: None,
                }],
                return_type: ObjectType("Master".to_string()).into(),
                return_mode: CallMode::Value,
                attributes: functions::Attribute::empty(),
            }],
            constructible_children: Vec::new(),
            member_functions: vec![FunctionHead {
                name: "get_value".to_string(),
                member_of: Some(("Master".to_string(), CallMode::Reference)),
                associate_of: None,
                parameters: vec![Parameter {
                    name: "yes".to_string(),
                    ty: PrimitiveType::Boolean.into(),
                    mode: CallMode::Value,
                    default: None,
                }],
                return_type: PrimitiveType::Qint32.into(),
                return_mode: CallMode::Value,
                attributes: functions::Attribute::empty(),
            }],
            signals: vec![FunctionHead {
                name: "signal".to_string(),
                member_of: Some(("Master".to_string(), CallMode::Reference)),
                associate_of: None,
                parameters: vec![],
                return_type: PrimitiveType::Void.into(),
                return_mode: CallMode::Value,
                attributes: functions::Attribute::empty(),
            }],
        };

        let mut buffer = Vec::<u8>::new();
        let mut context = Context::new();
        object.impl_header_cpp(&mut buffer, &mut context).unwrap();
        let mut buffer = Vec::<u8>::new();
        object.impl_source_cpp(&mut buffer, &mut context).unwrap();
        let mut buffer = Vec::<u8>::new();
        object.impl_rust(&mut buffer, &mut context).unwrap();
    }
}
