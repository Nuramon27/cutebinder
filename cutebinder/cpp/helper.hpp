#ifndef HELPER_H
#define HELPER_H


#include <QString>
#include <QByteArray>
#include <QDate>
#include <QTime>
#include <QDateTime>
#include <QPoint>
#include <QPointF>
#include <QSize>
#include <QSizeF>
#include <QRect>
#include <QRectF>


template<class R>
class Option;


template<class T>
class OptionFFI {
public:
    char is_some;
    union {
        T some;
        char none;
    };

    template<class R>
    OptionFFI<T>(Option<R> const &other) :
        is_some(other.is_some),
        none(0)
    {
        if (is_some)
        {
            some = other.some;
        }
    }
};

template<class T>
class Option {
public:
    char is_some;
    union {
        T some;
        char none;
    };

public:
    Option() :
        is_some(false),
        none(0)
    {
    }
    Option(Option<T> const &other) :
        is_some(other.is_some),
        none(0)
    {
        if (is_some)
        {
            new (&some) T(other.some);
        }
    }
    template<class R>
    Option(OptionFFI<R> const &other) :
        is_some(other.is_some),
        none(0)
    {
        if (is_some)
        {
            new (&some) T(other.some);
        }
    }
    Option(T const &value_new) :
        is_some(true),
        some(value_new)
    {
    }
    ~Option()
    {
        if (is_some)
        {
            some.~T();
        }
    }

    T get()
    {
        return some;
    }



};

template<class T1, class T2>
void rust_cpp_option(Option<T1> *target, OptionFFI<T2> source)
{
    if (source.is_some)
    {
        target->some = T1(source.some);
    }
    else
    {
        target->none = 0;
    }
    target->is_some = source.is_some;
}

class QStringParts {
public:
    QChar const *data;
    quintptr len;

    QStringParts(QString const &other);
};

class RustStringParts {
public:
    char const *data;
    quintptr len;

    operator QString() const;
};

class QByteArrayParts {
public:
    char const *data;
    quintptr len;

    QByteArrayParts(QByteArray const &other);
};

class RustByteArrayParts {
public:
    char const *data;
    quintptr len;

    operator QByteArray() const;
};

extern "C" {
    void rust_cpp_qstring(QString* target, RustStringParts source);
    void rust_cpp_qbytearray(QByteArray *target, RustStringParts source);
    //void rust_cpp_option(Option<void*> *target, OptionFFI<void*> source, void conversion_func(void*, void*));
}

class FFIDate {
    qint32 year;
    quint32 month;
    quint32 day;

public:
    FFIDate(QDate other);
    operator QDate() const;
};

class FFITime {
    quint32 hour;
    quint32 minute;
    quint32 second;
    quint32 millisecond;

public:
    FFITime(QTime other);
    operator QTime() const;
};

class FFIDateTime {
    FFIDate date;
    FFITime time;

public:
    FFIDateTime(QDateTime other);
    operator QDateTime() const;
};

class FFIPoint {
    qint32 x;
    qint32 y;

public:
    FFIPoint(QPoint other);
    FFIPoint(QSize other);
    operator QPoint() const;
    operator QSize() const;
};

class FFIPointF {
    qreal x;
    qreal y;

public:
    FFIPointF(QPointF other);
    FFIPointF(QSizeF other);
    operator QPointF() const;
    operator QSizeF() const;
};

class FFIRect {
    qint32 x;
    qint32 y;
    qint32 width;
    qint32 height;

public:
    FFIRect(QRect other);
    operator QRect() const;
};

class FFIRectF {
    qreal x;
    qreal y;
    qreal width;
    qreal height;

public:
    FFIRectF(QRectF other);
    operator QRectF() const;
};

#endif // HELPER_H
