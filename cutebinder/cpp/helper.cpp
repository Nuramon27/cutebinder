#include "helper.hpp"

void rust_cpp_qstring(QString* target, RustStringParts source) {
    *target = QString::fromUtf8(source.data, static_cast<int>(source.len));
}

void rust_cpp_qbytearray(QByteArray *target, RustStringParts source) {
    *target = QByteArray(source.data, static_cast<int>(source.len));
}

void rust_cpp_qpoint(QPoint *target, int x, int y) {
    target->setX(x);
    target->setY(y);
}

void rust_cpp_qpointf(QPointF *target, double x, double y) {
    target->setX(x);
    target->setY(y);
}

void rust_cpp_qrect(QRect *target, int x, int y, int width, int height) {
    target->setRect(x, y, width, height);
}

void rust_cpp_qrect(QRectF *target, double x, double y, double width, double height) {
    target->setRect(x, y, width, height);
}


QStringParts::QStringParts(QString const &other) :
    data(other.constData()),
    len(static_cast<quintptr>(other.length()))
{
}

RustStringParts::operator QString() const
{
    return QString::fromUtf8(data, static_cast<int>(len));
}

QByteArrayParts::QByteArrayParts(QByteArray const &other) :
    data(other.constData()),
    len(static_cast<quintptr>(other.length()))
{
}

RustByteArrayParts::operator QByteArray() const
{
    return QByteArray(data, static_cast<int>(len));
}


FFIDate::FFIDate(QDate other) :
    year(static_cast<qint32>(other.year())),
    month(static_cast<quint32>(other.month())),
    day(static_cast<quint32>(other.day()))
{
}

FFIDate::operator QDate() const
{
    return QDate(static_cast<int>(year), static_cast<int>(month), static_cast<int>(day));
}

FFITime::FFITime(QTime other) :
    hour(static_cast<quint32>(other.hour())),
    minute(static_cast<quint32>(other.minute())),
    second(static_cast<quint32>(other.second())),
    millisecond(static_cast<quint32>(other.msec()))
{
}

FFITime::operator QTime() const
{
    return QTime(static_cast<quint32>(hour), static_cast<quint32>(minute), static_cast<quint32>(second), static_cast<quint32>(millisecond));
}

FFIDateTime::FFIDateTime(QDateTime other) :
    date(other.date()),
    time(other.time())
{
}

FFIDateTime::operator QDateTime() const
{
    return QDateTime(QDate(date), QTime(time));
}



FFIPoint::FFIPoint(QPoint other) :
    x(other.x()),
    y(other.y())
{
}

FFIPoint::FFIPoint(QSize other) :
    x(other.width()),
    y(other.height())
{
}

FFIPoint::operator QPoint() const
{
    return QPoint(x, y);
}

FFIPoint::operator QSize() const
{
    return QSize(x, y);
}

FFIPointF::FFIPointF(QPointF other) :
    x(other.x()),
    y(other.y())
{
}

FFIPointF::FFIPointF(QSizeF other) :
    x(other.width()),
    y(other.height())
{
}

FFIPointF::operator QPointF() const
{
    return QPointF(x, y);
}

FFIPointF::operator QSizeF() const
{
    return QSizeF(x, y);
}

FFIRect::FFIRect(QRect other) :
    x(other.x()),
    y(other.y()),
    width(other.width()),
    height(other.height())
{
}

FFIRect::operator QRect() const
{
    return QRect(x, y, width, height);
}

FFIRectF::FFIRectF(QRectF other) :
    x(other.x()),
    y(other.y()),
    width(other.width()),
    height(other.height())
{
}

FFIRectF::operator QRectF() const
{
    return QRectF(x, y, width, height);
}
