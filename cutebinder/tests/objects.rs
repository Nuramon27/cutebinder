use std::convert::TryFrom;
use std::process::Command;

use cutebinder::{config, toplevel};

#[test]
#[ignore]
fn objects() {
    let main: config::Main0_1_0 =
        toml::from_str(&String::from_utf8(std::fs::read("tests/objects.toml").unwrap()).unwrap())
            .unwrap();

    let main = toplevel::Main::try_from(main).unwrap();
    let base_path = std::path::PathBuf::from("test_dir").join("objects");
    main.write(&base_path).unwrap();

    let res = Command::new("cargo")
        .current_dir(base_path)
        .args(["run"])
        .output()
        .expect("cargo run could not be executed successfully");

    if !res.status.success() {
        panic!(
            "\n\n––––––––––––\n{}\n––––––––––––\n\n",
            String::from_utf8_lossy(&res.stderr)
        );
    }
}
